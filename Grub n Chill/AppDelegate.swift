//

//grubnchill@gmail.com
//Treasury8!
//ondk-meeu-obvk-gmcr

//  AppDelegate.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import FacebookCore
import FacebookLogin
import GiFHUD_Swift
import UserNotifications
//import Firebase
//import Fabric
//import Crashlytics
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate{

    var window: UIWindow?
    var nvc : UINavigationController?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        FirebaseApp.configure()
//        Fabric.with([Crashlytics.self])
//        let emoji = "😃"
//        print(emoji.encodeEmoji)
//        
//        let encEmoji = emoji.encodeEmoji
//        
//        print(encEmoji.decodeEmoji)
//            
//            
//        
//        
        self.registerForPushNotifications()
        
//    assert(false)
        
        if #available(iOS 10.0, *) {
            
            
            UNUserNotificationCenter.current().getNotificationSettings() { (setttings) in
                switch setttings.soundSetting {
                case .enabled:
                    print("enabled sound setting")
                    
                case .disabled:
                    print("setting has been disabled")
                    
                case .notSupported:
                    print("something vital went wrong here")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
        
        GIFHUD.shared.setGif(named: "loading.gif")
        IQKeyboardManager.shared.enable = true
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let viewObject = LoginVC(nibName: "LoginVC", bundle: nil)
        nvc = UINavigationController(rootViewController: viewObject)
        nvc?.navigationBar.isHidden = true
        nvc?.interactivePopGestureRecognizer?.isEnabled = false
        self.window!.rootViewController = nvc
        self.window!.makeKeyAndVisible()
        
        
//        let arr = NSArray()
//        arr[5]
//        
        if (launchOptions != nil)
        {
            guard let userinfo = (launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary)  else {
                return true
            }
            
            //TODO:- change
//            if userinfo["order_id"] != nil{
//                isNotification = true
//            }
        }
        

        return true
    }
    
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                //print("Permission granted: \(granted)")
                guard granted else { return }
                self.getNotificationSettings()
            }
        } else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
            // Fallback on earlier versions
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        fcm_reg_id = "\(token)"
        print("Device Token: \(token)")
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if(application.applicationState == .active || application.applicationState == .background) {
//            if userInfo["order_id"] != nil{
//                if (userInfo["title"] as! String).contains("Delivered") {
//                    self.getOrderList(orderID: userInfo["order_id"] as! String)
//                }
//                else if isOpenOrder {
//                    let nc = NotificationCenter.default
//                    nc.post(name: Notification.Name(Notify.checkOrderScreen), object: nil)
//                }
//                else{
//                    self.getOrderList(orderID: userInfo["order_id"] as! String)
//                }
//            }
        }
    }
    
//    func getOrderList(orderID : String){
//        let hud = MBProgressHUD.showAdded(to: (nvc?.view)!, animated: true)
//        hud.label.text = "Loading...."
//        APIManager().postRequest(values: "order_history", params: ["device_id":device_id,"user_id":userID,"order_id":orderID], viewController: nil) { (error, json) in
//            DispatchQueue.main.async {
//                MBProgressHUD.hide(for: (self.nvc?.view)!, animated: true)
//                if error == nil{
//                    for(key,dic) in json!.jsonDic!{
//                        let dict = (dic as! NSDictionary).mutableCopy() as! NSMutableDictionary
//                        dict.setValue(key, forKey: "oderID")
//                        let orderValue = try! OrderStatus.init(json: dict as! [String : Any])
//                        if isOpenTracker{
//                            let nc = NotificationCenter.default
//                            nc.post(name: Notification.Name(Notify.checkTrackerScreen), object: ["orderStatus":orderValue])
//                        }else{
//                            let viewOject : OrderTrack = OrderTrack(nibName: "OrderTrack", bundle: nil)
//                            viewOject.status = orderValue
//                            let navController = UINavigationController(rootViewController: viewOject)
//                            navController.isNavigationBarHidden = true
//                            self.nvc?.present(navController, animated: true, completion: nil)
//                        }
//                    }
//                }
//            }
//        }
//    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //print("Failed to register: \(error)")
    }
    
    
//    func getKeychainValue() {
//        device_id = Validation().getUniqueDeviceIdentifierAsString()!
//    }
//   
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
//        let signedIn: Bool = SDKApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)

        return SDKApplicationDelegate.shared.application(application, open: url)
    }
    
//    @available(iOS 9.0, *)
//    private func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
//        
//        
//        SDKApplicationDelegate.shared.application(app, open: url, options:[UIApplication.OpenURLOptionsKey.sourceApplication])
//        
//        
//        let signedIn: Bool = SDKApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKeyUIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: nil)
//        return signedIn
//    }
    

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Grub_n_Chill")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

