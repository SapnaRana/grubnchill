//
//  APIManager.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
import Toast_Swift
import MBProgressHUD
import GiFHUD_Swift
import SwiftyJSON

enum RequestType: String {
    case userList = "getInfo"
}



class APIManager: NSObject {
    
    
    enum ApiResult {
        case success(JSON)
        case failure(RequestError)
    }
    enum RequestError: Error {
        case unknownError
        case connectionError
        case authorizationError(JSON)
        case invalidRequest
        case notFound
        case invalidResponse
        case serverError
        case serverUnavailable
    }
    
    @objc  static let shared = APIManager()
//    typealias ResponseCompletion = (NSError?,Jsondata?)->Void

    typealias ResponseCompletion = ((_ isSuccess: Bool, _ response: Jsondata?) -> Void)

    
    class Jsondata {
        var jsonDic: NSDictionary?
        var jsonArray: NSArray?
        var jsonString: String?
        var jsonBool: Bool?
    }
    
    func showProgress(){
        GIFHUD.shared.show(withOverlay: true)

//        let hud = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!, animated: true)
//        hud.label.text = "Loading...."
    }
    
    func hideProgress(){
        GIFHUD.shared.isShowing = true
        GIFHUD.shared.dismiss(completion: {
            print("GiFHUD dismissed")
        })

//        MBProgressHUD.hide(for: UIApplication.shared.keyWindow!, animated: true)
    }
    
    //***** check network avilability ***//
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
    func rxApiMethod(typeOf values:String, completion: @escaping (ApiResult)->Void) {
        let parameters: Parameters = [:]

        let requestString = BaseUrl + values
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        manager.request(requestString, method: .get, parameters: parameters).responseJSON { (response) in

            guard response.result.isSuccess else {
                let error: Error = response.result.error!
                completion(ApiResult.failure(.connectionError))
                return
            }
            guard let Json = response.result.value as? [String: Any] else {
                completion(ApiResult.failure(.connectionError))
                return
            }
            do {
                let json = try! JSON(data: response.data!)
                completion(ApiResult.success(json))

            }
        }
    }
//

    
    
    func requestGetInfo(value : String , completion: @escaping ResponseCompletion) {
        let parameters: Parameters = [:]
        self.requestGET(typeOf: value, parameters: parameters, completion: completion)
    }

    func requestGET(typeOf values:String, parameters: Parameters, completion: @escaping ResponseCompletion) {
        let requestString = BaseUrl + values
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        manager.request(requestString, method: .get, parameters: parameters).responseJSON { (response) in
            let JSON = response.result.value as! NSDictionary
            let handlerData =  Jsondata()
            handlerData.jsonDic = JSON

            completion(response.result.isSuccess, handlerData)
        }
    }

    
    
    
    
    func getRequest(values:String , showIndicator : Bool  , viewController : UIViewController?  , handler: @escaping (NSError?,Jsondata?)->Void) {
        if (self.isInternetAvailable() == false){
            viewController?.view.makeToast(NetworkError, duration: toastTimer, position: .center)
            handler(NSError(domain: identifire, code: 0, userInfo: nil),nil)
            return
        }
        if showIndicator == true{
            self.showProgress()
        }
        var path = BaseUrl+values
        path = path.replacingOccurrences(of: " ", with: "%20")
        
        Alamofire.request(path, method: .get)
            .responseJSON {response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.hideProgress()
                        let JSON = response.result.value as! NSDictionary
                        let handlerData =  Jsondata()
                        handlerData.jsonDic = JSON
                        print(JSON)
                        if  getString(string: JSON["status"] as Any) == "1"{
                            if JSON["data"] != nil || JSON["data"] is NSNull == false{
                                handler(nil,handlerData)
                            }else{
                                handler(NSError(domain: identifire, code: 101, userInfo: ["error":""]),handlerData)
                            }
                        }
                        else{
                            handler(NSError(domain: identifire, code: 101, userInfo: ["error":""]),handlerData)
                        }
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self.hideProgress()
                        viewController?.view.makeToast(error.localizedDescription, duration: toastTimer, position: .center)
                        handler(error as NSError?,nil)
                    }
                }
        }
    }
    
    
    func getGoogleDataRequest(values:String , showIndicator : Bool  , viewController : UIViewController?  , handler: @escaping(NSError?,Jsondata?)->Void) {
        if (self.isInternetAvailable() == false){
            viewController?.view.makeToast(NetworkError, duration: toastTimer, position: .center)
            handler(NSError(domain: identifire, code: 0, userInfo: nil),nil)
            return
        }
        if showIndicator == true{
            self.showProgress()
        }
        Alamofire.request(GoogleBaseUrl+values, method: .get)
            .responseJSON {response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.hideProgress()
                        let JSON = response.result.value as! NSDictionary
                        let handlerData =  Jsondata()
                        handlerData.jsonDic = JSON
                        print(JSON)
                        if  getString(string: JSON["status"] as Any) == "OK"{
                            if JSON["results"] != nil || JSON["results"] is NSNull == false{
                                handler(nil,handlerData)
                            }else{
                                handler(NSError(domain: identifire, code: 101, userInfo: ["error":""]),handlerData)
                            }
                        }
                        else{
                            handler(NSError(domain: identifire, code: 101, userInfo: ["error":""]),handlerData)
                        }
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self.hideProgress()
                        viewController?.view.makeToast(error.localizedDescription, duration: toastTimer, position: .center)
                        handler(error as NSError?,nil)
                    }
                }
        }
    }
    
    //*** upload image with other parameters ***//
    func uploadImage(values:String,imagesArray:NSArray, postKey : NSArray, showIndicator : Bool ,viewController : UIViewController , handler: @escaping(NSError?,Jsondata?)->Void) {
        if (self.isInternetAvailable() == false){
            viewController.view.makeToast(NetworkError, duration: toastTimer, position: .center)
            handler(NSError(domain: identifire, code: 0, userInfo: nil),nil)
            return
        }
        if showIndicator == true{
            self.showProgress()
        }

        
        let escapedAddress = BaseUrl+values.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
//        let urlpath = String(format: "http://maps.googleapis.com/maps/api/geocode/json?address=\(escapedAddress)")
        let URL = try! URLRequest(url: escapedAddress, method: .post)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (index,image) in imagesArray.enumerated() {
                let image1 = image as! UIImage
                if  let imageData = image1.jpegData(compressionQuality: 0.5){
                    multipartFormData.append(imageData, withName: postKey[index] as! String, fileName: "\(postKey[index] as! String)image.jpeg", mimeType: "image/jpeg")
                }
            }
        }, with: URL, encodingCompletion: { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress)
                })
                upload.responseJSON { response in
                    DispatchQueue.main.async {
                        if response.result.isSuccess{
                            let JSON = response.result.value as! NSDictionary
                            let handlerData =  Jsondata()
                            handlerData.jsonDic = JSON
                            handler(nil,handlerData)
                        }
                        else{
                            //  viewController.view.makeToast("image not uploaded")
                            //                            handler(NSError(domain: error.localizedDescription, code: 0, userInfo: nil),nil)
                        }
                    }  // original URL request
                }
            case .failure(let encodingError):
                DispatchQueue.main.async {
                    viewController.view.makeToast(encodingError.localizedDescription)
                    handler(encodingError as NSError?,nil)
                }
            }
        })
    }
    
    
}
