//
//
//  LocationManager.swift
//  BLE
//
//  Created by Vikas Bhatt on 31/05/17.
//  Copyright © 2017 Vikas Bhatt. All rights reserved.
///

import UIKit
import CoreLocation

@objc public protocol LocationManagerDelegate {
    // MARK: - Delegate functions
    @objc optional func didUpdateLocations(locations: [CLLocation])
    @objc optional func didChangeAuthorization()
    @objc optional func didFailWithError (error: Error)

}

public class LocationManager: NSObject,CLLocationManagerDelegate {
    public static let sharedInstance = LocationManager()
    public lazy var locationManager: CLLocationManager = CLLocationManager()
    public var startLocation: CLLocation!
    public var latitude = String()
    public var longitude = String()
    public var horizontalAccuracy = String()
    public var altitude = String()
    public var verticalAccuracy = String()
    public weak var delegate : LocationManagerDelegate?
    public var LocationSpeed : Double?
    public var bestEffortAtLocation : CLLocation!
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var timer = Timer()
    var speedValueGet = ""
    
    public override init() {
        super.init()
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        self.locationManager.startUpdatingLocation()
        self.locationManager.delegate = self
        self.locationManager.startMonitoringSignificantLocationChanges()
        startLocation = nil
        
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
        })
    }
    
    
    // MARK: - Location Manager Delegate
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
//        delegate?.didFailWithError!(error: error)
    }
    
    
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let newLocation: CLLocation? = locations.last
        let locationAge: TimeInterval? = -(newLocation?.timestamp.timeIntervalSinceNow)!
        print("Location speed : - " + "\(newLocation!.speed)")
       let Speed = "\(newLocation!.speed * 3.6)"
        print("Location speed : - " + "\(Speed) KM")

        speedValueGet = Speed
        LocationSpeed = Speed.contains("-") ? Double(String(Speed).replacingOccurrences(of: "-", with: "")) : newLocation!.speed * 3.6
//        if Double(locationAge!) > 5.0 {
//            return
//        }
//        if Double((newLocation?.horizontalAccuracy)!) < 0 {
//            return
//        }
        delegate?.didUpdateLocations!(locations: locations)
    }
    
    
  
    
    public  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            delegate?.didChangeAuthorization!()
        }
    }
    
    
    //****** check bluetooh is trun on or off ******//
    public  func checkLocationSetting()->Bool{
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                print("No access")
                return false
            case .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                return true
            }
        } else {
            print("Location services are not enabled")
            return false
        }
    }
    
    
    
    //*** Show Location Setting Alert *******//
    public func open_location_Seting(message : String , viewController : UIViewController){
        let alert = UIAlertController(title: "Location Error", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Setting", style: UIAlertAction.Style.default, handler: { action in
    self.gotoLocationSetting()
    
    }))
    viewController.present(alert, animated: true, completion: nil)
    
    }
    
    
    //***** Set Path of the Location Setting page and open Page *******//
   public  func gotoLocationSetting() {
    
    if !CLLocationManager.locationServicesEnabled() {
        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
            // If general location settings are disabled then open general location settings
            UIApplication.shared.openURL(url)
        }
    } else {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            // If general location settings are enabled then open location settings for the app
            UIApplication.shared.openURL(url)
        }
    }
    }
}

