//
//  LocationModal.swift
//  Grub n Chill
//
//  Created by orangemac04 on 19/03/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit

import MapKit


enum LocationType {
    case user
    case restaurant
}

class LocationModal: NSObject,MKAnnotation {

    var coordinate: CLLocationCoordinate2D
    var id: String?
    var icon: String?
    var distance: String?
    var title: String?
    var type: LocationType
    
    init(_ latitude:CLLocationDegrees,_ longitude:CLLocationDegrees, id:String, icon:String, type:LocationType , distance :String, name : String){
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        self.id       = id
        self.icon     = icon
        self.type     = type
        self.distance = distance
        self.title     = name
    }

}

class LocationAnnotations: NSObject {
    var restaurants:[LocationModal] = []
    
}

