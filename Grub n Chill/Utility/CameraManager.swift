//
//  CameraManager.swift
//  MedsIndia
//
//  Created by Raj Kumar on 11/09/17.
//  Copyright © 2017 Orange Mantra. All rights reserved.
//


import UIKit
@objc public protocol CameraManagerDelegate {
    // MARK: - Delegate functions
    @objc optional func willStart()
    @objc optional func didEndChoose(image:UIImage)
}

public class CameraManager: NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    static let sharedInstance = CameraManager()
    let picker = UIImagePickerController()
    public weak var delegate : CameraManagerDelegate?
    //MARK: - Delegates UIImagePickerControllerDelegate
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let topController = UIApplication.topViewController() {
            if let chosenImage = info[.editedImage] as? UIImage{
                topController.dismiss(animated: true, completion: {
                    self.delegate?.didEndChoose!(image:chosenImage)
                })
            }
            else{
                topController.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if let topController = UIApplication.topViewController() {
            topController.dismiss(animated: true, completion: {
                //to do
            })
        }
    }
    //MARK: -
    
    // ****** Get images from galary ******//
    func PickImageFromGalary(){
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        DispatchQueue.main.async {
            if let topController = UIApplication.topViewController() {
                topController.present(self.picker, animated: true, completion: {
                    
                })
            }
        }
    }
    // ****** Get images from Camera ******//
    func PickImageFromCamera(){
        picker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = .camera
            // picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            DispatchQueue.main.async {
                if let topController = UIApplication.topViewController() {
                    topController.present(self.picker, animated: true, completion: {
                        
                    })
                }
            }
        }else{
            self.noCamera()
        }
        
    }
    
    // ****** Check Camera is Available or Not******//
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        DispatchQueue.main.async {
            if let topController = UIApplication.topViewController() {
                topController.present(alertVC, animated: true, completion: {
                })
            }
        }
    }
    
    // ***** open stye sheet to choose ipmage source ***//
    func openSetting(sender: AnyObject) {
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertController.Style.actionSheet)
        settingsActionSheet.addAction(UIAlertAction(title:"Camera", style:UIAlertAction.Style.default, handler:{ action in
            self.PickImageFromCamera()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Photo Library", style:UIAlertAction.Style.default, handler:{ action in
            self.PickImageFromGalary()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Cancel", style:UIAlertAction.Style.cancel, handler:nil))
        if let presenter = settingsActionSheet.popoverPresentationController {
            if let button = sender as? UIView {
                presenter.sourceView = button
                presenter.sourceRect = button.bounds
            }
            else if let button = sender as? UITapGestureRecognizer{
                presenter.sourceView = button.view
            }
            else {
                presenter.barButtonItem = sender as? UIBarButtonItem
                presenter.permittedArrowDirections = UIPopoverArrowDirection.down;
            }
        }
        if let topController = UIApplication.topViewController() {
            DispatchQueue.main.async {
                topController.present(settingsActionSheet, animated: true, completion: {
                })
            }
        }
    }
}

//**** check -the last open viewController ******//
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

