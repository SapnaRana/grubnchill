//
//  HeaderView.swift
//  Grub n Chill
//
//  Created by orangemac04 on 22/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    @IBOutlet weak var _slideButton: UIButton!
    @IBOutlet weak var _icon: UIImageView!
    @IBOutlet weak var _searchBtn: UIButton!
    @IBOutlet weak var _wheelBtn: UIButton!
    @IBOutlet weak var _saveChangeBtn: UIButton!
    @IBOutlet weak var _headerLbl: UILabel!
    @IBOutlet weak var _searchView: UIView!
    @IBOutlet weak var _searchTextField: UITextField!
    @IBOutlet weak var _crossBtn: UIButton!
    @IBOutlet weak var searchConstent: NSLayoutConstraint!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
