//
//  ParentClass.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Toast_Swift
import MBProgressHUD
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import REFrostedViewController
import FBSDKShareKit
import Malert
import SwiftTransition
import AppImageViewer
import Windless
import RxSwift
var notificationCount = ""

struct Example {
    var title: String
    var malert: Malert
}
class ParentClass: UIViewController,UIViewControllerTransitioningDelegate{
    
    let transition = MTransition()
    let navTranstion = MNavigationTranstion()

    let disposeBag = DisposeBag()
    
    var examples: [Example] = []

    @IBOutlet var _headerView: HeaderView!
    @IBOutlet var _footerView: FooterView!
    
    var controllerString : NSString = ""

    override func viewDidLoad() {
        super.viewDidLoad()

//        let userPromise : Promise =
        // Do any additional setup after loading the view.
    }
    
    
    @objc func stopAnimation(view : UITableView){
        view.windless.end()
        view.reloadData()
    }
    
    @objc func startAnimation(view : UITableView){
        view.windless
            .apply {
                $0.beginTime = 0.1
                $0.duration = 2
                $0.animationLayerOpacity = 0.5
            }
            .start()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    func setHeaderView(view : UIView){
        _headerView = (Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)![0] as! HeaderView)
        _headerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        if !_headerView.isDescendant(of: view) {
            view.addSubview(_headerView)
        }
        
        _headerView._slideButton.addTarget(self, action: #selector(clickSideMenu(sender:)), for: .touchUpInside)
        _headerView._searchBtn.addTarget(self, action: #selector(clickSearchBtn(sender:)), for: .touchUpInside)
        _headerView._wheelBtn.addTarget(self, action: #selector(clickWheelBtn(sender:)), for: .touchUpInside)
        _headerView._saveChangeBtn.addTarget(self, action: #selector(clickSaveBtn(sender:)), for: .touchUpInside)
        _headerView._crossBtn.addTarget(self, action: #selector(clickCrossBtn(sender:)), for: .touchUpInside)

    }
    
    func setFooterView(view : UIView){
        
        
        _footerView = (Bundle.main.loadNibNamed("FooterView", owner: nil, options: nil)![0] as! FooterView)
        _footerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        if !_footerView.isDescendant(of: view) {
            view.addSubview(_footerView)
        }
        _footerView._homeBtn.addTarget(self, action: #selector(clickHomeBtn(sender:)), for: .touchUpInside)
        _footerView._locationBtn.addTarget(self, action: #selector(clickLocationBtn(sender:)), for: .touchUpInside)
        _footerView._cameraBtn.addTarget(self, action: #selector(clickCameraBtn(sender:)), for: .touchUpInside)
        _footerView._notificationBtn.addTarget(self, action: #selector(clickNotificationBtn(sender:)), for: .touchUpInside)
//        _footerView._messageBtn.addTarget(self, action: #selector(clickMessageBtn(sender:)), for: .touchUpInside)
        
        
        if notificationCount != "" && notificationCount != "0"{
            self._footerView._countLbl.text = notificationCount
            self._footerView._countLbl.isHidden = false
        }
        else{
            self._footerView._countLbl.isHidden = true
        }
        

        

    }
    
    
    func openImageViewController(sender: UIImageView) {
        
        
        let appImage = ViewerImage.appImage(forImage: sender.image!)
        let viewer = AppImageViewer(originImage: sender.image!, photos: [appImage], animatedFromView: sender)
        viewer.delegate = self
        present(viewer, animated: true, completion: nil)

        
        
//        let viewObject = FullImageVC.init(nibName: "FullImageVC", bundle: nil)
////        viewObject.transitioningDelegate = self
////        viewObject.modalPresentationStyle = .custom
//        viewObject.image = sender.image
////        transition.startingPoint = sender.center
////        transition.circleColor = UIColor(patternImage: sender.image!)
//        self.present(viewObject, animated: true, completion: nil)

        
    }
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .present
//        return transition
//    }
//
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        transition.transitionMode = .dismiss
//        return transition
//    }
    
    @objc func clickHomeBtn(sender : UIButton){
        let navigationController = DEMONavigationController.init(rootViewController: HomeVC() as UIViewController)
        frostedViewController.contentViewController = navigationController
    }
    @objc func clickLocationBtn(sender : UIButton){
        let navigationController = DEMONavigationController.init(rootViewController: LocationVC() as UIViewController)
        frostedViewController.contentViewController = navigationController
    }
    @objc func clickCameraBtn(sender : UIButton){
        self.openCamera(controllerType: "home")
    }
    
    func openCamera(controllerType : NSString ) {
        controllerString = controllerType
        CameraManager.sharedInstance.PickImageFromCamera()
        CameraManager.sharedInstance.delegate = self
    }
    
    @objc func clickNotificationBtn(sender : UIButton){
        let navigationController = DEMONavigationController.init(rootViewController: NotificationVC() as UIViewController)
        frostedViewController.contentViewController = navigationController
    }
    @objc func clickMessageBtn(sender : UIButton){
        let navigationController = DEMONavigationController.init(rootViewController: DirectMessageVC() as UIViewController)
        frostedViewController.contentViewController = navigationController
    }
    
    
    @objc func clickSaveBtn(sender :UIButton){
        
    }
    @objc func clickSearchBtn(sender : UIButton) {
        if sender.tag == 11{
            let viewObject = EditProfileVC.init(nibName: "EditProfileVC", bundle: nil)
            self.navigationController?.pushViewController(viewObject, animated: true);
            
        }
        else
        {
            DispatchQueue.main.async {
                self._headerView._searchView.isHidden = false;
                self._headerView._searchTextField.text = "";
                self._headerView._searchTextField.placeholder = "Search User By Name"
                self._headerView._searchTextField.becomeFirstResponder()
            }

        }
    }
    
    
    @objc func clickCrossBtn(sender : UIButton) {
        DispatchQueue.main.async {
            self._headerView._searchView.isHidden = true;
            self._headerView._searchTextField.text = "";
            self._headerView._searchTextField.resignFirstResponder()
        }
        
    }
    @objc func clickWheelBtn(sender : UIButton) {
        if sender.tag == 11{
            let viewObject = UpdatePasswordVC.init(nibName: "UpdatePasswordVC", bundle: nil)
            self.navigationController?.pushViewController(viewObject, animated: true);
            
        }
        else
        {
            let navigationController = DEMONavigationController.init(rootViewController: GrabNChillWheelVC() as UIViewController)
            frostedViewController.contentViewController = navigationController

        }

    }
    @objc func clickSideMenu(sender : UIButton){
        if sender.tag == backButtonTag{
            self.navigationController?.popViewController(animated: true)
        }
        else if sender.tag == dismissButtonTag{
            if let child = self as? CommentListVC {
                child.check_api = false
            }
            self.dismiss(animated: true, completion: nil)
        }
        else{
            frostedViewController.presentMenuViewController()

        }
    }
    func showMessage(msg : String){
        self.view.makeToast(msg, duration: toastTimer, position: .center)
        
    }
    
    func loginMethod(value : String , typeOffLogin : String){
        APIManager.shared.getRequest(values: value, showIndicator: true, viewController: self) { (error, JsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    let dict = JsonData?.jsonDic!["data"]
                    GlobalMethod.shared.saveUserDefaultValue(key: LoginDataKey, value: dict as Any)
//                    if typeOffLogin != "FB"{
                        self.sendnextHomePage()
//                    }
                    GlobalMethod.shared.setUserData()
                    checkLoginWithFb = false
                }
                else{
                    self.view.makeToast((JsonData?.jsonDic!["message"] as! String), duration: toastTimer, position: .center)
                }
            }
        }
        
    }
    
    
    
    func sendnextHomePage(){
        let value = UserDefaults.standard.object(forKey: "RunAppCheck") as? String
        if value == "1"{
            let viewObject = HomeVC(nibName: "HomeVC", bundle: nil)
            let navigationController = DEMONavigationController.init(rootViewController: viewObject)
            let slide = SideMenuVC(nibName: "SideMenuVC", bundle: nil)
            let frostedViewController = REFrostedViewController(contentViewController: navigationController, menuViewController: slide)
            frostedViewController?.direction = .left
            frostedViewController?.delegate = self
            frostedViewController?.liveBlurBackgroundStyle = .light
            frostedViewController?.liveBlur = true
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.pushViewController(frostedViewController!, animated: false)
        }
        else{
            let viewObject = OnBoardingVC.init(nibName: "OnBoardingVC", bundle: nil)
            self.navigationController?.pushViewController(viewObject, animated: true)
        }
    }

    
    func loginFBClicked() {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile,.email,.userFriends], viewController: self) {
            loginResult in
            APIManager.shared.showProgress()

            switch loginResult {
            case .failed(let error):
                APIManager.shared.hideProgress()
                print(error)
            case .cancelled:
                APIManager.shared.hideProgress()

                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
//                APIManager.shared.hideProgress()

                if let currentaccessToken = AccessToken.current {
                    print(currentaccessToken)
                }
                let graphRequest:FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"first_name,last_name,email, picture.type(large)"])
                graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                    if ((error) == nil)
                    {
                        let data:[String:AnyObject] = result as! [String : AnyObject]
                        FBloginValue = String(format: "type=fbsignup&fb_id=%@&user_email=%@&user_firstname=%@&user_lastname=%@&user_device=i&user_token=%@",data["id"] as! String, data["email"] as! String,data["first_name"] as! String,data["last_name"] as! String,fcm_reg_id)
//                        checkLoginWithFb = true
//                        self.sendnextHomePage()
                          APIManager.shared.hideProgress()
                         self.loginMethod(value: FBloginValue,typeOffLogin: "FB")


                    }
                })
            }
        }
    }
    
    
    
    func updateNearByRestaurant(){
        
//        http://www.letsgrubnchill.com/api/Api.php?app=grubnchill&type=insertRestaurants&lat=13.067886&long=77.5836129
        let lat = UserDefaults.standard.object(forKey: "lat")
        let long = UserDefaults.standard.object(forKey: "long")
        if lat == nil && long == nil {
            return
        }
        
        APIManager.shared.getRequest(values: "type=insertRestaurants&lat=\(String(describing: lat!))&long=\(String(describing: long!))", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                
                print(jsonData?.jsonDic)
                
            }
        }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func shareONFacebook(){
        
        var url = ""
        if sharePost?.review_image!  != "" {
            url = (sharePost?.review_image)!
        }
        else{
            url = (sharePost?.review_image)!
        }
        let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = URL(string: url)
        content.quote = sharePost?.review_text
        let dialog : FBSDKShareDialog = FBSDKShareDialog()
        dialog.fromViewController = self
        dialog.shareContent = content
        dialog.delegate = self
        dialog.show()
    }
    
    
    
    //MARK: - Get Notification Count Method
    func getNotificationCount(){
        APIManager.shared.getRequest(values: "type=mynotificationcounter&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    print(jsonData?.jsonDic)
                    let count = (jsonData?.jsonDic!["data"] as! NSDictionary)["count"]
                    
                    notificationCount = getString(string: count as Any)
                    if getString(string: count as Any) != "" && getString(string: count as Any) != "0"{
                        self._footerView._countLbl.text = getString(string: count as Any)
                        self._footerView._countLbl.isHidden = false
                    }
                    else{
                       self._footerView._countLbl.isHidden = true
                    }
 

                    
                }
            }
        }
    }
    
    //MARK: - Remove Notification Count Method
    func removeNotificationCount(){
        APIManager.shared.getRequest(values: "type=mynotificationstatus&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    print(jsonData?.jsonDic)
                    notificationCount = "0"
                    self._footerView._countLbl.isHidden = true
                }
            }
        }
    }

}


extension ParentClass : FBSDKSharingDelegate {
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print("FB: SHARE RESULTS : - \(results.debugDescription)")
        self.showMessage(msg: "Share on facebook successfully!!")
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print("Error : - \(error.localizedDescription)")
        self.showMessage(msg: "Not Share on facebook. Please Try Again")
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        print("Cancel")
    }
    
    
    
}
extension ParentClass : REFrostedViewControllerDelegate {
    func frostedViewController(_ frostedViewController: REFrostedViewController!, didRecognizePanGesture recognizer: UIPanGestureRecognizer!) {
        
    }
    func frostedViewController(_ frostedViewController: REFrostedViewController!, willShowMenuViewController menuViewController: UIViewController!) {
        
    }
    
    func frostedViewController(_ frostedViewController: REFrostedViewController!, didShowMenuViewController menuViewController: UIViewController!) {
        
    }
    func frostedViewController(_ frostedViewController: REFrostedViewController!, didHideMenuViewController menuViewController: UIViewController!) {
        
    }
    func frostedViewController(_ frostedViewController: REFrostedViewController!, willHideMenuViewController menuViewController: UIViewController!) {
        
    }
    
}


extension ParentClass : CameraManagerDelegate{
    func didEndChoose(image: UIImage) {
       
        if let child = self as? ReviewPostVC {
            child.headerSetOnTableView(image: image)
            self.dismiss(animated: true, completion: nil)
        }
        else if let child = self as? EditProfileVC {
            child.headerSetOnTableView(image: image)
            self.dismiss(animated: true, completion: nil)

        }
        else{
            let reviewPostVC = ReviewPostVC.init(nibName: "ReviewPostVC", bundle: nil)
            reviewPostVC.postType = "Create"
            reviewPostVC.presentType = "CameraOpen"
            reviewPostVC.image = image
            self.navigationController?.pushViewController(reviewPostVC, animated: false);
        }


    }
}


extension ParentClass : AppImageViewerDelegate {
    func didTapShareButton(atIndex index: Int, _ browser: AppImageViewer) {
        print("share button tapped")
    }
    
    func didTapMoreButton(atIndex index: Int, _ browser: AppImageViewer) {
        
    }
}


extension ParentClass {
    
    func sendCommentListView(obj : homeObjectData){
        var postObj = obj
        let viewObjet = CommentListVC.init(nibName: "CommentListVC", bundle: nil)
        viewObjet.postObject = obj
        let nvc = UINavigationController(rootViewController: viewObjet)
        nvc.isNavigationBarHidden = true
        
        viewObjet.delegate = self as! CommentDelegate;
//        
//        viewObjet.updateCommentCountOberver.subscribe({ commentCount in
//            postObj.review_commentcount = commentCount as? String
//        }).disposed(by: disposeBag)

        
        self.present(nvc, animated: true, completion: nil)
    }
    
    
    
}
