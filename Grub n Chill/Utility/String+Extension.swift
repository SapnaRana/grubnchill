//
//  String+Extension.swift
//  Grub n Chill
//
//  Created by orangemac04 on 10/04/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit

extension String {
    
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
    
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
    
    
    var isValidEmail : Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    
    func isValidText() -> Bool{
        if self == ""{
            return false
        }
        else if self.replacingOccurrences(of: " ", with: "") == ""{
            return false
        }
        else if self.replacingOccurrences(of: ".", with: "") == ""{
            return false
        }
        else if self.count == 0{
            return false
        }
        return true
    }

    
    
    func dateFromString(appformate : String , serverFormate : String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = appformate
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let getdate = formatter.date(from: self)
        formatter.dateFormat = serverFormate
        let lastValue = formatter.string(from: getdate!)
        return lastValue
    }
    
    func stringFromDate(datePicker : UIDatePicker) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = self
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let date = formatter.string(from: datePicker.date)
        return date
    }
    
    
}


