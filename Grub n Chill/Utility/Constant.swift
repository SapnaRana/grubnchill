
//
//  Constant.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

var fcm_reg_id = "123"

let toastTimer                     = 1.0


let GoogleBaseUrl                  =  "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"

let BaseUrl                        =  "http://www.letsgrubnchill.com/api/Api.php?app=grubnchill&"
//let BaseUrl                        =  "http://www.pixeltoweb.com/grubnchill/Api/Api.php?app=grubnchill&"
let loginUrl                       =  "type=login&user_email=%@&user_pass=%@"
let getAvatarUrl                   =  "type=getavatar"
let getHomePageDataURL             =  "type=getreviews&page_num=1"

//MARK: - Identifire
let identifire                     =  "com.dem.Grub-n-Chill"
let NetworkError                   = "Please check your network connection."



var checkLoginWithFb               = false
var FBloginValue                     = ""


// User Default Key
let LoginDataKey                   = "loginDetail"
let profileObjectKey               = "UserProfieData"


let backButtonTag                  = 11
let slideButtonTag                 = 22
let dismissButtonTag               = 33

var profileObject                  : userProfileOBj!
var avatarObj                      : avatarObjectData!
var sharePost                      :  homeObjectData?

var blogCategoryArray              =  [blogCategoryObject]()




//var selectedRestaurant             = ""
//var selectedFoodCategory           = ""
//var selectedFoodType               = ""


//var selectedRestaurantObj          : restaurantListObject!


 let selectedRestaurantObj = PublishSubject<restaurantListObject>()


var selectedFoodCategoryObj        = [foodCategoryObject]()
var selectedFoodTypeObj            = [foodTypeObject]()


var OTPValue                       = ""


let appDateFormate                 = "dd MMM, yyyy"
let serverDateFormate              = "dd-MM-yyyy"



let topColor = UIColor(red: 70.0/255.0, green: 48.0/255.0, blue: 133.0/255.0, alpha: 1.0)
