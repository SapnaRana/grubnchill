//
//  AvatarCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 24/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class AvatarCell: UICollectionViewCell {
    @IBOutlet weak var _imageView: UIImageView!
    @IBOutlet weak var _backView: UIView!
    @IBOutlet weak var _checkView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _backView.layer.cornerRadius = _backView.bounds.width / 2
        _backView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _backView.layer.borderWidth = 1
        _checkView.layer.cornerRadius = _checkView.bounds.width / 2

    }

}
