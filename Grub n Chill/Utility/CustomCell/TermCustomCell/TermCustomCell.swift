//
//  TermCustomCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import WebKit

class TermCustomCell: UITableViewCell {
    @IBOutlet weak var _centerView: UIView!
    @IBOutlet weak var _textView: UITextView!
    @IBOutlet weak var _webView: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _centerView.layer.cornerRadius = 15

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
