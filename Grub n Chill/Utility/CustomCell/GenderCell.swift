//
//  GenderCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 25/12/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class GenderCell: UITableViewCell {
    @IBOutlet weak var _maleButton: UIButton!
    @IBOutlet weak var _femaleButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
