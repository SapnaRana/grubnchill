//
//  FoodCategoryCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 22/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class FoodCategoryCell: UITableViewCell {
    @IBOutlet weak var _nameLbl: UILabel!
    @IBOutlet weak var _checkIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
