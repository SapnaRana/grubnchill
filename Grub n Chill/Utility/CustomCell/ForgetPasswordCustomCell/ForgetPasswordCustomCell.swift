//
//  ForgetPasswordCustomCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 16/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import MaterialTextField

class ForgetPasswordCustomCell: UITableViewCell {
    @IBOutlet weak var _centerView: UIView!
    @IBOutlet weak var _emailTxtField: MFTextField!
    @IBOutlet weak var _SendButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        _centerView.layer.cornerRadius = 15
        _SendButton.layer.cornerRadius = 20

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
