//
//  LoginCustomCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import MaterialTextField
class LoginCustomCell: UITableViewCell {
    @IBOutlet weak var _emailtxtField: MFTextField!
    @IBOutlet weak var _passwordTxtField: MFTextField!
    @IBOutlet weak var _forgetButton: UIButton!
    @IBOutlet weak var _signInButton: UIButton!
    @IBOutlet weak var _facebookButton: UIButton!
    @IBOutlet weak var centerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        centerView.layer.cornerRadius = 15
        _signInButton.layer.cornerRadius = 20
        _facebookButton.layer.cornerRadius = 20

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
