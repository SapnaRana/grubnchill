
//
//  TextFieldCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 23/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import MaterialTextField
class TextFieldCell: UITableViewCell {
    @IBOutlet weak var _txtField: MFTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
