//
//  RatingCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 03/12/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import HCSStarRatingView

class RatingCell: UITableViewCell {
//    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingView.allowsHalfStars = false

        // Initialization code
//        ratingView.contentMode = UIView.ContentMode.scaleAspectFit
//        ratingView.type = .wholeRatings


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
