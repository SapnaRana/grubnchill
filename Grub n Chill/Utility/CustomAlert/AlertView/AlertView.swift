//
//  AlertView.swift
//  Grub n Chill
//
//  Created by orangemac04 on 21/10/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
@objc public protocol AlertVDelegate {
    // MARK: - Delegate functions
    @objc optional func didCancelClick()
    @objc optional func didOkClick(title : String , tag : Int)
    
    
}
class AlertView: UIView {
    @IBOutlet weak var _imageVIew: UIImageView!
    @IBOutlet weak var _titleLbl: UILabel!
    @IBOutlet weak var _cancelBtn: UIButton!
    @IBOutlet weak var _oKbtn: UIButton!
    public weak var delegate : AlertVDelegate?

    var cancelString = ""
    var okString     = ""

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        _imageVIew.layer.cornerRadius = _imageVIew.bounds.width / 2
        _imageVIew.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _imageVIew.layer.borderWidth = 1

    }
    
    func btnSetup(cancel : String , ok : String ){
        _cancelBtn.setTitle(cancel, for: .normal)
        _oKbtn.setTitle(ok, for: .normal)

    }
    
    class func instantiateFromNib() -> AlertView {
        return Bundle.main.loadNibNamed("AlertView", owner: nil, options: nil)!.first as! AlertView
    }
    @IBAction func OkClick(_ sender: UIButton) {
        delegate?.didOkClick!(title: (sender.titleLabel?.text)! , tag: sender.tag)
    }
    
    @IBAction func cancelClick(_ sender: Any) {
        delegate?.didCancelClick!()

    }
}
