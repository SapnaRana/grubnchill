//
//  CustomCameraPopUp.swift
//  Grub n Chill
//
//  Created by orangemac04 on 25/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

@objc public protocol CustomCameraPopUpDelegate {
    // MARK: - Delegate functions
    @objc optional func didCameraClick()
    @objc optional func didGalleryClick()

    
}
class CustomCameraPopUp: UIView {
    @IBOutlet weak var _centerView: UIView!
    public weak var delegate : CustomCameraPopUpDelegate?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        applyLayout()
    }
    
    @IBAction func _galleryClick(_ sender: Any) {
        delegate?.didGalleryClick!()
    }
    @IBAction func _cameraClick(_ sender: Any) {
        delegate?.didCameraClick!()
    }
  
    private func applyLayout() {
        
        _centerView.backgroundColor = UIColor.black
        _centerView.layer.shadowOffset = CGSize.zero
        _centerView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.6).cgColor
        _centerView.layer.shadowOpacity = 1
        _centerView.layer.shadowRadius = 120
        
      
    }
    
    
    class func instantiateCustomFromNib() -> CustomCameraPopUp {
        return Bundle.main.loadNibNamed("CustomCameraPopUp", owner: nil, options: nil)!.first as! CustomCameraPopUp
    }
    
   
}
