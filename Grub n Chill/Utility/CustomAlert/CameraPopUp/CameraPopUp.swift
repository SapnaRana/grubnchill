//
//  CameraPopUp.swift
//  Grub n Chill
//
//  Created by orangemac04 on 24/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
@objc public protocol CameraPopUpDelegate {
    // MARK: - Delegate functions
    @objc optional func didAvatarClick()
    @objc optional func didGalleryClick()
    @objc optional func didCameraClick()
    @objc optional func didDeleteClick()


}
class CameraPopUp: UIView {
    @IBOutlet weak var _centerView: UIView!
    public weak var delegate : CameraPopUpDelegate?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        applyLayout()
    }
    
    @IBAction func _deleteClick(_ sender: Any) {
        delegate?.didDeleteClick!()
    }
    @IBAction func _cameraClick(_ sender: Any) {
        delegate?.didCameraClick!()
    }
    @IBAction func _galleryClick(_ sender: Any) {
        delegate?.didGalleryClick!()
    }
    @IBAction func _clickAvatar(_ sender: Any) {
        
        delegate?.didAvatarClick!()
    }
    private func applyLayout() {
        
        _centerView.backgroundColor = UIColor.black
        _centerView.layer.shadowOffset = CGSize.zero
        _centerView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.6).cgColor
        _centerView.layer.shadowOpacity = 1
        _centerView.layer.shadowRadius = 120

        
        //TODO PUT AN IMAGE
//        imageView.image = UIImage.fromColor(color: UIColor(red:0.91, green:0.12, blue:0.39, alpha:1.0))
//
//        titleLabel.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.light)
//        titleLabel.text = "Three Invites"
//
//        descriptionLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
//        descriptionLabel.textColor = UIColor.lightGray
//        descriptionLabel.numberOfLines = 0
//        descriptionLabel.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet ante ut massa dignissim feugiat. Morbi eu faucibus diam. Nunc et nisl et tellus ultrices blandit. Proin pharetra hendrerit augue sed tempus. Aliquam vel nibh laoreet tortor euismod tempus. Integer et pharetra magna."
    }
    

    class func instantiateCustomFromNib() -> CameraPopUp {
        return Bundle.main.loadNibNamed("CustomCameraPopUp", owner: nil, options: nil)!.first as! CameraPopUp
    }
    
    class func instantiateFromNib() -> CameraPopUp {
        return Bundle.main.loadNibNamed("CameraPopUp", owner: nil, options: nil)!.first as! CameraPopUp
    }
}
