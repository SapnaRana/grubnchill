//
//  GlobalMethod.swift
//  Grub n Chill
//
//  Created by orangemac04 on 05/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class GlobalMethod: NSObject {
    @objc static let shared   = GlobalMethod()
    static var userDetail     : userDetailOBj?
    
    func setUserData(){
        let dict = getUserdefaultData(key: LoginDataKey) as! NSDictionary
        GlobalMethod.userDetail = userDetailOBj.init(json: dict as! [String : Any])
    }
    
    func getUserProfileObject(){
        let value = getUserdefaultData(key: profileObjectKey) as! NSDictionary
        profileObject = userProfileOBj.init(json: value as! [String : Any])
    }
    
    func saveUserDefaultValue(key : String , value : Any){
        let data = NSKeyedArchiver.archivedData(withRootObject:value)
        let defaults = UserDefaults.standard
        defaults.set(data, forKey: key)
    }
    
 
    
    func getUserdefaultData(key : String) -> Any{
        let detailGet = UserDefaults.standard.object(forKey: key) as? NSData
        var dict = NSDictionary()
        if let placesData = detailGet {
            dict = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as! NSDictionary
        }
        return dict as Any
    }
    
    func removeDefultsValue(){
        UserDefaults.standard.removeObject(forKey: LoginDataKey)
        UserDefaults.standard.removeObject(forKey: profileObjectKey)

    }
    
}
