//
//  UIManager.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//


import UIKit


class UIManager: NSObject {
    
    @objc  static let shared = UIManager()

    //******** Check ViewController in NavigationController Stack *****//
    func checkNavigationController(getviewController : AnyClass , navigation : UINavigationController) -> UIViewController {
        let viewContrllers = navigation.viewControllers as [AnyObject]
        let contArray = viewContrllers.filter({ m in
            m.isKind(of: getviewController)
        })
        if contArray.count > 0{
            return  contArray.first as! UIViewController
        }
        return UIViewController()
        
    }
}

extension UIView {
    
    func roundcornerWithout(){
        self.layer.cornerRadius = 20
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 1
        self.backgroundColor = UIColor.clear
    }
    
    func roundcorner(){
        self.layer.cornerRadius = 20
        self.layer.borderColor = UIColor.white.cgColor
        self.backgroundColor = UIColor.white
        
        self.layer.borderWidth = 1
        self.clipsToBounds = true
    }
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
                self.clipsToBounds = false
            }
        }
    }
    
    
    
    
    func addShadow(shadowColor: CGColor = UIColor.lightGray.cgColor,
                   shadowOffset: CGSize = CGSize(width: 0, height: 0),
                   shadowOpacity: Float = 0.5,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}
