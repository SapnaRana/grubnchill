//
//  StructClass.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import Foundation
import UIKit

func getString(string : Any) -> String{
    if string is NSNull{
        return ""
    }
    else if string is NSNumber{
        return (string as! NSNumber).stringValue
    }
    else if string is Int{
        return ("\(string as! Int)")
    }

    else {
        return string as! String
    }
}

struct userDetailOBj {
    let firstname          : String?
    let lastname           : String?
    var user_email         : String?
    let user_id            : String?
    let username           : String?
}

extension userDetailOBj{
    init(json : [String : Any]) {
        self.firstname           = json["firstname"] != nil ? getString(string: json["firstname"] as Any) : ""
        self.lastname            = json["lastname"] != nil ? getString(string: json["lastname"] as Any) : ""
        self.user_email          = json["user_email"] != nil ? getString(string: json["user_email"] as Any) : ""
        self.user_id             = json["user_id"] != nil ? getString(string: json["user_id"] as Any) : ""
        self.username            = json["username"] != nil ? getString(string: json["username"] as Any) : ""
    }
}

struct userProfileOBj {
    let user_address1          : String?
    let user_address2           : String?
    var user_bio         : String?
    let user_city            : String?
    let user_country           : String?
    
    let user_dob          : String?
    let user_email           : String?
    var user_firstname         : String?
    let user_id            : String?
    let user_lastname           : String?
    let user_gender           : String?

    
    let user_mobile          : String?
    let user_phone           : String?
    var user_postcode         : String?
    let user_profilepicture            : String?
    let user_state           : String?
    let username           : String?


}

extension userProfileOBj{
    init(json : [String : Any]) {
        self.user_address1           = json["user_address1"] != nil ? getString(string: json["user_address1"] as Any) : ""
        self.user_address2            = json["user_address2"] != nil ? getString(string: json["user_address2"] as Any) : ""
        self.user_bio          = json["user_bio"] != nil ? getString(string: json["user_bio"] as Any) : ""
        self.user_city             = json["user_city"] != nil ? getString(string: json["user_city"] as Any) : ""
        self.user_country            = json["user_country"] != nil ? getString(string: json["user_country"] as Any) : ""
        self.user_dob           = json["user_dob"] != nil ? getString(string: json["user_dob"] as Any) : ""
        self.user_email            = json["user_email"] != nil ? getString(string: json["user_email"] as Any) : ""
        self.user_firstname          = json["user_firstname"] != nil ? getString(string: json["user_firstname"] as Any) : ""
        self.user_id             = json["user_id"] != nil ? getString(string: json["user_id"] as Any) : ""
        self.user_lastname            = json["user_lastname"] != nil ? getString(string: json["user_lastname"] as Any) : ""
        
        self.user_mobile           = json["user_mobile"] != nil ? getString(string: json["user_mobile"] as Any) : ""
        self.user_phone            = json["user_phone"] != nil ? getString(string: json["user_phone"] as Any) : ""
        self.user_postcode          = json["user_postcode"] != nil ? getString(string: json["user_postcode"] as Any) : ""
        self.user_profilepicture             = json["user_profilepicture"] != nil ? getString(string: json["user_profilepicture"] as Any) : ""
        self.user_state            = json["user_state"] != nil ? getString(string: json["user_state"] as Any) : ""
        self.username            = json["username"] != nil ? getString(string: json["username"] as Any) : ""
        self.user_gender            = json["user_gender"] != nil ? getString(string: json["user_gender"] as Any) : ""


    }
}


struct onBoardingDataObject {
    let page_content          : String?
    let page_image        : String?
    let page_title        : String?

}

extension onBoardingDataObject{
    init(json : [String : Any]) {
        self.page_content           = json["page_content"] != nil ? getString(string: json["page_content"] as Any) : ""
        self.page_image         = json["page_image"] != nil ? getString(string: json["page_image"] as Any) : ""
        self.page_title         = json["page_title"] != nil ? getString(string: json["page_title"] as Any) : ""

    }
}


struct avatarObjectData {
    let id          : String?
    let path        : String?
}

extension avatarObjectData{
    init(json : [String : Any]) {
        self.id           = json["id"] != nil ? getString(string: json["id"] as Any) : ""
        self.path         = json["path"] != nil ? getString(string: json["path"] as Any) : ""
    }
}


struct countryObjectData {
    let id          : String?
    let name        : String?
}

extension countryObjectData{
    init(json : [String : Any]) {
        self.id           = json["id"] != nil ? getString(string: json["id"] as Any) : ""
        self.name         = json["name"] != nil ? getString(string: json["name"] as Any) : ""
    }
}



struct notificationObjectData {
    let current_userid          : String?
    let follow_userid        : String?
    let notification_id          : String?
    let type        : String?
    let user_firstname          : String?
    let user_lastname        : String?
    let user_message          : String?
    let user_profilepicture        : String?
    let review_id        : String?

}

extension notificationObjectData{
    init(json : [String : Any]) {
        self.current_userid        = json["current_userid"] != nil ? getString(string: json["current_userid"] as Any) : ""
        self.review_id             = json["review_id"] != nil ? getString(string: json["review_id"] as Any) : ""
        self.follow_userid         = json["follow_userid"] != nil ? getString(string: json["follow_userid"] as Any) : ""
        self.notification_id       = json["notification_id"] != nil ? getString(string: json["notification_id"] as Any) : ""
        self.type                  = json["type"] != nil ? getString(string: json["type"] as Any) : ""
        self.user_firstname        = json["user_firstname"] != nil ? getString(string: json["user_firstname"] as Any) : ""
        self.user_lastname         = json["user_lastname"] != nil ? getString(string: json["user_lastname"] as Any) : ""
        self.user_message          = json["user_message"] != nil ? getString(string: json["user_message"] as Any) : ""
        self.user_profilepicture   = json["user_profilepicture"] != nil ? getString(string: json["user_profilepicture"] as Any) : ""
    }
}




struct commentObjectData {
    let following          : String?
    let following_id        : String?
    let reviewscomment_id          : String?
    let reviewscomment_reviewid        : String?
    let reviewscomment_userid          : String?
    let user_firstname        : String?
    let user_lastname          : String?
    let user_profilepicture        : String?
    var reviewscomment_comment        : String?
    let reviewscomment_date        : String?

    
}

extension commentObjectData{
    init(json : [String : Any]) {
        self.following           = json["following"] != nil ? getString(string: json["following"] as Any) : ""
        self.following_id         = json["following_id"] != nil ? getString(string: json["following_id"] as Any) : ""
        self.reviewscomment_id           = json["reviewscomment_id"] != nil ? getString(string: json["reviewscomment_id"] as Any) : ""
        self.reviewscomment_reviewid         = json["reviewscomment_reviewid"] != nil ? getString(string: json["reviewscomment_reviewid"] as Any) : ""
        self.reviewscomment_userid           = json["reviewscomment_userid"] != nil ? getString(string: json["reviewscomment_userid"] as Any) : ""
        self.user_firstname         = json["user_firstname"] != nil ? getString(string: json["user_firstname"] as Any) : ""
        self.user_lastname           = json["user_lastname"] != nil ? getString(string: json["user_lastname"] as Any) : ""
        self.user_profilepicture         = json["user_profilepicture"] != nil ? getString(string: json["user_profilepicture"] as Any) : ""
        self.reviewscomment_comment         = json["reviewscomment_comment"] != nil ? getString(string: json["reviewscomment_comment"] as Any) : ""
        self.reviewscomment_date         = json["reviewscomment_date"] != nil ? getString(string: json["reviewscomment_date"] as Any) : ""

    }
}


struct shareUserObjectData {
    let following          : String?
    let following_id        : String?
    let reviewsshared_id          : String?
    let reviewsshared_reviewid        : String?
    let reviewsshared_userid          : String?
    let user_firstname        : String?
    let user_lastname          : String?
    let user_profilepicture        : String?
    
}

extension shareUserObjectData{
    init(json : [String : Any]) {
        self.following           = json["following"] != nil ? getString(string: json["following"] as Any) : ""
        self.following_id         = json["following_id"] != nil ? getString(string: json["following_id"] as Any) : ""
        self.reviewsshared_id           = json["reviewsshared_id"] != nil ? getString(string: json["reviewsshared_id"] as Any) : ""
        self.reviewsshared_reviewid         = json["reviewsshared_reviewid"] != nil ? getString(string: json["reviewsshared_reviewid"] as Any) : ""
        self.reviewsshared_userid           = json["reviewsshared_userid"] != nil ? getString(string: json["reviewsshared_userid"] as Any) : ""
        self.user_firstname         = json["user_firstname"] != nil ? getString(string: json["user_firstname"] as Any) : ""
        self.user_lastname           = json["user_lastname"] != nil ? getString(string: json["user_lastname"] as Any) : ""
        self.user_profilepicture         = json["user_profilepicture"] != nil ? getString(string: json["user_profilepicture"] as Any) : ""
        
    }
}


struct likeUserObjectData {
    var following                  : String?
    let following_id               : String?
    let reviewliked_id             : String?
    let reviewliked_reviewid       : String?
    let reviewliked_userid         : String?
    let user_firstname             : String?
    let user_lastname              : String?
    let user_profilepicture        : String?
    var following_status           : String?

}

extension likeUserObjectData{
    init(json : [String : Any]) {
        self.following           = json["following"] != nil ? getString(string: json["following"] as Any) : ""
        self.following_id         = json["following_id"] != nil ? getString(string: json["following_id"] as Any) : ""
        self.reviewliked_id           = json["reviewliked_id"] != nil ? getString(string: json["reviewliked_id"] as Any) : ""
        self.reviewliked_reviewid         = json["reviewliked_reviewid"] != nil ? getString(string: json["reviewliked_reviewid"] as Any) : ""
        self.reviewliked_userid           = json["reviewliked_userid"] != nil ? getString(string: json["reviewliked_userid"] as Any) : ""
        self.user_firstname         = json["user_firstname"] != nil ? getString(string: json["user_firstname"] as Any) : ""
        self.user_lastname           = json["user_lastname"] != nil ? getString(string: json["user_lastname"] as Any) : ""
        self.user_profilepicture         = json["user_profilepicture"] != nil ? getString(string: json["user_profilepicture"] as Any) : ""
        self.following_status         = json["following_status"] != nil ? getString(string: json["following_status"] as Any) : "0"


    }
}

struct followingUserData {
    var following            : String?
    let following_status     : String?
    var user_firstname       : String?
    var user_lastname        : String?
    var user_profilepicture  : String?
    var user_id              : String?

}

extension followingUserData{
    init(json : [String : Any]) {
        self.following               = json["following"] != nil ? getString(string: json["following"] as Any) : ""
        self.following_status        = json["following_status"] != nil ? getString(string: json["following_status"] as Any) : ""
        self.user_firstname          = json["user_firstname"] != nil ? getString(string: json["user_firstname"] as Any) : ""
        self.user_lastname           = json["user_lastname"] != nil ? getString(string: json["user_lastname"] as Any) : ""
        self.user_profilepicture     = json["user_profilepicture"] != nil ? getString(string: json["user_profilepicture"] as Any) : ""
        self.user_id                 = json["user_id"] != nil ? getString(string: json["user_id"] as Any) : ""


    }
}



struct foodTypeObject {
    var foodtype_id          : String?
    let foodtype_name        : String?
    var selected             : String?

}

extension foodTypeObject{
    init(json : [String : Any]) {
        self.foodtype_id           = json["foodtype_id"] != nil ? getString(string: json["foodtype_id"] as Any) : ""
        self.foodtype_name         = json["foodtype_name"] != nil ? getString(string: json["foodtype_name"] as Any) : ""
        self.selected              = "0"
    }
}

struct foodCategoryObject {
    var foodcategory_id          : String?
    let foodcategory_name        : String?
    var selected                 : String?

}

extension foodCategoryObject{
    init(json : [String : Any]) {
        self.foodcategory_id           = json["foodcategory_id"] != nil ? getString(string: json["foodcategory_id"] as Any) : ""
        self.foodcategory_name         = json["foodcategory_name"] != nil ? getString(string: json["foodcategory_name"] as Any) : ""
        self.selected                  = "0"

    }
}

struct blogCategoryObject {
    var category_id          : String?
    let category_name        : String?
}

extension blogCategoryObject{
    init(json : [String : Any]) {
        self.category_id           = json["category_id"] != nil ? getString(string: json["category_id"] as Any) : ""
        self.category_name         = json["category_name"] != nil ? getString(string: json["category_name"] as Any) : ""
    }
}

struct blogListObject {
    var post_content          : String?
    let post_id        : String?
    let post_image        : String?
    let post_title        : String?
    let post_url        : String?

}

extension blogListObject{
    init(json : [String : Any]) {
        self.post_content           = json["post_content"] != nil ? getString(string: json["post_content"] as Any) : ""
        self.post_id         = json["post_id"] != nil ? getString(string: json["post_id"] as Any) : ""
        self.post_image           = json["post_image"] != nil ? getString(string: json["post_image"] as Any) : ""
        self.post_title         = json["post_title"] != nil ? getString(string: json["post_title"] as Any) : ""
        self.post_url           = json["post_url"] != nil ? getString(string: json["post_url"] as Any) : ""
    }
}



struct restaurantListObject {
    var restaurant_distance          : String?
    let restaurant_foodtype        : String?
    let restaurant_icon        : String?
    let restaurant_id        : String?
    let restaurant_name        : String?
    let restaurant_rating        : String?

}

extension restaurantListObject{
    init(json : [String : Any]) {
        self.restaurant_distance           = json["restaurant_distance"] != nil ? getString(string: json["restaurant_distance"] as Any) : ""
        self.restaurant_foodtype         = json["restaurant_foodtype"] != nil ? getString(string: json["restaurant_foodtype"] as Any) : ""
        self.restaurant_icon           = json["restaurant_icon"] != nil ? getString(string: json["restaurant_icon"] as Any) : ""
        self.restaurant_id         = json["restaurant_id"] != nil ? getString(string: json["restaurant_id"] as Any) : ""
        self.restaurant_name           = json["restaurant_name"] != nil ? getString(string: json["restaurant_name"] as Any) : ""
        self.restaurant_rating           = json["restaurant_rating"] != nil ? getString(string: json["restaurant_rating"] as Any) : "0.0"

    }
}


struct homeObjectData  {
    var review_commentcount          : String?
    let review_date                  : String?
    let review_id                    : String?
    let review_image                 : String?
    var review_likedcount            : String?
    var review_sharedcount           : String?
    var review_text                  : String?
    let review_userimage             : String?
    let review_username              : String?
    let user_id                      : String?
    let restaurant_address           : String?
    var restaurant_foodcategory      : String?
    var restaurant_foodtype          : String?
    var restaurant_name              : String?
//    var restaurant_rating            : String?
    var review_rating                : String?
    let review_contenttype           : String?
    var review_userliked             : String?
    let review_userlikedid           : String?
    let user_profilepicture          : String?
    var restaurant_foodcategoryid    : String?
    var restaurant_id                : String?

}

extension homeObjectData{
    init(json : [String : Any]) {
        self.review_commentcount           = json["review_commentcount"] != nil ? getString(string: json["review_commentcount"] as Any) : "0"
        self.review_date         = json["review_date"] != nil ? getString(string: json["review_date"] as Any) : ""
        self.review_id           = json["review_id"] != nil ? getString(string: json["review_id"] as Any) : ""
        self.review_likedcount         = json["review_likedcount"] != nil ? getString(string: json["review_likedcount"] as Any) : "0"
        self.review_image           = json["review_image"] != nil ? getString(string: json["review_image"] as Any) : ""
        self.review_sharedcount         = json["review_sharedcount"] != nil ? getString(string: json["review_sharedcount"] as Any) : "0"
        self.review_text           = json["review_text"] != nil ? getString(string: json["review_text"] as Any) : ""
        self.review_userimage         = json["review_userimage"] != nil ? getString(string: json["review_userimage"] as Any) : ""
        self.review_username           = json["review_username"] != nil ? getString(string: json["review_username"] as Any) : ""
        self.user_id         = json["user_id"] != nil ? getString(string: json["user_id"] as Any) : ""
        
        self.restaurant_address         = json["restaurant_address"] != nil ? getString(string: json["restaurant_address"] as Any) : ""
        self.restaurant_foodcategory         = json["restaurant_foodcategory"] != nil ? getString(string: json["restaurant_foodcategory"] as Any) : ""
        self.restaurant_foodtype         = json["restaurant_foodtype"] != nil ? getString(string: json["restaurant_foodtype"] as Any) : ""
        self.restaurant_name         = json["restaurant_name"] != nil ? getString(string: json["restaurant_name"] as Any) : ""
        self.review_rating         = json["review_rating"] != nil ? getString(string: json["review_rating"] as Any) : "0.0"
        self.review_contenttype         = json["review_contenttype"] != nil ? getString(string: json["review_contenttype"] as Any) : ""
        self.review_userliked         = json["review_userliked"] != nil ? getString(string: json["review_userliked"] as Any) : ""
        self.review_userlikedid         = json["review_userlikedid"] != nil ? getString(string: json["review_userlikedid"] as Any) : ""
        self.user_profilepicture         = json["user_profilepicture"] != nil ? getString(string: json["user_profilepicture"] as Any) : ""
        self.restaurant_foodcategoryid         = json["restaurant_foodcategoryid"] != nil ? getString(string: json["restaurant_foodcategoryid"] as Any) : ""
        self.restaurant_id         = json["restaurant_id"] != nil ? getString(string: json["restaurant_id"] as Any) : ""

    }
}
