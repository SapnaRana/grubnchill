//
//  FooterView.swift
//  Grub n Chill
//
//  Created by orangemac04 on 16/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class FooterView: UIView {
    @IBOutlet weak var _homeBtn: UIButton!
    @IBOutlet weak var _locationBtn: UIButton!
    @IBOutlet weak var _cameraBtn: UIButton!
    @IBOutlet weak var _notificationBtn: UIButton!
    @IBOutlet weak var _messageBtn: UIButton!
    @IBOutlet weak var _countLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _countLbl.layer.cornerRadius = _countLbl.bounds.width / 2
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
