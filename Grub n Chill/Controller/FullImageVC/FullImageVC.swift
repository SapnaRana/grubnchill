//
//  FullImageVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 06/04/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit

class FullImageVC: UIViewController {
    @IBOutlet weak var _imageView: UIImageView!
    @IBOutlet weak var _backBtn: UIButton!
    var image : UIImage!
    var pathImage : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _imageView.image = image
        _backBtn.setImage(UIImage(named: "whiteCrossIcon"), for: .normal);
        // Do any additional setup after loading the view.
    }


    @IBAction func backBtnClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
