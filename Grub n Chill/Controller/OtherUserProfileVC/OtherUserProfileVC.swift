//
//  OtherUserProfileVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 23/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class OtherUserProfileVC: ParentClass {
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _topView: UIView!
    @IBOutlet var _tableHeaderView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeaderView(view: _topView)
        _tableView.tableHeaderView = _tableHeaderView
        _headerView._slideButton.setImage(UIImage(named: "back-arrow"), for: .normal);
        _headerView._slideButton.tag = backButtonTag
        _headerView._icon.isHidden   = true
        _headerView._wheelBtn.isHidden = true

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OtherUserProfileVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 0 {
            let identifier = "HomeImageCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeImageCell
            if cell == nil {
                tableView.register(UINib(nibName: "HomeImageCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeImageCell
            }
            return cell!
        }
        else{
            let identifier = "HomeTextCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeTextCell
            if cell == nil {
                tableView.register(UINib(nibName: "HomeTextCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeTextCell
            }
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}
