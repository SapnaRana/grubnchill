//
//  ForgetPasswordVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import MaterialTextField
import Alamofire

class ForgetPasswordVC: ParentClass {
    @IBOutlet weak var _emailTxtField: MFTextField!
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet var _tableFooterView: UIView!
    @IBOutlet var _tableHeaderView: UIView!
    @IBOutlet weak var _signUpButton: UIButton!
    
    let valueDict = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        valueDict.setValue("", forKey: "email")
        _signUpButton.layer.cornerRadius = 20
        
        _tableView.tableHeaderView = _tableHeaderView
        _tableView.tableFooterView = _tableFooterView

        // Do any additional setup after loading the view.
    }


  
    @objc  @IBAction func sendClick(_ sender: Any) {
        self.view.endEditing(true)
        if valueDict["email"] == nil || (valueDict["email"] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter Email Address")
        }
        else if (valueDict["email"] as! String).isValidEmail == false{
            self.showMessage(msg: "Please Enter a Valid Email Address")
        }
            
        else{
            self.forgetPasswordMethod()
        }
    }
    
    //MARK:- Forget Password Method
    func forgetPasswordMethod(){
        
        let value = String(format: "type=forgotpassword&user_email=%@", valueDict["email"] as! String)
        APIManager.shared.getRequest(values: value, showIndicator: true, viewController: self) { (error, JsonData) in
            DispatchQueue.main.async {
                self.showMessage(msg: JsonData?.jsonDic!["message"] as! String)
                if error == nil {
                    
//                    {"status":"1","error":"0","data":{"otp":9538,"user_email":"tester@gmail.com"},"message":"Please check your mail for otp!"}
                    let dict = JsonData?.jsonDic!["data"] as! NSDictionary
                    OTPValue = getString(string: dict["otp"] as Any)
                    let msg = getString(string: JsonData?.jsonDic!["message"] as Any)
                    self.view.makeToast(msg, duration: toastTimer, position: .center)

                    DispatchQueue.main.async {
                        self.perform(#selector(self.sendToNextC), with: nil, afterDelay: 1.2)
                    }
                    
                    print(JsonData?.jsonDic)
                }
                else{
                    print(JsonData?.jsonDic)
                    
                }
            }
        }
        
    }
    
    @objc func sendToNextC(){
        
        let changeVC = ChangePasswordVC(nibName: "ChangePasswordVC", bundle: nil)
        changeVC.emailID = valueDict["email"] as! String
        self.navigationController?.pushViewController(changeVC, animated: false)
    }
    @IBAction func _backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func _signUpClick(_ sender: Any) {
        let viewObject = RegistrationVC.init(nibName: "RegistrationVC", bundle: nil)
        let controller =  UIManager.shared.checkNavigationController(getviewController: RegistrationVC.self, navigation: self.navigationController!)
        if !controller .isKind(of: RegistrationVC.self) {
            self.navigationController?.pushViewController(viewObject, animated: false)
        }
        else{
            self.navigationController?.popToViewController(controller, animated: false)
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ForgetPasswordVC : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "ForgetPasswordCustomCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ForgetPasswordCustomCell
        if cell == nil {
            tableView.register(UINib(nibName: "ForgetPasswordCustomCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ForgetPasswordCustomCell
        }
        
        cell?._SendButton.addTarget(self, action: #selector(sendClick(_:)), for: .touchUpInside)
        cell?._emailTxtField.tag         = 51
        cell?._emailTxtField.delegate    = self

        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension ForgetPasswordVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
            valueDict.setValue(textField.text!, forKey: "email")
     
    }
}
