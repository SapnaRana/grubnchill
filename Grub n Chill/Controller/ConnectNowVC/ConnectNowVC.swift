//
//  ConnectNowVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class ConnectNowVC: ParentClass {
    @IBOutlet weak var _topView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeaderView(view: _topView)
        _headerView._headerLbl.isHidden = false
        _headerView._headerLbl.text = "Connect Now"
        _headerView._icon.isHidden   = true
        _headerView._wheelBtn.isHidden = true
        _headerView._saveChangeBtn.isHidden = true
        _headerView._searchBtn.isHidden = true

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
