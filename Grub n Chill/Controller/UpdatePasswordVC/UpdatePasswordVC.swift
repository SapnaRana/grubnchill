//
//  UpdatePasswordVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 22/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import MaterialTextField
class UpdatePasswordVC: ParentClass {
    @IBOutlet weak var _topView: UIView!
    @IBOutlet weak var _oldPasswordTxtField: MFTextField!
    @IBOutlet weak var _newPasswordTxtField: MFTextField!
    @IBOutlet weak var _confirmTxtField: MFTextField!
    @IBOutlet weak var _backBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        _backBtn.setImage(UIImage(named: "back-arrow"), for: .normal);
        // Do any additional setup after loading the view.
    }
    @IBAction func submitClick(_ sender: Any) {
        self.checkStatus()
    }
    @IBAction func _backBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func checkStatus() {
        self.view.endEditing(true)
        
       
         if _oldPasswordTxtField.text! == "" || _oldPasswordTxtField.text!.isValidText() == false {
            self.showMessage(msg: "Please Enter Old Password")
            
        }
        else if _newPasswordTxtField.text! == "" || _newPasswordTxtField.text!.isValidText() == false {
            self.showMessage(msg: "Please Enter New Password")
            
        }
         else if _confirmTxtField.text! == "" || _confirmTxtField.text!.isValidText() == false {
            self.showMessage(msg: "Please Enter Confirm Password")
            
         }
       
        else if _newPasswordTxtField.text! != _confirmTxtField.text!{
            self.showMessage(msg: "Pasword and Confirm Password not match")
            
        }
        else{
            let value = String(format: "type=changepassword&old_password=%@&new_password=%@&user_id=%@", _oldPasswordTxtField.text!,_newPasswordTxtField.text!,GlobalMethod.userDetail!.user_id!)
            self.changePasswordMethod(value: value)
            
        }
        
        
    }
    
    func changePasswordMethod(value : String ){
        APIManager.shared.getRequest(values: value, showIndicator: true, viewController: self) { (error, JsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    let dict = JsonData?.jsonDic!["data"]
                    self.view.makeToast((JsonData?.jsonDic!["message"] as! String), duration: toastTimer, position: .center)

                }
                else{
                    self.view.makeToast((JsonData?.jsonDic!["message"] as! String), duration: toastTimer, position: .center)
                }
            }
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
