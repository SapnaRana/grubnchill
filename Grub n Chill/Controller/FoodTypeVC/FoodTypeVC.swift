//
//  FoodTypeVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 03/12/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class FoodTypeVC: ParentClass {
    @IBOutlet weak var _backBtn: UIButton!
    @IBOutlet weak var _tableView: UITableView!
    var foodTypeArray = [foodTypeObject]()
    @IBOutlet weak var _selectedCount: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        _backBtn.setImage(UIImage(named: "back-arrow"), for: .normal);
        self.getFoodType()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func _addbtn(_ sender: Any) {
        
        let categoryCount = foodTypeArray.filter({m in
            m.selected == "1" }
            ).map({$0.foodtype_name!}).count
        if categoryCount > 0 {
            
            selectedFoodTypeObj = foodTypeArray.filter({m in
                m.selected == "1" }
            )
            self.navigationController?.popViewController(animated: true)
        }
    }
    func getFoodType(){
        
        APIManager.shared.getRequest(values: "&type=getfoodtype", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                self.foodTypeArray.removeAll()
                if error == nil{
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray{
                        let obj = foodTypeObject.init(json: dict as! [String : Any])
                        self.foodTypeArray.append(obj)
                    }
                    
                    
                    var index = 0
                    for case var obj  in self.foodTypeArray {
                        for dummyObj in selectedFoodTypeObj {
                            if dummyObj.foodtype_id == obj.foodtype_id {
                                obj.selected = "1"
                                self.foodTypeArray[index] = obj
                            }
                        }
                        index += 1
                    }
                    
                }
                self._tableView.reloadData()
                
            }
        }
    }
    
    
    @IBAction func _backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FoodTypeVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodTypeArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "FoodCategoryCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FoodCategoryCell
        if cell == nil {
            tableView.register(UINib(nibName: "FoodCategoryCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FoodCategoryCell
        }
        let obj = foodTypeArray[indexPath.row]
        cell?._nameLbl.text = obj.foodtype_name
        if obj.selected == "0"{
            cell!._checkIcon.image = UIImage(named: "uncheckIcon")
        }
        else{
            cell!._checkIcon.image = UIImage(named: "checkIcon-1")
            
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var obj = foodTypeArray[indexPath.row]
        if obj.selected == "0"{
            obj.selected = "1"
        }
        else{
            obj.selected = "0"
        }
        foodTypeArray[indexPath.row] = obj
        self._tableView.beginUpdates()
        self._tableView.reloadRows(at: [indexPath], with: .automatic)
        self._tableView.endUpdates()
        
        
        let categoryCount = foodTypeArray.filter({m in
            m.selected == "1" }
            ).map({$0.foodtype_name!}).count
        
        
        if categoryCount == 0 {
            _selectedCount.text = "0 Selected"
        }
        else{
            _selectedCount.text = "\(categoryCount) Selected"
        }
        
        
        
    }
    
    
}
