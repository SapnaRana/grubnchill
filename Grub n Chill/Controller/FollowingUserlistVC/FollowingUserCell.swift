

//
//  FollowingUserCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 22/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class FollowingUserCell: UITableViewCell {
    @IBOutlet weak var _profileImage: UIImageView!
    @IBOutlet weak var _name: UILabel!
    @IBOutlet weak var _btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        _btn.layer.cornerRadius = 3
        _btn.layer.borderColor  = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        _btn.layer.borderWidth  = 1
        
        _profileImage.layer.cornerRadius = _profileImage.bounds.width / 2
        _profileImage.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _profileImage.layer.borderWidth = 1
        
        
        
        // Initialization code
    }
    func setBtnColorAndText(str :String){
        _btn.layer.cornerRadius = 3

        if str == "1"{
            _btn.layer.borderColor  = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            _btn.layer.borderWidth  = 1
            _btn.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
            _btn.backgroundColor  = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        }
        else{
            _btn.layer.borderColor  = UIColor.clear.cgColor
            _btn.layer.borderWidth  = 0
            _btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            _btn.backgroundColor  = UIColor(red:1, green:0.95, blue:0, alpha:1)

        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
