//
//  GrabNChillWheelVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import SpinWheelControl
import RxSwift
import RxCocoa

class GrabNChillWheelVC: ParentClass,SpinWheelControlDataSource, SpinWheelControlDelegate {
    @IBOutlet weak var _arrowBtn: UIButton!
    @IBOutlet weak var _nameView: UIView!
    @IBOutlet weak var _lineView: UIView!
    
    
    @IBOutlet weak var _topView: UIView!
    @IBOutlet weak var spinWheelControl: SpinWheelControl!
//    var foodCategoryArray = [foodCategoryObject]()

//    var selectedCat  : foodCategoryObject?
    var selectedResturantObj  : restaurantListObject?


    @IBOutlet weak var resNameView: UIView!
    @IBOutlet weak var resNameLbl: UILabel!
    @IBOutlet weak var _spinBtn: UIButton!
    var restaurantArray = [restaurantListObject]()

    
    let colorPalette : [UIColor] = [#colorLiteral(red: 0.1509480774, green: 0.6760635376, blue: 0.7242119908, alpha: 1),#colorLiteral(red: 0.187196821, green: 0.8049731851, blue: 0.8669851422, alpha: 1),#colorLiteral(red: 0.8088834882, green: 0.002696537413, blue: 0.7293425202, alpha: 1),#colorLiteral(red: 0.9780107141, green: 0, blue: 0.7313776612, alpha: 1),#colorLiteral(red: 0.9812543988, green: 0.4353326559, blue: 0.959115088, alpha: 1),#colorLiteral(red: 0.9785724282, green: 0.06638825685, blue: 0.005035162438, alpha: 1),#colorLiteral(red: 0.134444803, green: 0.6174433827, blue: 0.6608697772, alpha: 1),#colorLiteral(red: 0.990105927, green: 0.747781992, blue: 0.008116673678, alpha: 1),#colorLiteral(red: 0.4251144528, green: 0.8543201685, blue: 0.2645141184, alpha: 1),#colorLiteral(red: 0.09472907335, green: 0.4882336259, blue: 0.7331457734, alpha: 1)]
    
    
    func viewReset(status : Bool){
        _spinBtn.isHidden = status
        _nameView.isHidden = status
        _lineView.isHidden = status
        _arrowBtn.isHidden = status
    }
    
    func wedgeForSliceAtIndex(index: UInt) -> SpinWheelWedge {
        let wedge = SpinWheelWedge()
//        let randomIndex = Int(arc4random_uniform(UInt32(colorPalette.count)))
        wedge.shape.fillColor = colorPalette[Int(index)].cgColor
//            colorPalette[Int(randomIndex)].cgColor
        let obj = self.restaurantArray[Int(index)]
        wedge.label.text =  obj.restaurant_name
        return wedge
        
        
    }

  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateNearByRestaurant()
        self.resNameLbl.text = ""
        _spinBtn.layer.cornerRadius = _spinBtn.frame.width / 2
        
        
        self.viewReset(status: true)
        self.setHeaderView(view: _topView)
        _headerView._icon.isHidden   = false
        _headerView._wheelBtn.isHidden = true
        _headerView._saveChangeBtn.isHidden = true
        _headerView._searchBtn.isHidden = true
        
        resNameView.layer.cornerRadius = 4
        resNameView.layer.borderColor = UIColor.lightGray.cgColor
        resNameView.layer.borderWidth = 1
        

        
//        spinWheelControl.addTarget(self, action: #selector(spinWheelDidChangeValue), for: .valueChanged)
        
        spinWheelControl.dataSource = self
        
        spinWheelControl.delegate = self
        spinWheelControl.isUserInteractionEnabled = false
        
        

//        self.getCategoryAPi()
        
        
        let lat = UserDefaults.standard.object(forKey: "lat")
        let long = UserDefaults.standard.object(forKey: "long")
        
        
        if lat != nil && long != nil {
            self.getRestaurant(lat: lat as! String, long: long as! String , isShowloadingIndicator: true)
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
//    func getCategoryAPi(){
//        APIManager.shared.getRequest(values: "&type=getfoodcategory", showIndicator: true, viewController: self) { (error, jsonData) in
//            DispatchQueue.main.async {
//                self.foodCategoryArray.removeAll()
//                if error == nil{
//                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
//                    for dict in localArray{
//                        let obj = foodCategoryObject.init(json: dict as! [String : Any])
//                        self.foodCategoryArray.append(obj)
//                    }
//                }
//
//                if self.foodCategoryArray.count > 0{
//                    self.selectedCat = self.foodCategoryArray[0]
//                    self.resNameLbl.text =  self.selectedCat!.foodcategory_name
//                }
//
//                self.spinWheelControl.reloadData()
//
//            }
//        }
//
//    }
//
    
    
    
    func getRestaurant(lat : String , long : String , isShowloadingIndicator : Bool){
        
        APIManager.shared.getRequest(values: "type=getNearestRestaurantsList&lat=\(lat)&lon=\(long)&s=", showIndicator: isShowloadingIndicator, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                self.restaurantArray.removeAll()
                if error == nil {
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray {
                        let obj = restaurantListObject.init(json: dict as! [String : Any])
                        self.restaurantArray.append(obj)
                    }
                }
                if self.restaurantArray.count > 0{
                    self.viewReset(status: false)
                    self.selectedResturantObj = self.restaurantArray[0]
                    self.resNameLbl.text =  self.selectedResturantObj!.restaurant_name
                }

                self.spinWheelControl.reloadData()

            }
        }
        
    }
    
    
    
    
    @IBAction func _clickBtnForResturant(_ sender: Any) {
        
        let reviewPostVC = RestaurantDetailVC.init(nibName: "RestaurantDetailVC", bundle: nil)
        reviewPostVC.restaurantId =  (selectedResturantObj?.restaurant_id)!
        self.navigationController?.pushViewController(reviewPostVC, animated: true)

        
//        let lat = UserDefaults.standard.object(forKey: "lat")
//        let long = UserDefaults.standard.object(forKey: "long")
//
//        if lat == nil && long == nil {
//            return
//        }
//
//
//
//
////        http://www.letsgrubnchill.com/api/Api.php?app=grubnchill&type=getrestaurantbycategory&lat=-33.8670522&lon=151.1957362&category=10&user_id=5
//        APIManager.shared.getRequest(values: "&type=getrestaurantbycategory&lat=\(lat as! String)&lon=\(long as! String)&category=\(String(describing: selectedCat!.foodcategory_id!))&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: true, viewController: self) { (error, jsonData) in
//            DispatchQueue.main.async {
//                if error == nil{
//                    print(jsonData?.jsonDic)
//
//                }
//
//
//            }
//        }
//
        
    }
    
    @IBAction func spinBtn(_ sender: Any) {
        spinWheelControl.randomSpin()
    }
    
   
    func numberOfWedgesInSpinWheel(spinWheel: SpinWheelControl) -> UInt {
        return  self.restaurantArray.count > 10 ?  10 : UInt(self.restaurantArray.count)
    }
    
    
    //Target was added in viewDidLoad for the valueChanged UIControlEvent
    @objc func spinWheelDidChangeValue(sender: AnyObject) {
        print("Value changed to " + String(self.spinWheelControl.selectedIndex))
    }
    
    
    func spinWheelDidEndDecelerating(spinWheel: SpinWheelControl) {
        print("The spin wheel did end decelerating.")
        selectedResturantObj = self.restaurantArray[Int(self.spinWheelControl.selectedIndex)]
        resNameLbl.text =  selectedResturantObj!.restaurant_name
    }
    
    
    func spinWheelDidRotateByRadians(radians: Radians) {
        print("The wheel did rotate this many radians - " + String(describing: radians))
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
