
//
//  SlideCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 05/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class SlideCell: UITableViewCell {
    @IBOutlet weak var _iconPng: UIImageView!
    @IBOutlet weak var _titleLb: UILabel!
    @IBOutlet weak var _arrowImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
