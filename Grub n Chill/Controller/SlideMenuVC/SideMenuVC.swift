//
//  SideMenuVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 05/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class SideMenuVC: ParentClass {
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet var _tableHeaderView: UIView!
    let titleArray = ["Home","Profile","Location","Notification","Camera","Connect Now","Blog","Grub n Chill wheel","Direct Messaging","Logout"]
    let imageArray = ["home","profile","location","notification","camera","connect","blog","wheelicon","message","logout"]
    var controllerArray = [HomeVC(),ProfileVC(),LocationVC(),NotificationVC(),CameraPostVC(),ConnectNowVC(),BlogVC(),GrabNChillWheelVC(),DirectMessageVC(),""] as [Any]

    @IBOutlet weak var _userNameLbl: UILabel!
    @IBOutlet weak var _userImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        _userImage.layer.cornerRadius = _userImage.bounds.width / 2
        _userImage.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _userImage.layer.borderWidth = 1
        
        
        
        
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(clickUserDetail(sender:)))
        _userNameLbl.isUserInteractionEnabled = true
        _userNameLbl.addGestureRecognizer(tapGesture4)
        
        let tapGesture5 = UITapGestureRecognizer(target: self, action: #selector(clickUserDetail(sender:)))
        _userImage.isUserInteractionEnabled = true
        _userImage.addGestureRecognizer(tapGesture5)

       
       _tableView.tableHeaderView = _tableHeaderView
        _tableView.tableFooterView = UIView(frame: .zero)
        self.navigationController?.navigationBar.isHidden = true
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func clickUserDetail(sender : UITapGestureRecognizer){
        let navigationController = DEMONavigationController.init(rootViewController: controllerArray[1] as! UIViewController)
        frostedViewController.contentViewController = navigationController
        frostedViewController.hideMenuViewController()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if profileObject != nil {
            _userNameLbl.text = (profileObject.user_firstname)! + " " + (profileObject.user_lastname)!
            _userImage.sd_setImage(with:  URL(string:(profileObject.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
        }
        else{
            _userNameLbl.text = (GlobalMethod.userDetail?.firstname)! + " " + (GlobalMethod.userDetail?.lastname)!

        }
    }
    

    @IBAction func closeSlide(_ sender: Any) {
        frostedViewController.hideMenuViewController()
    }
    
    
    
    func sendConnectNowMessage(message : String){
        let lat = UserDefaults.standard.object(forKey: "lat")
        let long = UserDefaults.standard.object(forKey: "long")
        if lat == nil && long == nil {
            return
        }
        APIManager.shared.getRequest(values: "&type=connectnow&lat=\(lat as! String)&lon=\(long as! String)&message=\(message)&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    print(jsonData?.jsonDic as Any)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "connectNowMessage"), object: jsonData?.jsonDic!["message"] as! String) //posts the list items obect

                }
                
                
                
            }
        }
        
        
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SideMenuVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "SlideCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SlideCell
        if cell == nil {
            tableView.register(UINib(nibName: "SlideCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SlideCell
        }
        cell?._titleLb.text = titleArray[indexPath.row]
        cell?._iconPng.image = UIImage(named: imageArray[indexPath.row])
        cell?._arrowImage.isHidden = titleArray[indexPath.row] == "Blog" ? false : true
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if titleArray[indexPath.row] == "Logout" {
        GlobalMethod.shared.removeDefultsValue()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.nvc?.popToRootViewController(animated: true)
            return
        }
        else if titleArray[indexPath.row] == "Camera"{
            frostedViewController.hideMenuViewController()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "slideCameraOpen"), object: nil) //posts the list items obect
            return
        }
        else if titleArray[indexPath.row] == "Connect Now" {
            let alertController = UIAlertController(title: "Grub n Chill", message: "", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Send", style: .default, handler: { alert -> Void in
                let textField = alertController.textFields![0] as UITextField
                self.sendConnectNowMessage(message: textField.text!)
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
                textField.placeholder = "Enter Message"
            })
            self.navigationController?.present(alertController, animated: true, completion: nil)
            frostedViewController.hideMenuViewController()
            return
        }
        else if titleArray[indexPath.row] == "Direct Messaging"{
            return
        }
        else{
            let navigationController = DEMONavigationController.init(rootViewController: controllerArray[indexPath.row] as! UIViewController)
            frostedViewController.contentViewController = navigationController
            frostedViewController.hideMenuViewController()
        }
    }
}
