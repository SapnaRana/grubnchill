//
//  BlogListVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 25/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Windless

class BlogListVC: ParentClass {
    var blogListArray = [blogListObject]()
    var animationStop = false

    @IBOutlet weak var _tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadTableViewData(notification:)) , name: NSNotification.Name(rawValue: "blogListLoad"), object: nil)

        
//        self.getBlogListMethod(category_id: "0")
        // Do any additional setup after loading the view.
    }
    
    @objc func loadTableViewData(notification: NSNotification) {
        let obj = notification.object as! blogCategoryObject
        self.getBlogListMethod(category_id: obj.category_id!)
    }
//    
//    @objc func stopAnimation(){
//        _tableView.windless.end()
//        animationStop = true
//        _tableView.reloadData()
//    }
//    @objc func startAnimation(){
//        if animationStop == false {
//            _tableView.windless
//                .apply {
//                    $0.beginTime = 2
//                    $0.duration = 4
//                    $0.animationLayerOpacity = 0.5
//                }
//                .start()
//        }
//    }
    override func viewDidLayoutSubviews() {
        self.startAnimation(view: _tableView)
    }
    
    func getBlogListMethod(category_id : String){
        APIManager.shared.getRequest(values: "type=getblogpostlisting&category_id=\(category_id)&page_num=1", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                self.stopAnimation(view: self._tableView)
                self.animationStop = true
                self.blogListArray.removeAll()
                if error == nil{
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray{
                        let obj = blogListObject.init(json: dict as! [String : Any])
                        self.blogListArray.append(obj)
                    }
                }
                self._tableView.reloadData()

            }
        }
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension BlogListVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if animationStop == true {
            return self.blogListArray.count
        }
        else{
            return 10
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "BlogCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? BlogCell
        if cell == nil {
            tableView.register(UINib(nibName: "BlogCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? BlogCell
        }
        if animationStop == true {
           let obj = self.blogListArray[indexPath.row]
            cell?._blogTitle.text = obj.post_title
            cell?._blogContent.text = obj.post_content
        }
        
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewObject = BlogDetailVC.init(nibName: "BlogDetailVC", bundle: nil)
        viewObject.post_url = self.blogListArray[indexPath.row].post_url!
        viewObject.controllertype = "Blog"
        let  appdel = UIApplication.shared.delegate as! AppDelegate
        appdel.nvc?.pushViewController(viewObject, animated: true);

    }
    
    
}

