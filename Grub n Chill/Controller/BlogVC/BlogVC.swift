//
//  BlogVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class BlogVC: ParentClass {
    @IBOutlet weak var _topView: UIView!
    var controllerArray : [UIViewController] = []
    var pageMenu : CAPSPageMenu?
    @IBOutlet weak var _centerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeaderView(view: _topView)
        _headerView._icon.isHidden   = false
        _headerView._wheelBtn.isHidden = true
        _headerView._saveChangeBtn.isHidden = true
        _headerView._searchBtn.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func viewDidLayoutSubviews() {
        
        if  self.controllerArray.count == 0{
            self.pageSet()
        }
    }
    func pageSet() {
        
        for obj in blogCategoryArray {
            let controller : BlogListVC = BlogListVC(nibName: "BlogListVC", bundle: nil)
            controller.title = obj.category_name
            self.controllerArray.append(controller)
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorHeight(2.0),
            .selectionIndicatorColor(UIColor.red ),
            .bottomMenuHairlineColor(UIColor.groupTableViewBackground),
            .selectedMenuItemLabelColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)),
            .unselectedMenuItemLabelColor(#colorLiteral(red: 0.6901344657, green: 0.6902355552, blue: 0.690112412, alpha: 1)),
            .useMenuLikeSegmentedControl(false),
            .enableHorizontalBounce(false),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .menuMargin(20.0),
            .menuItemSeparatorPercentageHeight(0.0),
            .menuItemFont(UIFont.systemFont(ofSize: 14)),
            .selectedMenuItemFont(UIFont.systemFont(ofSize: 14)),
            .menuHeight(50.0),
            .centerMenuItems(true)
        ]
        
        // Initialize scroll menu
        self.pageMenu = CAPSPageMenu(viewControllers: self.controllerArray, frame: CGRect(x: 0.0, y: 0, width: UIScreen.main.bounds.size.width, height: _centerView.bounds.height), pageMenuOptions: parameters)
        self._centerView.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
        pageMenu!.delegate = self
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "blogListLoad"), object: blogCategoryArray[0]) //posts the list items obect

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BlogVC :CAPSPageMenuDelegate {
    func willMoveToPage(_ controller: UIViewController, index: Int){
        
        
    }
    func didMoveToPage(_ controller: UIViewController, index: Int){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "blogListLoad"), object: blogCategoryArray[index]) //posts the list items obect

        
    }
}
