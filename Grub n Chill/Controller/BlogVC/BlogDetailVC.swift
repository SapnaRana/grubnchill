//
//  BlogDetailVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 26/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import WebKit

class BlogDetailVC: ParentClass {
    @IBOutlet weak var _backBtn: UIButton!
    var post_url = ""
    var controllertype = ""

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        _backBtn.setImage(UIImage(named: "back-arrow"), for: .normal);

        let url = URL(string: post_url)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        // Do any additional setup after loading the view.
    }

    @IBAction func _backClick(_ sender: Any) {
        if controllertype == "Blog"{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
