//
//  BlogCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 26/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class BlogCell: UITableViewCell {
    @IBOutlet weak var _blogmage: UIImageView!
    @IBOutlet weak var _blogTitle: UILabel!
    @IBOutlet weak var _blogContent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
