//
//  NotificationVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import SDWebImage

class NotificationVC: ParentClass {
    @IBOutlet weak var _topView: UIView!
    @IBOutlet weak var _buttomView: UIView!
//    var notificationArray = [notificationObjectData]()
    @IBOutlet weak var _tableView: UITableView!
    var page_num      = 1
    var page_limit    = 10
    var isLoadingList : Bool = false
    var animationStop = false

    
    var notificationModel = NotificationViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeaderView(view: _topView)
        self.setFooterView(view: _buttomView)
        _headerView._headerLbl.isHidden = false
        _headerView._headerLbl.text = "Notification"
        _headerView._icon.isHidden   = true
        _headerView._wheelBtn.isHidden = true
        _headerView._saveChangeBtn.isHidden = true
        _headerView._searchBtn.isHidden = true
        self.getAllNotificationMethod(isRefersh: true)
        
//        self.removeNotificationCount()
        
        _tableView.tableFooterView = UIView(frame: _buttomView.bounds)


        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        if !animationStop {
            self.startAnimation(view: _tableView)
        }
    }

    //MARK:-  GET ALL Notification Method
    func getAllNotificationMethod(isRefersh : Bool){
        notificationModel.requestData(value: "&type=mynotifications&user_id=\(GlobalMethod.userDetail!.user_id!)&page_num=\(page_num)&page_limit=\(page_limit)") { (isSuccess) in
            if isSuccess {
                notificationCount = "0"
                self._footerView._countLbl.isHidden = true
                self.isLoadingList = false
                self.animationStop = true
                self.stopAnimation(view: self._tableView)
                self._tableView.reloadData()
                DispatchQueue.main.async {
                    self._tableView.tableFooterView?.isHidden = true
                }

            }
        }
        
//
//        APIManager.shared.getRequest(values: "&type=mynotifications&user_id=\(GlobalMethod.userDetail!.user_id!)&page_num=\(page_num)&page_limit=\(page_limit)", showIndicator: isRefersh, viewController: self) { (error, jsonData) in
//            DispatchQueue.main.async {
//                self.isLoadingList = false
//                if isRefersh == true{
//                    self.notificationArray.removeAll()
//                }
//                if error == nil {
//                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
//                    for dict in localArray {
//                        let obj = notificationObjectData.init(json: dict as! [String : Any])
//                        self.notificationArray.append(obj)
//                    }
//
//                    if localArray.count > 0{
//                        if isRefersh == true{
//                            self._tableView.reloadData()
//                        }
//                        else {
//
//                            var indexPathsArray = [IndexPath]()
//                            var totalCount = (self.notificationArray.count - localArray.count ) - 1
//                            for _ in 0..<localArray.count{
//                                totalCount = totalCount + 1
//                                let indexPath = IndexPath(row: totalCount, section: 0)
//                                indexPathsArray.append(indexPath)
//                            }
//                            print(indexPathsArray)
//                            print(self.notificationArray.count)
//                            self._tableView.beginUpdates()
//                            self._tableView.insertRows(at: indexPathsArray, with: .automatic)
//                            self._tableView.endUpdates()
//                        }
//                    }
//                    self._tableView.tableFooterView?.isHidden = true
//
//
//
//                }
//                else{
//                    self.page_num = self.page_num - 1
//                    if self.isLoadingList == false{
//                        self.notificationArray.removeAll()
//                        self._tableView.reloadData()
//                    }
//                }
//            }
//        }
        
    }
    
    
    @objc func acceptClick(sender : UIButton){
        let obj = self.notificationModel.notification[sender.tag]
        self.requestAcceptAndRejectMethod(obj: obj, follow_status: "2",tag: sender.tag)
    }
    @objc func rejectClick(sender : UIButton){
        let obj = self.notificationModel.notification[sender.tag]
        self.requestAcceptAndRejectMethod(obj: obj, follow_status: "3",tag: sender.tag)
    }
    
    
    //MARK:-  GET ALL Notification Method
    func requestAcceptAndRejectMethod(obj : Notification , follow_status : String , tag : Int){
        APIManager.shared.showProgress()
        notificationModel.requestAcceptAndRejectMethod(value: "&type=updatefollow&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(obj.follow_userid!)&follow_status=\(follow_status)&notification_id=\(obj.notification_id!)",from: self) { (isSuccess) in
            if isSuccess {
                DispatchQueue.main.async {
                    APIManager.shared.hideProgress()
                }
                self.notificationModel.notification.remove(at: tag)
                self._tableView.reloadData()
            }
            
        }
        
        
//        APIManager.shared.getRequest(values: "&type=updatefollow&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(obj.follow_userid!)&follow_status=\(follow_status)&notification_id=\(obj.notification_id!)", showIndicator: true, viewController: self) { (error, jsonData) in
//            DispatchQueue.main.async {
//                if error == nil {
//                    self.notificationModel.notification.remove(at: tag)
//                    self.view!.makeToast("\(String(describing: jsonData!.jsonDic!["message"]!))", duration: toastTimer, position: .center)
//                    self._tableView.reloadData()
//
//                }
//            }
//        }
        
    }
    
    
    
    @objc func clickProfile(sender : UIGestureRecognizer){
        
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let Obj = self.notificationModel.notification[indexPath.row]
            let viewObject = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
            viewObject.viewType = "backShow"
            viewObject.user_id = Obj.follow_userid!
            self.navigationController?.pushViewController(viewObject, animated: true)
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NotificationVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  animationStop == true ? self.notificationModel.notification.count : 10
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            self._tableView.tableFooterView = spinner
            self._tableView.tableFooterView?.isHidden = false
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if animationStop == false {
            
            let identifier = "FirstCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FirstCell
            if cell == nil {
                tableView.register(UINib(nibName: "FirstCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FirstCell
            }
            return cell!
            
        }
        else{
            let identifier = "NotificationCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? NotificationCell
            if cell == nil {
                tableView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? NotificationCell
            }
            let obj = self.notificationModel.notification[indexPath.row]
            cell?.configurationCell(notificationModel: obj, controller: self, indexPath: indexPath)
            
            return cell!

        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.notificationModel.notification[indexPath.row]
        if obj.review_id != "" && obj.review_id != "0" {
            notificationModel.gotoSinglePostView(notificationModel: obj, from: self)
        }

    }
    
    
}

extension NotificationVC : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height ) && !isLoadingList){
            isLoadingList = true
            page_num = page_num + 1
            self.getAllNotificationMethod(isRefersh: false)
        }
        
    }

}
