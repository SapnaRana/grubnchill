//
//  NotificationCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 22/01/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var _profileImage: UIImageView!
    @IBOutlet weak var _nameLbl: UILabel!
    @IBOutlet weak var _subDetailLbl: UILabel!
    @IBOutlet weak var _acceptBtn: UIButton!
    @IBOutlet weak var _rejectBtn: UIButton!
    @IBOutlet weak var _rWidthCostant: NSLayoutConstraint!
    @IBOutlet weak var aWidthConstant: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        _profileImage.layer.cornerRadius = _profileImage.bounds.width / 2
        _profileImage.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _profileImage.layer.borderWidth = 1
        
        _acceptBtn.layer.cornerRadius = 3

        _rejectBtn.layer.cornerRadius = 3
        _rejectBtn.layer.borderColor  = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        _rejectBtn.layer.borderWidth  = 1


        // Initialization code
    }

     
    
    func configurationCell(notificationModel : Notification ,controller: UIViewController , indexPath : IndexPath){
        
        _nameLbl.text = notificationModel.user_firstname! + " " + notificationModel.user_lastname!
        _subDetailLbl.text = notificationModel.user_message
        _profileImage.sd_setImage(with:  URL(string:(notificationModel.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
        
       
        
        let tap = UITapGestureRecognizer.init(target: controller, action: #selector(NotificationVC.clickProfile(sender:)))
        _profileImage.isUserInteractionEnabled = true
        _profileImage.addGestureRecognizer(tap);
        
        let tapGesture4 = UITapGestureRecognizer(target: controller, action: #selector(NotificationVC.clickProfile(sender:)))
        _nameLbl.isUserInteractionEnabled = true
        _nameLbl.addGestureRecognizer(tapGesture4)
        
        
        if notificationModel.type! == "1"{
            _rWidthCostant.constant = 82
            aWidthConstant.constant = 82
        }
        else
        {
            _rWidthCostant.constant = 0
            aWidthConstant.constant = 0
            
        }
       _acceptBtn.tag = indexPath.row
       _rejectBtn.tag = indexPath.row
        _acceptBtn.addTarget(controller, action: #selector(NotificationVC.acceptClick(sender:)), for: .touchUpInside)
        _rejectBtn.addTarget(controller, action: #selector(NotificationVC.rejectClick(sender:)), for: .touchUpInside)

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
