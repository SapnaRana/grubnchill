//
//  LoginVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire

class LoginVC: ParentClass {
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet var _tableHeaderView: UIView!
    @IBOutlet var _tableFooterView: UIView!
    @IBOutlet weak var _signUpButton: UIButton!
    
    var valueDict = NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        print((GlobalMethod.shared.getUserdefaultData(key: LoginDataKey) as! NSDictionary))
        if (GlobalMethod.shared.getUserdefaultData(key: LoginDataKey) as! NSDictionary).count > 0 {
            self.sendnextHomePage()
        }
        valueDict.setValue("", forKey: "email")
        valueDict.setValue("", forKey: "password")
        
        _tableView.tableHeaderView = _tableHeaderView
        _tableView.tableFooterView = _tableFooterView
        _signUpButton.layer.cornerRadius = 20
        
        // Do any additional setup after loading the view.
    }
    
    @objc @IBAction func _signInCllick(_ sender: Any) {
        
        self.view.endEditing(true)
        if valueDict["email"] == nil || (valueDict["email"] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter Email Address")
        }
        else if valueDict["password"] == nil || (valueDict["password"] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter  Password")
            
        }
        else if (valueDict["email"] as! String).isValidEmail == false{
            self.showMessage(msg: "Please Enter a Valid Email Address")
        }
        else{
            let value = String(format: "type=login&user_email=%@&user_pass=%@&user_device=i&user_token=%@", valueDict["email"] as! String,valueDict["password"] as! String,fcm_reg_id)
            self.loginMethod(value: value,typeOffLogin: "Normal")
        }
    }
    
    //MARK:- Login Method
    
    @objc @IBAction func _forgetPasswordClick(_ sender: Any) {
        let viewObject = ForgetPasswordVC.init(nibName: "ForgetPasswordVC", bundle: nil)
        self.navigationController?.pushViewController(viewObject, animated: false)
    }
    
    @objc @IBAction func _facebookClick(_ sender: Any) {
        self.loginFBClicked()
        
    }
    
    
    
    
    @IBAction func _signUpClick(_ sender: Any) {
        let viewObject = RegistrationVC.init(nibName: "RegistrationVC", bundle: nil)
        let controller =  UIManager.shared.checkNavigationController(getviewController: RegistrationVC.self, navigation: self.navigationController!)
        if !controller .isKind(of: RegistrationVC.self) {
            self.navigationController?.pushViewController(viewObject, animated: false)
        }
        else{
            self.navigationController?.popToViewController(controller, animated: false)
        }
        
    }
    @IBAction func _termClick(_ sender: UIButton) {
        let viewObject = TermsAndConditionVC.init(nibName: "TermsAndConditionVC", bundle: nil)
        if sender.tag == 11{
            viewObject.typeOfView = "terms"
        }
        else{
            viewObject.typeOfView = "privacy"
        }
        self.navigationController?.pushViewController(viewObject, animated: false)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
   
}



extension LoginVC : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "LoginCustomCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? LoginCustomCell
        if cell == nil {
            tableView.register(UINib(nibName: "LoginCustomCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? LoginCustomCell
        }
        
        cell?._forgetButton.addTarget(self, action: #selector(_forgetPasswordClick(_:)), for: .touchUpInside)
        cell?._signInButton.addTarget(self, action: #selector(_signInCllick(_:)), for: .touchUpInside)
        cell?._facebookButton.addTarget(self, action: #selector(_facebookClick(_:)), for: .touchUpInside)
        
        cell?._emailtxtField.tag         = 51
        cell?._passwordTxtField.tag      = 52
        cell?._emailtxtField.delegate    = self
        cell?._passwordTxtField.delegate = self
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension LoginVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 51 {
            valueDict.setValue(textField.text!, forKey: "email")
        }
        else{
            valueDict.setValue(textField.text!, forKey: "password")
            
        }
    }
}
