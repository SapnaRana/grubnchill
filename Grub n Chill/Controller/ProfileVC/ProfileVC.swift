//
//  ProfileVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 21/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire
import DropDown

class ProfileVC: ParentClass {
    var page_num      = 1
    var page_limit    = 100
    var isLoadingList : Bool = false

    @IBOutlet weak var _topView: UIView!
    @IBOutlet var _tableHeaderView: UIView!
    @IBOutlet weak var _tableView: UITableView!
    
    @IBOutlet weak var _userImage: UIImageView!
    @IBOutlet weak var _userNameLbl: UILabel!
    @IBOutlet weak var _userBioLbl: UILabel!
    var viewType = ""
    var homeDataArray = [homeObjectData]()
    @IBOutlet weak var _slideBtn: UIButton!
    @IBOutlet weak var _iconBtn: UIImageView!
    @IBOutlet weak var _editBtn: UIButton!
    @IBOutlet weak var _passwordBtn: UIButton!
    @IBOutlet weak var _heightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var _followingBtn: UIButton!
    var homeObjData : homeObjectData?
    var user_id  = ""
    var profileUserID = ""

    var user_followstatus = ""
    
    let chooseArticleDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown,
            ]
    }()
    var alertView = AlertView.instantiateFromNib()


    @IBOutlet weak var _followingCountLbl: UILabel!
    @IBOutlet weak var _postCountLbl: UILabel!
    @IBOutlet weak var _followerCountLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.setHeaderView(view: _topView)
        print((GlobalMethod.shared.getUserdefaultData(key: LoginDataKey) as! NSDictionary))

        _userImage.layer.cornerRadius = _userImage.bounds.width / 2
        _userImage.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _userImage.layer.borderWidth = 1
        _userBioLbl.text = ""
        _tableView.tableHeaderView = _tableHeaderView;
        
        _followingBtn.layer.cornerRadius = 3
        _followingBtn.layer.borderColor  = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        _followingBtn.layer.borderWidth  = 1


        NotificationCenter.default.addObserver(self, selector: #selector(loadTableViewData(notification:)) , name: NSNotification.Name(rawValue: "updateReviewPost"), object: nil)

        
        let tapGesture5 = UITapGestureRecognizer(target: self, action: #selector(clickImage(sender:)))
        _userImage.isUserInteractionEnabled = true
        _userImage.addGestureRecognizer(tapGesture5)

        

        // Do any additional setup after loading the view.
    }
    
    @objc func clickImage(sender : UITapGestureRecognizer){
        self.openImageViewController(sender: _userImage)
    }
    
    @objc func loadTableViewData(notification: NSNotification) {
        let obj = notification.object as! homeObjectData
        self.updateArrayObj(obj: obj)
    }
    
    @IBAction func _editBtn(_ sender: Any) {
        let viewObject = EditProfileVC.init(nibName: "EditProfileVC", bundle: nil)
        self.navigationController?.pushViewController(viewObject, animated: true);
    }
    
    @IBAction func _slideBtnClick(_ sender: Any) {
        if viewType == "backShow"{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            frostedViewController.presentMenuViewController()

        }
    }
    @IBAction func _changePassword(_ sender: Any) {
        let viewObject = UpdatePasswordVC.init(nibName: "UpdatePasswordVC", bundle: nil)
        self.navigationController?.pushViewController(viewObject, animated: true);
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getUserProfile(showIndicator: true)
        self.getProfile(showIndicator: false)

        
         var userID = ""
        if viewType == "backShow"{
            if homeObjData != nil {
            self._userNameLbl.text = (homeObjData!.review_username)!
            self._userImage.sd_setImage(with:  URL(string:(homeObjData!.review_userimage)!), placeholderImage:UIImage(named: "placeHolder"))
                 userID = homeObjData!.user_id!
            }
            else
            {
                userID = user_id
            }
        }
        else{
            if profileObject != nil {
                self._userBioLbl.text = profileObject.user_bio
                self._userNameLbl.text = (profileObject.user_firstname)! + " " + (profileObject.user_lastname)!
                self._userImage.sd_setImage(with:  URL(string:(profileObject.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
                userID = profileObject!.user_id!
            }
        }
        
        
        if viewType == "backShow"{
            _slideBtn.setImage(UIImage(named: "back-arrow"), for: .normal);
            _iconBtn.isHidden  = true
          
        }
       
      
        profileUserID = userID
        if GlobalMethod.userDetail?.user_id == userID {
            _passwordBtn.isHidden = false
            _editBtn.isHidden = false
            _heightConstant.constant = 0
            _followingBtn.isHidden = true
            _tableHeaderView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 320)
        }
        else
        {
            _passwordBtn.isHidden = true
            _editBtn.isHidden = true
            _followingBtn.isHidden = false
            _tableHeaderView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 350)
        }
    }
    
    func getUserProfile(showIndicator : Bool){
        var userId = ""
        if viewType == "backShow"{
            if homeObjData != nil {
                userId = homeObjData!.user_id!
            }
            else{
            userId = user_id
            }

        }
        else
        {
            userId = GlobalMethod.userDetail!.user_id!
        }
        let path = "type=editprofile&user_id=\(userId)"
        
        APIManager.shared.getRequest(values: path, showIndicator: showIndicator, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    let profileDict  = jsonData?.jsonDic!["data"] as! NSDictionary
                    if self.viewType != "backShow"{
                        GlobalMethod.shared.saveUserDefaultValue(key: profileObjectKey, value: profileDict as Any)
                        GlobalMethod.shared.getUserProfileObject()
                        self._userBioLbl.text = profileObject.user_bio
                        self._userNameLbl.text = (profileObject.user_firstname)! + " " + (profileObject.user_lastname)!
                        self._userImage.sd_setImage(with:  URL(string:(profileObject.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
                    }
                    else{
                        self._userBioLbl.text = profileDict["user_bio"] as? String
                        self._userNameLbl.text = "\(profileDict["user_firstname"] as! String)" + " " + "\(profileDict["user_lastname"] as! String)"
                        self._userImage.sd_setImage(with:  URL(string:("\(profileDict["user_profilepicture"] as! String)")), placeholderImage:UIImage(named: "placeHolder"))

                    }
                }
            }
        }
        
    }
    
    func getProfile(showIndicator : Bool){
        var userId = ""
        if viewType == "backShow"{
            if homeObjData != nil {
                userId = homeObjData!.user_id!
            }
            else{
                userId = user_id
            }
            
        }
        else
        {
            userId = GlobalMethod.userDetail!.user_id!
        }
        let path = "type=viewprofile&user_id=\(GlobalMethod.userDetail!.user_id!)&other_user_id=\(userId)&page_limit=\(page_limit)&page_num=\(page_num)"
        APIManager.shared.getRequest(values: path, showIndicator: showIndicator, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    let Dic = jsonData?.jsonDic!["data"] as! NSDictionary
                    print(Dic)
                    
                    if self.isLoadingList == false{
                        self.homeDataArray.removeAll()
                    }
                    let localArray = (jsonData?.jsonDic!["data"] as! NSDictionary)["review"] as! NSArray
                    for dict in localArray{
                        let obj = homeObjectData.init(json: dict as! [String : Any])
                        self.homeDataArray.append(obj)
                    }
                    if localArray.count > 0{
                        if self.isLoadingList == true {
                            self.isLoadingList = false
                            var indexPathsArray = [IndexPath]()
                            var totalCount = (self.homeDataArray.count - localArray.count ) - 1
                            for _ in 0..<localArray.count{
                                totalCount = totalCount + 1
                                let indexPath = IndexPath(row: totalCount, section: 0)
                                indexPathsArray.append(indexPath)
                            }
                            print(indexPathsArray)
                            print(self.homeDataArray.count)
                            self._tableView.beginUpdates()
                            self._tableView.insertRows(at: indexPathsArray, with: .automatic)
                            self._tableView.endUpdates()
                        }
                        else{
                            self._tableView.reloadData()
                        }
                    }
                    else
                    {
                        if self.isLoadingList == false{
                            self._tableView.reloadData()
                        }
                        self.page_num = self.page_num - 1
                        self._tableView.tableFooterView = nil
                        self.isLoadingList = false

                    }
                    self._postCountLbl.text = "\(self.homeDataArray.count)"
                    self._followerCountLbl.text = "\(Dic["user_followers"] as! Int)"
                    self._followingCountLbl.text = "\(Dic["user_followings"] as! Int)"
                    
                    self.user_followstatus = Dic["user_followstatus"] != nil ? getString(string: Dic["user_followstatus"] as Any) : "0"
                    let titleText = self.user_followstatus == "0" ? "Follow": self.user_followstatus == "1" ? "Requested" : self.user_followstatus == "2" ? "Unfollow" : "Follow Back"
                    self.setBtnColorAndText(str: self.user_followstatus == "0" || self.user_followstatus == "4" ? "0":"1")
                    self._followingBtn.tag  = Int(self.user_followstatus)!
                    self._followingBtn.setTitle(titleText, for: .normal)

                }
            }
        }
        
    }
    
    func setBtnColorAndText(str :String){
        self._followingBtn.layer.cornerRadius = 3
        
        if str == "1"{
            self._followingBtn.layer.borderColor  = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self._followingBtn.layer.borderWidth  = 1
            self._followingBtn.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
            self._followingBtn.backgroundColor  = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
        else{
            self._followingBtn.layer.borderColor  = UIColor.clear.cgColor
            self._followingBtn.layer.borderWidth  = 0
            self._followingBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            self._followingBtn.backgroundColor  = UIColor(red:1, green:0.95, blue:0, alpha:1)
            
        }
        
        
    }
    
    @IBAction func _followBtn(_ sender: UIButton) {
        if self.user_followstatus == "1" || sender.titleLabel?.text == "Follower"{
            return
        }
        else {
            DispatchQueue.main.async {
                let titleText = self.user_followstatus == "0" || self.user_followstatus == "" ? "Follow": self.user_followstatus == "1" ? "Requested" : self.user_followstatus == "2" ? "Unfollow" : "Follow Back"
                self.alertView._titleLbl.text = titleText + " " + self._userNameLbl.text!
                self.alertView._imageVIew.image = self._userImage.image
                self.alertView._oKbtn.tag = Int(self.user_followstatus)!
                self.alertView.btnSetup(cancel: "Cancel", ok: (sender.titleLabel?.text)!)
                self.alertView.delegate = self
                self.alertView.frame = UIScreen.main.bounds
                self.view.addSubview(self.alertView)
                
            }
        }
    }
    
    
    func followAndUnFollowApiMethod(type : String , tag : Int , serviceType : String){
        APIManager.shared.getRequest(values: type, showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    self.alertView.removeFromSuperview()
                    if serviceType == "Follow"{
                        self._followingBtn.setTitle("Requested", for: .normal)
                        self.setBtnColorAndText(str: "1")
                    }
                    else{
                        self._followingBtn.setTitle("Follow", for: .normal)
                        self.setBtnColorAndText(str: "0")
                    }
                    self.getProfile(showIndicator: false)

                    
                }
            }
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func didDotActionClick(sender: UIButton) {
        self.setupChooseArticleDropDown(listArray: ["Edit","Delete"], choosebutton: sender )
        self.chooseArticleDropDown.show()
    }
    
    func setupChooseArticleDropDown(listArray : NSArray , choosebutton : UIButton ) {
        chooseArticleDropDown.anchorView = choosebutton
        chooseArticleDropDown.bottomOffset = CGPoint(x: -60, y: choosebutton.frame.size.height)
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseArticleDropDown.dataSource = listArray as! [String]
        // Action triggered on selection
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            self?.chooseArticleDropDown.hide()
            if item == "Edit"{
                let viewObject = ReviewPostVC.init(nibName: "ReviewPostVC", bundle: nil)
                viewObject.postType = "Edit"
                viewObject.postObject = self?.homeDataArray[choosebutton.tag]
                self?.navigationController?.pushViewController(viewObject, animated: true)
            }
            else{
                let alertView = UIAlertController(title: "Grub n Chill", message: "Are you sure want to delete?", preferredStyle: .alert)
                let canclAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel , handler: nil)
                let ok = UIAlertAction(title: "Ok", style: .default, handler: {  _ in
                    self?.deletePostMethod(tag: choosebutton.tag)
                })
                alertView.addAction(canclAction)
                alertView.addAction(ok)
                self?.present(alertView, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc func clickImageView(sender : UITapGestureRecognizer){
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let cell = _tableView.cellForRow(at: indexPath) as? HomeImageCell
            self.openImageViewController(sender: cell!._imageView)
        }
        
        
    }
    
   
    @IBAction func _clickFollowingBtn(_ sender: Any) {
        let viewObject = FollowersUserListVC.init(nibName: "FollowersUserListVC", bundle: nil)
        viewObject.user_id = profileUserID
        viewObject.typeUrl = "Following"

        self.navigationController?.pushViewController(viewObject, animated: true);

    }
    
    @IBAction func _clickFollowersBtn(_ sender: Any) {
        let viewObject = FollowersUserListVC.init(nibName: "FollowersUserListVC", bundle: nil)
        viewObject.user_id = profileUserID
        viewObject.typeUrl = "Follower"
        self.navigationController?.pushViewController(viewObject, animated: true);

    }
    
    
    
    @objc func clickCommentButton(sender : UIButton){
        self.sendCommentListView(obj: homeDataArray[sender.tag])
    }
    
//    func sendCommentListView(obj : homeObjectData){
//        let viewObjet = CommentListVC.init(nibName: "CommentListVC", bundle: nil)
//        viewObjet.postObject = obj
//        self.present(viewObjet, animated: true, completion: nil)
//    }
//    
    
    @objc func clickLikeButton(sender : UIButton){
        self.likeAndUnlikeMethod(tag: sender.tag)
    }
    
    @objc func clickShareButton(sender : UIButton)
    {
        self.sharePostMethod(tag:sender.tag)
        
    }
    func updateArrayObj(obj : homeObjectData){
        if let row = self.homeDataArray.index(where: {$0.review_id == obj.review_id}) {
            self.homeDataArray[row] = obj
            self._tableView.beginUpdates()
            self._tableView.reloadRows(at: [IndexPath(item: row, section: 0)], with: .automatic)
            self._tableView.endUpdates()
            
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height ) && !isLoadingList){
            isLoadingList = true
            page_num = page_num + 1
            self.getProfile(showIndicator: false)
        }
        
    }
    
}

extension ProfileVC{
    
    //MARK: - Home Page All Api Method
    //MARK: - Like and Unlike Post Method
    func likeAndUnlikeMethod(tag : Int){
        var obj = homeDataArray[tag]
        let status = obj.review_userliked == "1" ? "2" : "1"
        APIManager.shared.getRequest(values: "type=getreviewsstatus&review_id=\(obj.review_id!)&status=\(status)&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    var count = Int(obj.review_likedcount!)
                    count = status == "1" ? count! + 1 : count! - 1
                    obj.review_likedcount = "\(String(describing: count!))"
                    obj.review_userliked = status == "1" ? "1" : "0"
                    self.updateArrayObj(obj: obj)
                    
                }
            }
        }
    }
    

    
    
    //MARK: - Delete Post Method
    func deletePostMethod(tag : Int){
        let obj = homeDataArray[tag]
        APIManager.shared.getRequest(values: "type=deleteReviews&user_id=\(GlobalMethod.userDetail!.user_id!)&review_id=\(obj.review_id!)", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    self.homeDataArray.remove(at: tag)
                    self._tableView.deleteRows(at: [IndexPath(row: tag, section: 0)], with: .automatic)
                }
            }
        }
    }
    
    //MARK: - Share Post Method
    func sharePostMethod(tag : Int){
        sharePost = homeDataArray[tag]
        self.shareONFacebook()
        
    }
    
    
    
}

extension ProfileVC {
    
    @objc func clickResturantName(sender : UITapGestureRecognizer){
        
        let reviewPostVC = RestaurantDetailVC.init(nibName: "RestaurantDetailVC", bundle: nil)
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let obj = homeDataArray[indexPath.row]
            reviewPostVC.restaurantId = obj.restaurant_id!
        }
        self.navigationController?.pushViewController(reviewPostVC, animated: true)
    }
    
    
    @objc func clickLiked(sender : UITapGestureRecognizer){
        let viewObjet = LikedUserListVC.init(nibName: "LikedUserListVC", bundle: nil)
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            viewObjet.postObject = homeDataArray[indexPath.row]
        }
        self.present(viewObjet, animated: true, completion: nil)
    }
    
    
    
    @objc func clickCommentCount(sender : UITapGestureRecognizer){
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            self.sendCommentListView(obj: homeDataArray[indexPath.row])
        }
    }
    
    @objc func clickUserDetail(sender : UITapGestureRecognizer){
    }
}
extension ProfileVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeDataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            self._tableView.tableFooterView = spinner
            self._tableView.tableFooterView?.isHidden = false
        }
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = homeDataArray[indexPath.row]
        let identifier = "HomeImageCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeImageCell
        if cell == nil {
            tableView.register(UINib(nibName: "HomeImageCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeImageCell
        }
        cell?.setCellData(obj: obj, indexPath: indexPath, contoller: self)
        
       cell?._userImage.sd_setImage(with:  URL(string:(obj.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))

        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.homeDataArray.count > 0 {
            
            let viewObject = SinglePostVC.init(nibName: "SinglePostVC", bundle: nil)
            viewObject.reviewObject = homeDataArray[indexPath.row]
            viewObject.reviewId = homeDataArray[indexPath.row].review_id!
            viewObject.delegate = self
            self.navigationController?.pushViewController(viewObject, animated: true)

//            if GlobalMethod.userDetail?.user_id == self.homeDataArray[indexPath.row].user_id{
//                let viewObject = ReviewPostVC.init(nibName: "ReviewPostVC", bundle: nil)
//                viewObject.postType = "Edit"
//                viewObject.controllerType = "Profile"
//                viewObject.postObject = self.homeDataArray[indexPath.row]
//                self.navigationController?.pushViewController(viewObject, animated: true)
//            }
        }
        

   }
    
    
}

extension ProfileVC : AlertVDelegate {
    
    func didCancelClick() {
        alertView.removeFromSuperview()
    }
    func didOkClick(title: String, tag: Int) {
        
        var path = ""
        if tag == 4 || tag == 0 {
            path  =  "type=followuser&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(String(describing: profileUserID))&follow_state=1"
            self.followAndUnFollowApiMethod(type: path, tag: tag,serviceType: "Follow")
        }
        else if tag == 2{
            path  =  "type=followuser&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(String(describing: profileUserID))&follow_state=2"
            self.followAndUnFollowApiMethod(type: path, tag: tag,serviceType: "Unfollow")
        }
    }
}
extension ProfileVC : CommentDelegate{
    func commentCountUpdate(value: homeObjectData) {
        self.updateArrayObj(obj: value)
    }
}

extension ProfileVC : LikeCommetDelegate {
    func likeCountUpdate(value: homeObjectData) {
        self.updateArrayObj(obj: value)

    }
    
    func deleteReviewPost(value: homeObjectData) {
        if let row = self.homeDataArray.index(where: {$0.review_id == value.review_id}) {
            self.homeDataArray.remove(at: row)
            self._tableView.deleteRows(at: [IndexPath(row: row, section: 0)], with: .automatic)
            
        }
    }
}
