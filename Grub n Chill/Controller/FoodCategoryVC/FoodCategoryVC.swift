//
//  FoodCategoryVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 22/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class FoodCategoryVC: ParentClass {
    @IBOutlet weak var _backBtn: UIButton!
    @IBOutlet weak var _tableView: UITableView!
    var foodCategoryArray = [foodCategoryObject]()
    @IBOutlet weak var _selectedCount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _backBtn.setImage(UIImage(named: "back-arrow"), for: .normal);
        self.getFoodCategory()
        
        // Do any additional setup after loading the view.
    }
    
    //http://www.letsgrubnchill.com/api/Api.php?app=grubnchill&type=getfoodcategory
    
    @IBAction func _addbtn(_ sender: Any) {
        
        let categoryCount = foodCategoryArray.filter({m in
            m.selected == "1" }
            ).map({$0.foodcategory_name!}).count
        if categoryCount > 0 {
            
            selectedFoodCategoryObj = foodCategoryArray.filter({m in
                m.selected == "1" }
            )
//            selectedFoodCategory = foodCategoryArray.filter({m in
//                m.selected == "1" }
//                ).map({$0.foodcategory_name!}).joined(separator: ",")
            self.navigationController?.popViewController(animated: true)
        }
    }
    func getFoodCategory(){
        
        APIManager.shared.getRequest(values: "&type=getfoodcategory", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                self.foodCategoryArray.removeAll()
                if error == nil{
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray{
                        let obj = foodCategoryObject.init(json: dict as! [String : Any])
                        self.foodCategoryArray.append(obj)
                    }
                    
                    var index = 0
                    for case var obj  in self.foodCategoryArray {
                        for dummyObj in selectedFoodCategoryObj {
                            if dummyObj.foodcategory_id == obj.foodcategory_id {
                                obj.selected = "1"
                                self.foodCategoryArray[index] = obj
                            }
                        }
                        index += 1
                    }
                    
                   
                }
                self._tableView.reloadData()
                
            }
        }
    }
    
    
    @IBAction func _backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension FoodCategoryVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodCategoryArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "FoodCategoryCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FoodCategoryCell
        if cell == nil {
            tableView.register(UINib(nibName: "FoodCategoryCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FoodCategoryCell
        }
        let obj = foodCategoryArray[indexPath.row]
        cell?._nameLbl.text = obj.foodcategory_name
        if obj.selected == "0"{
            cell!._checkIcon.image = UIImage(named: "uncheckIcon")
        }
        else{
            cell!._checkIcon.image = UIImage(named: "checkIcon-1")
            
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var obj = foodCategoryArray[indexPath.row]
        if obj.selected == "0"{
            obj.selected = "1"
        }
        else{
            obj.selected = "0"
        }
        foodCategoryArray[indexPath.row] = obj
        self._tableView.beginUpdates()
        self._tableView.reloadRows(at: [indexPath], with: .automatic)
        self._tableView.endUpdates()
        
        
        let categoryCount = foodCategoryArray.filter({m in
            m.selected == "1" }
            ).map({$0.foodcategory_name!}).count
        
        
        if categoryCount == 0 {
            _selectedCount.text = "0 Selected"
        }
        else{
            _selectedCount.text = "\(categoryCount) Selected"
        }
        
        
        
    }
    
    
}
