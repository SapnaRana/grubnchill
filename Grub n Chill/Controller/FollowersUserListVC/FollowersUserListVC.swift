//
//  FollowersUserListVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 23/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Windless

class FollowersUserListVC: ParentClass {
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _topView: UIView!
    var animationStop = false

    @IBOutlet weak var _titleLbl: UILabel!
    var user_id = ""
    var followingUserArray = [followingUserData]()
    var typeUrl = ""
    var alertView = AlertView.instantiateFromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeaderView(view: _topView)
        _headerView._slideButton.setImage(UIImage(named: "back-arrow"), for: .normal);
        _headerView._slideButton.tag = backButtonTag
        _headerView._icon.isHidden   = true
        _headerView._wheelBtn.isHidden = true
        _headerView._searchTextField.delegate = self
        if typeUrl == "Following"{
            _titleLbl.text = "Following"
        }
        else
        {
            _titleLbl.text = "Followers"
        }
        self.getAllFollowerMethod(searchTxt: "")
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        if animationStop == false {
            self.startAnimation(view: _tableView)
        }
    }
    
    //MARK:-  GET ALL Follower Method
    func getAllFollowerMethod(searchTxt : String){
        
        var path = ""
        if typeUrl == "Following"{
            path = "myfollowings"
        }
        else
        {
            path = "myfollowers"
        }
        
        print("&type=\(path)&current_user_id=\(user_id)&user_id=\(GlobalMethod.userDetail!.user_id!)")
        APIManager.shared.getRequest(values: "&type=\(path)&current_user_id=\(user_id)&user_id=\(GlobalMethod.userDetail!.user_id!)&s=\(searchTxt)", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                self.followingUserArray.removeAll()
                if error == nil {
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray{
                        let obj = followingUserData.init(json: dict as! [String : Any])
                        self.followingUserArray.append(obj)
                    }
                }
                
                self.stopAnimation(view: self._tableView)
                self.animationStop = true

                
                self._tableView.reloadData()

            }
        }
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func clickFolowerBtn(sender : UIButton){
        
        let obj = followingUserArray[sender.tag];
        if obj.following_status == "1" || sender.titleLabel?.text == "Follower"{
            return
        }
        else {
            DispatchQueue.main.async {
                let titleText = obj.following_status == "0" || obj.following_status == "" ? "Follow": obj.following_status == "1" ? "Requested" : obj.following_status == "2" ? "Unfollow" : "Follow Back"
                self.alertView._titleLbl.text = titleText + " " + obj.user_firstname! + " " + obj.user_lastname!
                self.alertView._imageVIew.sd_setImage(with:  URL(string:(obj.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
                self.alertView._oKbtn.tag = sender.tag
                self.alertView.btnSetup(cancel: "Cancel", ok: (sender.titleLabel?.text)!)
                self.alertView.delegate = self
                self.alertView.frame = UIScreen.main.bounds
                self.view.addSubview(self.alertView)

            }
        }
        
     
        
    }
    
    
 
    
    func followAndUnFollowApiMethod(type : String , tag : Int , serviceType : String){
        APIManager.shared.getRequest(values: type, showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    self.getAllFollowerMethod(searchTxt: "")
                    self.alertView.removeFromSuperview()

//                    if serviceType == "Follow"{
//                        self._followingBtn.setTitle("Requested", for: .normal)
//                        self.setBtnColorAndText(str: "1")
//                    }
//                    else{
//                        self._followingBtn.setTitle("Follow", for: .normal)
//                        self.setBtnColorAndText(str: "0")
//                    }
                    
                }
            }
        }
    }
    
    
    
    @objc func clickProfile(sender : UIGestureRecognizer){
        
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let Obj = followingUserArray[indexPath.row]
            let viewObject = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
            viewObject.viewType = "backShow"
            viewObject.user_id = Obj.user_id!
            self.navigationController?.pushViewController(viewObject, animated: true)
        }
        
    }
    
}

extension FollowersUserListVC : AlertVDelegate {
    
    func didCancelClick() {
        alertView.removeFromSuperview()
    }
    func didOkClick(title: String, tag: Int) {
        
        
        
        
        let obj = followingUserArray[tag];
        var path = ""
        
        if obj.following_status == "4" || obj.following_status == "0" || obj.following_status == "" {
            path  =  "type=followuser&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(String(describing: obj.user_id!))&follow_state=1"
            self.followAndUnFollowApiMethod(type: path, tag: tag,serviceType: "Follow")
        }
        else if obj.following_status == "2"{
            path  =  "type=followuser&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(String(describing: obj.user_id!))&follow_state=2"
            self.followAndUnFollowApiMethod(type: path, tag: tag,serviceType: "Unfollow")
            
        }
        
        
        
        
        
//        let obj = followingUserArray[tag]
//        var path = ""
//        if title == "Follow" {
//            path  =  "type=followuser&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(String(describing: obj.reviewliked_userid!))&follow_state=1"
//        }
//        else{
//            path  =  "type=followuser&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(String(describing: obj.reviewliked_userid!))&follow_state=2"
//        }
//        self.followAndUnFollowApiMethod(type: path, tag: tag,serviceType: title)
        print(title)
    }
}
extension FollowersUserListVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return animationStop == false ? 10 : followingUserArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "FollowingUserCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FollowingUserCell
        if cell == nil {
            tableView.register(UINib(nibName: "FollowingUserCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FollowingUserCell
        }
        
        if animationStop == true {
            let obj = followingUserArray[indexPath.row]
            cell?._name.text = obj.user_firstname! + " " + obj.user_lastname!
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(clickProfile(sender:)))
            cell?._profileImage.isUserInteractionEnabled = true
            cell?._profileImage.addGestureRecognizer(tap);
            
            let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(clickProfile(sender:)))
            cell?._name.isUserInteractionEnabled = true
            cell?._name.addGestureRecognizer(tapGesture4)
            
            
            cell?._profileImage.sd_setImage(with:  URL(string:(obj.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
            
            cell?._btn.addTarget(self, action: #selector(clickFolowerBtn(sender:)), for: .touchUpInside)
            cell?._btn.tag = indexPath.row;
            cell?._btn.isHidden =  obj.user_id ==  GlobalMethod.userDetail!.user_id! ?  true : false
            
            let titleText = obj.following_status == "0" || obj.following_status == "" ? "Follow": obj.following_status == "1" ? "Requested" : obj.following_status == "2" ? "Unfollow" : "Follow Back"
            cell?.setBtnColorAndText(str: obj.following_status == "0" || obj.following_status == "" || obj.following_status == "4" ? "0":"1")
            cell?._btn.setTitle(titleText, for: .normal)
            
        }

        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}


extension FollowersUserListVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text as NSString? {
            let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
            DispatchQueue.main.async {
            self.getAllFollowerMethod(searchTxt: txtAfterUpdate)
            }
            
        }

        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        DispatchQueue.main.async {
            self.getAllFollowerMethod(searchTxt: textField.text!)
        }
        return true
    }
}
