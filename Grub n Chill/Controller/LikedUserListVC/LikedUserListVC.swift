//
//  LikedUserListVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 02/10/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import SDWebImage
import Windless

class LikedUserListVC: ParentClass {
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _topView: UIView!
    var postObject : homeObjectData? = nil
    var animationStop = false
    var likeUserData = [likeUserObjectData]()
    var alertView = AlertView.instantiateFromNib()
    
    
    var userModel: UserModel!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeaderView(view: _topView)
        _headerView._slideButton.setImage(UIImage(named: "back-arrow"), for: .normal);
        _headerView._slideButton.tag = dismissButtonTag
        _headerView._icon.isHidden   = true
        _headerView._wheelBtn.isHidden = true
        _headerView._searchBtn.isHidden = true
        
        self.getLikeUserData()
        // Do any additional setup after loading the view.
    }
    
//    @objc func stopAnimation(){
//        _tableView.windless.end()
//        animationStop = true
//        _tableView.reloadData()
//    }
//    @objc func startAnimation(){
//        if animationStop == false {
//        _tableView.windless
//            .apply {
//                $0.beginTime = 2
//                $0.duration = 4
//                $0.animationLayerOpacity = 0.5
//            }
//            .start()
//        }
//    }
    override func viewDidLayoutSubviews() {
        if !animationStop {
            self.startAnimation(view: _tableView)
        }
    }
    
    
    @objc func clickBtnF(sender : UIButton){
        DispatchQueue.main.async {
            let obj = self.userModel.users![sender.tag];
            if obj.following_status == "1"{
                return
            }
            let titleText = obj.following_status == "0" || obj.following_status == "" ? "Follow": obj.following_status == "1" ? "Requested" : obj.following_status == "2" ? "Unfollow" : "Follow Back"
            self.alertView._titleLbl.text = titleText + " " + obj.user_firstname! + " " + obj.user_lastname!
            self.alertView._imageVIew.sd_setImage(with:  URL(string:(obj.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
            
            self.alertView._oKbtn.tag = sender.tag
            self.alertView.btnSetup(cancel: "Cancel", ok: (sender.titleLabel?.text)!)
            self.alertView.delegate = self
            self.alertView.frame = UIScreen.main.bounds
            self.self.view.addSubview(self.alertView)
        }
        
        
    }
    
    func getLikeUserData(){
        
        
        // setup view model
        userModel = UserModel()
        userModel.fetchUsers(value: "type=getreviewslikedlist&page_num=1&page_limit=10&user_id=\(GlobalMethod.userDetail!.user_id!)&review_id=\(postObject!.review_id!)") { (isSuccess) in
            if isSuccess {
                DispatchQueue.main.async {
                    self.stopAnimation(view: self._tableView)
                    self.animationStop = true
                    self._tableView.reloadData()
                }
            }
        }
        
        // fetch data
//        viewModel.fetchGirls { (isSuccess) in
//            if isSuccess {
//                // reload data
//                self.tableView.reloadData()
//            }
//        }
        
        
        
//        APIManager.shared.getRequest(values: "type=getreviewslikedlist&page_num=1&page_limit=10&user_id=\(GlobalMethod.userDetail!.user_id!)&review_id=\(postObject!.review_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
//            DispatchQueue.main.async {
//                self.stopAnimation(view: self._tableView)
//                self.animationStop = true
//                self.likeUserData.removeAll()
//                if error == nil{
//                    print(jsonData?.jsonDic!)
//                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
//                    for dict in localArray{
//                        let obj = likeUserObjectData.init(json: dict as! [String : Any])
//                        self.likeUserData.append(obj)
//                    }
//                    self._tableView.reloadData()
//                }
//            }
//        }
    }
    
    func followAndUnFollowApiMethod(type : String , tag : Int , serviceType : String){
        APIManager.shared.getRequest(values: type, showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    print(jsonData?.jsonDic!)
                    var obj = self.userModel.users![tag]
                    if serviceType == "Follow"{
                       obj.following = "1"
                        obj.following_status = "1"
                    }
                    else{
                        obj.following = "0"
                        obj.following_status = "0"


                    }
                    self.userModel.users![tag] = obj
                    self.alertView.removeFromSuperview()
                    self._tableView.beginUpdates()
                    self._tableView.reloadRows(at: [IndexPath(row: tag, section: 0)], with: .automatic)
                    self._tableView.endUpdates()

                }
            }
        }
    }
    
    @objc func clickUserDetail(sender : UITapGestureRecognizer){
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let Obj = self.userModel.users![indexPath.row]
            let viewObject = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
            viewObject.viewType = "backShow"
            viewObject.user_id = Obj.reviewliked_userid!
            self.navigationController?.pushViewController(viewObject, animated: true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension LikedUserListVC : AlertVDelegate {
    
    func didCancelClick() {
        alertView.removeFromSuperview()
    }
    func didOkClick(title: String, tag: Int) {
        let obj = self.userModel.users![tag]
        var path = ""
        if title == "Follow" {
            path  =  "type=followuser&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(String(describing: obj.reviewliked_userid!))&follow_state=1"
        }
        else{
            path  =  "type=followuser&user_id=\(GlobalMethod.userDetail!.user_id!)&follow_id=\(String(describing: obj.reviewliked_userid!))&follow_state=2"
        }
        self.followAndUnFollowApiMethod(type: path, tag: tag,serviceType: title)
        print(title)
    }
}
extension LikedUserListVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if animationStop == true {
            return userModel.users!.count
                //self.likeUserData.count
        }
        else{
            return 10
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "FollowingUserCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FollowingUserCell
        if cell == nil {
            tableView.register(UINib(nibName: "FollowingUserCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FollowingUserCell
        }
        if animationStop == true {
//            let obj = self.likeUserData[indexPath.row]
            let obj = userModel.users![indexPath.row]

            cell?._profileImage.isUserInteractionEnabled = true
            cell?._name.text = obj.user_firstname! + " " + obj.user_lastname!
            cell?._btn.isHidden =  obj.reviewliked_userid ==  GlobalMethod.userDetail!.user_id! ?  true : false
            let titleText = obj.following_status == "0" || obj.following_status == "" || obj.following_status == nil ? "Follow": obj.following_status == "1" ? "Requested" : obj.following_status == "2" ? "Unfollow" : "Follow Back"
            cell?.setBtnColorAndText(str: obj.following_status == "0" || obj.following_status == "" || obj.following_status == "4" ? "0":"1")
            cell?._btn.setTitle(titleText, for: .normal)
            cell?._btn.addTarget(self, action: #selector(clickBtnF(sender:)), for: .touchUpInside)
            cell?._btn.tag = indexPath.row
            cell?._profileImage.sd_setImage(with:  URL(string:(obj.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
            
            let tapGesture5 = UITapGestureRecognizer(target: self, action: #selector(clickUserDetail(sender:)))
            cell?._profileImage.isUserInteractionEnabled = true
            cell?._profileImage.addGestureRecognizer(tapGesture5)
            
            let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(clickUserDetail(sender:)))
            cell?._name.isUserInteractionEnabled = true
            cell?._name.addGestureRecognizer(tapGesture4)

            
        }
        
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}

