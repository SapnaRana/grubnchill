//
//  ShareUserListVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 06/10/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire

class ShareUserListVC: ParentClass {
    var postObject : homeObjectData? = nil
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _topView: UIView!
    var animationStop = false

    var shareUserList = [shareUserObjectData]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setHeaderView(view: _topView)
        _headerView._slideButton.setImage(UIImage(named: "back-arrow"), for: .normal);
        _headerView._slideButton.tag = dismissButtonTag
        _headerView._icon.isHidden   = true
        _headerView._wheelBtn.isHidden = true
        _headerView._searchBtn.isHidden = true
        
        self.getShareUserData()
        // Do any additional setup after loading the view.
    }
    
//    @objc func stopAnimation(){
//        _tableView.windless.end()
//        animationStop = true
//        _tableView.reloadData()
//    }
    
    override func viewDidLayoutSubviews() {
        self.startAnimation(view: _tableView)
    }
    
    func getShareUserData(){
        APIManager.shared.getRequest(values: "type=getreviewssharedlist&page_num=1&page_limit=10&user_id=\(GlobalMethod.userDetail!.user_id!)&review_id=\(postObject!.review_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                self.stopAnimation(view: self._tableView)
                self.animationStop = true
                self.shareUserList.removeAll()
                if error == nil{
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray{
                        let obj = shareUserObjectData.init(json: dict as! [String : Any])
                        self.shareUserList.append(obj)
                    }
                }
                self._tableView.reloadData()

            }
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ShareUserListVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if animationStop == true {
            return self.shareUserList.count
        }
        else{
            return 10
            
        }    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "FollowingUserCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FollowingUserCell
        if cell == nil {
            tableView.register(UINib(nibName: "FollowingUserCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FollowingUserCell
        }
        
     
        
        
        if animationStop == true {
            let obj = self.shareUserList[indexPath.row]
            cell?._profileImage.isUserInteractionEnabled = true
            cell?._name.text = obj.user_firstname! + " " + obj.user_lastname!
            cell?._btn.isHidden = true;
            cell?._profileImage.sd_setImage(with:  URL(string:(obj.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
            
            
            if obj.following == "1"{
                cell?._btn.setTitle("Following", for: .normal)
                cell?._btn.layer.borderColor  = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                cell?._btn.layer.borderWidth  = 1
                cell?._btn.setTitleColor(#colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1), for: .normal)
                
            }
//            else if obj.following == "0"{
//                cell?._btn.setTitle("Follow", for: .normal)
//                cell?._btn.layer.borderColor  = UIColor.clear.cgColor
//                cell?._btn.layer.borderWidth  = 0
//                cell?._btn.backgroundColor = #colorLiteral(red: 1, green: 0.9568627451, blue: 0, alpha: 1)
//                cell?._btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
//            }
             if obj.following_id == ""{
                cell?._btn.isHidden = true
            }
            
        }

        
        //        if animationStop == true {
        //            let obj = self.likeUserData[indexPath.row]
        //            cell?._profileImage.isUserInteractionEnabled = true
        //            cell?._btn.isHidden = true;
        //            cell?._profileImage.sd_setImage(with:  URL(string:(obj.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
        //        }
        
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}
