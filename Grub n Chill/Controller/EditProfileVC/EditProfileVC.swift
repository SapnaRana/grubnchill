//
//  EditProfileVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 23/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Malert
import SDWebImage
import Alamofire
import DropDown


class EditProfileVC: ParentClass {
    var indexPathSelect : IndexPath!
    @IBOutlet weak var _userImage: UIImageView!
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _topView: UIView!
    @IBOutlet var _tableHeaderView: UIView!
    var selectedDateValue = ""
    let titleArray = ["Email Address","First Name","Last Name","Bio","Address 1","Address 2","Country","State","City","Post Code","Phone","Mobile","Birth Date","Gender"]
    var datePicker = UIDatePicker()
    var profileDict = NSMutableDictionary()
    var genderType = "Male"
    var countryId  = ""
    var stateID  = ""

    var countryArray = [countryObjectData]()
    var searchCountryObject = [countryObjectData]()
    var stateArray = [countryObjectData]()
    let chooseArticleDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown,
            ]
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateSelect(sender:)), for: .valueChanged)
        self.setHeaderView(view: _topView)
        
        
        
        _userImage.layer.cornerRadius = _userImage.bounds.width / 2
        _userImage.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _userImage.layer.borderWidth = 1
        
        
        profileDict.setValue(profileObject.user_email!, forKey: "Email Address")
        profileDict.setValue(profileObject.user_firstname!, forKey: "First Name")
        profileDict.setValue(profileObject.user_lastname!, forKey: "Last Name")
        profileDict.setValue(profileObject.user_bio!, forKey: "Bio")
        profileDict.setValue(profileObject.user_address1!, forKey: "Address 1")
        profileDict.setValue(profileObject.user_address2!, forKey: "Address 2")
        profileDict.setValue(profileObject.user_city!, forKey: "City")
        profileDict.setValue(profileObject.user_state!, forKey: "State")
        profileDict.setValue(profileObject.user_country!, forKey: "Country")
        profileDict.setValue(profileObject.user_postcode!, forKey: "Post Code")
        profileDict.setValue(profileObject.user_phone!, forKey: "Phone")
        profileDict.setValue(profileObject.user_mobile!, forKey: "Mobile")
        
        if profileObject.user_dob != ""{
            let dateGet = profileObject.user_dob!.dateFromString(appformate: appDateFormate, serverFormate: serverDateFormate)
            profileDict.setValue(dateGet, forKey: "Birth Date")
        }
        else{
            profileDict.setValue(profileObject.user_dob!, forKey: "Birth Date")
            
        }
        
        
        let user_gender = profileObject.user_gender == "" ||  profileObject.user_gender == "Male" ? "Male" : "Female"
        profileDict.setValue(user_gender, forKey: "Gender")
        genderType = user_gender
        
        _tableView.tableHeaderView = _tableHeaderView
        _headerView._slideButton.setImage(UIImage(named: "back-arrow"), for: .normal);
        _headerView._slideButton.tag = backButtonTag
        _headerView._icon.isHidden   = true
        _headerView._wheelBtn.isHidden = true
        _headerView._saveChangeBtn.isHidden = false
        _headerView._searchBtn.isHidden = true
        
        _userImage.sd_setImage(with:  URL(string:(profileObject.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
        
        
        self.camerAPopUpSetUp()
        self.getCountry()
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - Get Country Method
    func getCountry(){
        let path = "type=getCountries"
        APIManager.shared.getRequest(values: path, showIndicator: true, viewController: self) { (error, JsonData) in
            DispatchQueue.main.async {
                if JsonData?.jsonDic! != nil {
                    self.showMessage(msg: JsonData?.jsonDic!["message"] as! String)
                }
                self.countryArray.removeAll()
                if error == nil {
                    let localArray  = JsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray {
                        let obj = countryObjectData.init(json: dict as! [String : Any])
                        self.countryArray.append(obj)
                    }
                }
                else{
                    
                }
                self._tableView.reloadData()
                
            }
        }
    }
    
    // MARK: - Get State Method
    func getState(countryid : String){
        let path = "type=getStates&countryid=\(countryid)"
        APIManager.shared.getRequest(values: path, showIndicator: true, viewController: self) { (error, JsonData) in
            DispatchQueue.main.async {
                if JsonData?.jsonDic! != nil {
                    self.showMessage(msg: JsonData?.jsonDic!["message"] as! String)
                }
                self.stateArray.removeAll()
                if error == nil {
                    let localArray  = JsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray {
                        let obj = countryObjectData.init(json: dict as! [String : Any])
                        self.stateArray.append(obj)
                    }
                }
                else{
                    
                }
                self._tableView.reloadData()
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if avatarObj != nil {
            _userImage.sd_setImage(with:  URL(string:(avatarObj.path!)), placeholderImage:UIImage(named: "placeHolder"))
        }
    }
    
    @IBAction func _editPicClick(_ sender: Any) {
        present(examples[0].malert, animated: true)
        
    }
    
    
    @objc func dateSelect(sender : UIDatePicker){
        let date = appDateFormate.stringFromDate(datePicker: datePicker)
        profileDict.setValue(date, forKey: "Birth Date")
        selectedDateValue = serverDateFormate.stringFromDate(datePicker: datePicker)
        let cell = _tableView.cellForRow(at:indexPathSelect!) as! TextFieldCell
        cell._txtField.text = date
    }
    
    private func camerAPopUpSetUp() {
        let example4View = CameraPopUp.instantiateFromNib()
        example4View.delegate = self
        
        let alert = Malert(customView: example4View)
        alert.animationType = .fadeIn
        alert.backgroundColor = UIColor(red:0.47, green:0.53, blue:0.80, alpha:1.0)
        alert.buttonsAxis = .horizontal
        alert.buttonsHeight = 60
        alert.separetorColor = .clear
        
        //        let buttonsColor = UIColor(red:0.36, green:0.42, blue:0.75, alpha:1.0)
        
        //        let firstAction = MalertAction(title: "GOT IT", backgroundColor: buttonsColor)
        //        firstAction.tintColor = .white
        //        alert.addAction(firstAction)
        //
        //        let secondAction = MalertAction(title: "LOOK UP", backgroundColor: buttonsColor)
        //        secondAction.tintColor = .white
        //        alert.addAction(secondAction)
        
        examples.append(Example(title: "Example 2", malert: alert))
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewDidDisappear(_ animated: Bool) {
        avatarObj = nil
    }
    
    @objc override func clickSaveBtn(sender : UIButton){
        
        self.view.endEditing(true)
        print(profileDict)
        
        //        let titleArray = ["Email Address","First Name","Last Name","Bio","Address 1","Address 2","City","State","Country","Post Code","Phone","Mobile","Birth Date","user_gender"]
        
        var user_firstname = ""
        if profileDict["First Name"] != nil{
            user_firstname = (profileDict["First Name"] as? String)!
        }
        var user_lastname = ""
        if profileDict["First Name"] != nil{
            user_lastname = (profileDict["Last Name"] as? String)!
        }
        var user_bio = ""
        if profileDict["Bio"] != nil{
            user_bio = (profileDict["Bio"] as? String)!
        }
        var user_address1 = ""
        if profileDict["Address 1"] != nil{
            user_address1 = (profileDict["Address 1"] as? String)!
        }
        var user_address2 = ""
        if profileDict["Address 2"] != nil{
            user_address2 = (profileDict["Address 2"] as? String)!
        }
        var user_city = ""
        if profileDict["City"] != nil{
            user_city = (profileDict["City"] as? String)!
        }
        var user_state = ""
        if profileDict["State"] != nil{
            user_state = (profileDict["State"] as? String)!
        }
        var user_country = ""
        if profileDict["Country"] != nil{
            user_country = (profileDict["Country"] as? String)!
        }
        var user_postcode = ""
        if profileDict["Post Code"] != nil{
            user_postcode = (profileDict["Post Code"] as? String)!
        }
        var user_phone = ""
        if profileDict["Phone"] != nil{
            user_phone = (profileDict["Phone"] as? String)!
        }
        var user_mobile = ""
        if profileDict["Mobile"] != nil{
            user_mobile = (profileDict["Mobile"] as? String)!
        }
        var user_dob = ""
        if profileDict["Birth Date"] != nil{
            user_dob = (profileDict["Birth Date"] as? String)!
        }
        
        if user_country == "" {
            self.showMessage(msg: "Please Select Country")

            return
        }
        if user_state == "" {
            self.showMessage(msg: "Please Select State")

            return
        }
        
//        user_gender
        var value = ""
        if avatarObj != nil{
            value = "user_id=\(GlobalMethod.userDetail!.user_id! as Any)&user_firstname=\(user_firstname)&user_lastname=\(user_lastname)&user_bio=\(user_bio)&user_address1=\(user_address1)&user_address2=\(user_address2)&user_city=\(user_city)&user_state=\(user_state)&user_country=\(user_country)&user_postcode=\(user_postcode)&user_phone=\(user_phone)&user_mobile=\(user_mobile)&user_dob=\(selectedDateValue)&type=updateprofile&user_imagetype=bitmoji&user_imageid=\(avatarObj.id!)&user_gender=\(self.genderType)&user_stateid=\(self.stateID)&user_countryid=\(self.countryId)"
            self.saveProfileData(value: value, imageArray: [])
        }
        else{
            value = "user_id=\(GlobalMethod.userDetail!.user_id! as Any)&user_firstname=\(user_firstname)&user_lastname=\(user_lastname)&user_bio=\(user_bio)&user_address1=\(user_address1)&user_address2=\(user_address2)&user_city=\(user_city)&user_state=\(user_state)&user_country=\(user_country)&user_postcode=\(user_postcode)&user_phone=\(user_phone)&user_mobile=\(user_mobile)&user_dob=\(selectedDateValue)&type=updateprofile&user_imagetype=image&user_gender=\(self.genderType)&user_stateid=\(self.stateID)&user_countryid=\(self.countryId)"
            let imageArray = [_userImage.image]
            self.saveProfileData(value: value, imageArray: imageArray as NSArray)
        }
        
        
        
        
        
        //        if user_imagetype =="image" send image in "user_profilepicture" variable  else if user_imagetype ="bitmoji" then send id in user_imageid field from getavatar api
    }
    
    func setupChooseArticleDropDown(listArray : [countryObjectData] , choosebutton : UITextField ) {
        chooseArticleDropDown.anchorView = choosebutton
        chooseArticleDropDown.topOffset = CGPoint(x: 0, y: -choosebutton.bounds.height)
        // You can also use localizationKeysDataSource instead. Check the docs.
        var finalValue : [String] = []
        for dict in listArray {
            finalValue.append(dict.name!)
        }
        chooseArticleDropDown.dataSource = finalValue
        // Action triggered on selection
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            DispatchQueue.main.async {
                let dict = listArray[index]
                self?.countryId = dict.id!
                self?.profileDict.setValue(item, forKey: (self?.titleArray[choosebutton.tag])!)
                self?.chooseArticleDropDown.hide()
                self?.getState(countryid: (self?.countryId)!)
                self?._tableView.reloadData()
            }
        }
    }
    
    func setupChooseStateDropDown(listArray : [countryObjectData] , choosebutton : UITextField ) {
        chooseArticleDropDown.anchorView = choosebutton
        chooseArticleDropDown.topOffset = CGPoint(x: 0, y: -choosebutton.bounds.height)
        // You can also use localizationKeysDataSource instead. Check the docs.
        var finalValue : [String] = []
        for dict in listArray {
            finalValue.append(dict.name!)
        }
        chooseArticleDropDown.dataSource = finalValue
        // Action triggered on selection
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            DispatchQueue.main.async {
                let dict = listArray[index]
                self?.stateID = dict.id!
                self?.profileDict.setValue(item, forKey: (self?.titleArray[choosebutton.tag])!)
                self?.chooseArticleDropDown.hide()
                self?._tableView.reloadData()
                
            }
        }
    }
    
    func saveProfileData(value : String , imageArray : NSArray){
        APIManager.shared.uploadImage(values: value, imagesArray: imageArray, postKey: ["user_profilepicture"], showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    print(jsonData?.jsonDic!)
                    let profileDict  = jsonData?.jsonDic!["data"] as! NSDictionary
                    GlobalMethod.shared.saveUserDefaultValue(key: profileObjectKey, value: profileDict as Any)
                    GlobalMethod.shared.getUserProfileObject()
                    
                    let dict = (GlobalMethod.shared.getUserdefaultData(key: LoginDataKey) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    dict.setValue(profileObject.user_firstname!, forKey: "firstname")
                    dict.setValue(profileObject.user_lastname!, forKey: "lastname")
                    dict.setValue(profileObject.user_gender!, forKey: "user_gender")
                    GlobalMethod.shared.saveUserDefaultValue(key: LoginDataKey, value: dict as Any)
                    GlobalMethod.shared.setUserData()
                    
                }
            }
            
            
        }
        
    }
    
    
    @IBAction func manClick(sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self._tableView)
        let indexPathGet = self._tableView.indexPathForRow(at:buttonPosition)! as NSIndexPath
        let cell = self._tableView.cellForRow(at: indexPathGet as IndexPath) as! GenderCell
        
        
        if sender.tag == 1{
            cell._maleButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            cell._femaleButton.setImage(#imageLiteral(resourceName: "unSelected"), for: .normal)
            self.genderType = "Male"

            
        }
        else{
            cell._maleButton.setImage(#imageLiteral(resourceName: "unSelected"), for: .normal)
            cell._femaleButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            self.genderType = "Female"

            
        }
        
        
        
    }
    
    
    func headerSetOnTableView(image : UIImage){
      _userImage.image = image
        avatarObj = nil
    }
    
}

//extension EditProfileVC : CameraManagerDelegate{
    //TODO: Add image
//    func didEndChoose(image: UIImage) {
//        self.dismiss(animated: true, completion: nil)
//        _userImage.image = image
//        avatarObj = nil
//    }
//}
extension EditProfileVC : CameraPopUpDelegate{
    func didAvatarClick() {
        self.dismiss(animated: true, completion: nil)
        let viewObject = AvatarVC.init(nibName: "AvatarVC", bundle: nil)
        self.navigationController?.pushViewController(viewObject, animated: true)
    }
    func didDeleteClick() {
        self.dismiss(animated: true, completion: nil)
        _userImage.image = UIImage(named: "placeHolder")
        avatarObj = nil
    }
    func didCameraClick() {
        CameraManager.sharedInstance.PickImageFromCamera()
        CameraManager.sharedInstance.delegate = self
        
    }
    func didGalleryClick() {
        CameraManager.sharedInstance.PickImageFromGalary()
        CameraManager.sharedInstance.delegate = self
        
    }
}

extension EditProfileVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if titleArray[indexPath.row] == "Gender"{
            
            let identifier = "GenderCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? GenderCell
            if cell == nil {
                tableView.register(UINib(nibName: "GenderCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? GenderCell
            }
         
            
            if genderType == "Male"{
                cell!._maleButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
                cell!._femaleButton.setImage(#imageLiteral(resourceName: "unSelected"), for: .normal)
            }
            else{
                cell!._maleButton.setImage(#imageLiteral(resourceName: "unSelected"), for: .normal)
                cell!._femaleButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            }
            cell?._maleButton.tag = 1
            cell?._femaleButton.tag = 2
            cell?._maleButton.addTarget(self, action: #selector(manClick(sender:)), for: .touchUpInside)
            cell?._femaleButton.addTarget(self, action: #selector(manClick(sender:)), for: .touchUpInside)

            return cell!
        }
        else{
            
            let identifier = "TextFieldCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextFieldCell
            if cell == nil {
                tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextFieldCell
            }
            if titleArray[indexPath.row] == "Email Address"{
                cell?._txtField.isEnabled = false
                cell?._txtField.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            }
            else{
                cell?._txtField.isEnabled = true
                cell?._txtField.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
            
            if titleArray[indexPath.row] == "Post Code" || titleArray[indexPath.row] == "Phone" || titleArray[indexPath.row] == "Mobile"{
                cell?._txtField.keyboardType = .phonePad
            }
            else{
                cell?._txtField.keyboardType = .default
            }
            
            if titleArray[indexPath.row] == "Birth Date"{
                cell?._txtField.inputView = datePicker
                indexPathSelect = indexPath
                
            }
            else {
                cell?._txtField.inputView = nil
            }
            
            cell?._txtField.placeholder = titleArray[indexPath.row]
            cell?._txtField.delegate = self
            cell?._txtField.tag = indexPath.row
            if profileDict[titleArray[indexPath.row]] != nil{
                cell?._txtField.text = profileDict[titleArray[indexPath.row]] as? String
            }
            
            return cell!
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}


extension EditProfileVC : UITextFieldDelegate {
    
   
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if titleArray[textField.tag] == "Country" || titleArray[textField.tag] == "State"{
            self.chooseArticleDropDown.hide()
        }
        else{
            profileDict.setValue(textField.text!, forKey: titleArray[textField.tag])
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if titleArray[textField.tag] == "Country" {
            self.countryId = ""
            self.profileDict.removeObject(forKey: titleArray[textField.tag])
            
            let searchStrig = (textField.text! + string)
            if string == "" && searchStrig.count == 1{
                searchCountryObject = countryArray
                self.setupChooseArticleDropDown(listArray: searchCountryObject, choosebutton: textField)
                self.chooseArticleDropDown.show()
                
                return true
            }
            let list = self.countryArray.filter({m in
                ((m.name?.lowercased().contains((searchStrig).lowercased()) ))!
            })
            
            if list.count > 0{
                searchCountryObject = []
                searchCountryObject = list
                self.setupChooseArticleDropDown(listArray: searchCountryObject, choosebutton: textField)
                self.chooseArticleDropDown.show()
                
            }
            return true
            
        }
        else if titleArray[textField.tag] == "State" {
            self.profileDict.removeObject(forKey: titleArray[textField.tag])
            
            if stateArray.count > 0 {
                
                let searchStrig = (textField.text! + string)
                if string == "" && searchStrig.count == 1{
                    searchCountryObject = stateArray
                    self.setupChooseStateDropDown(listArray: searchCountryObject, choosebutton: textField)
                    self.chooseArticleDropDown.show()
                    
                    return true
                }
                let list = self.stateArray.filter({m in
                    ((m.name?.lowercased().contains((searchStrig).lowercased()) ))!
                })
                
                if list.count > 0{
                    searchCountryObject = []
                    searchCountryObject = list
                    self.setupChooseStateDropDown(listArray: searchCountryObject, choosebutton: textField)
                    self.chooseArticleDropDown.show()
                    
                }
            }
            else{
                self.navigationController?.view.makeToast("Please Select Country First", duration: 0.5, position: .center)
            }
            return true
        }
       
        else if titleArray[textField.tag] == "Mobile"{
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else if titleArray[textField.tag] == "Pin Code"{
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
        
    }
}
