//
//  DEMONavigationController.swift
//  Grub n Chill
//
//  Created by orangemac04 on 21/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import REFrostedViewController

class DEMONavigationController: UINavigationController {
 
    let menuViewController : SideMenuVC? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
//        [self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]];
        
        self.view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognized(sender:))))


        // Do any additional setup after loading the view.
    }
    

    @objc func panGestureRecognized(sender : UIPanGestureRecognizer){
        self.view.endEditing(true)
//        self.frostedViewController.view.endEditing(true)
//        self.frostedViewController.presentMenuViewController()

    }
    
    func showMenu(){
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
//        [self.frostedViewController.view endEditing:YES];
//
//        // Present the view controller
//        //
//        [self.frostedViewController presentMenuViewController];

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
