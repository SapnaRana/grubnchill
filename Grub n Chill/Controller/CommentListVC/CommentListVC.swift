//
//  CommentListVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 02/10/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire
import DropDown
import IQKeyboardManagerSwift
import RxSwift

//@objc public protocol CommentDelegate {
//   @objc optional func commentCountUpdate(postObject : homeObjectData)
//}


protocol CommentDelegate {
    // MARK: - Delegate functions
     func commentCountUpdate(value : homeObjectData)
}

class CommentListVC: ParentClass {
    
    public var delegate : CommentDelegate?
    
    var postObject : homeObjectData? = nil
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _topView: UIView!
    @IBOutlet var _tableHeaderView: UIView!
    
    @IBOutlet weak var _descLbl: UILabel!
    @IBOutlet weak var _postUserImage: UIImageView!
    @IBOutlet weak var _postUserName: UILabel!
    @IBOutlet weak var _postTimeLbl: UILabel!
    @IBOutlet weak var _postImageVie: UIImageView!
    @IBOutlet weak var _likeCountLbl: UILabel!
//    @IBOutlet weak var _sharecountLbl: UILabel!
    @IBOutlet weak var _commentCountLbl: UILabel!
    @IBOutlet weak var _messageTxtField: UITextField!
    @IBOutlet weak var _likeBtn: UIButton!
    var check_api = true
    @IBOutlet weak var _commentBtn: UIButton!
    @IBOutlet weak var _shareBtn: UIButton!
    @IBOutlet weak var _viewButtomConstant: NSLayoutConstraint!
    
    @IBOutlet weak var _imageHeightConstant: NSLayoutConstraint!
    
//    self.postObject
//    private let commentCountVariable  = Variable<homeObjectData>([])
//    var updateCommentCountOberver : Observable<String>{
//        return commentCountVariable.asObservable()
//    }
    

    
    var commentObjArray = [commentObjectData]()
    
    @IBOutlet var _tableFooterView: UIView!
    let chooseArticleDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown,
            ]
    }()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
//        IQKeyboardManager.shared.enable = false
//        IQKeyboardManager.shared.enableAutoToolbar = false
        
        
        self.setHeaderView(view: _topView)
        _headerView._slideButton.setImage(UIImage(named: "back-arrow"), for: .normal);
        _headerView._slideButton.tag = dismissButtonTag
        _headerView._icon.isHidden   = true
        _headerView._wheelBtn.isHidden = true
        _headerView._searchBtn.isHidden = true
//        _tableView.tableFooterView = _tableFooterView
        
     
        
        
        _postUserImage.layer.cornerRadius = _postUserImage.bounds.width / 2
        _postUserImage.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _postUserImage.layer.borderWidth = 1

        
//        self.setHeaderViewData()
        self.getCommentData()
        // Do any additional setup after loading the view.
        
   
        
        
        
    }
    
    
    

    @IBAction func _sendClick(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    @objc func clickLikeButton(sender : UIButton){
//        self.likeAndUnlikeMethod(tag: sender.tag)
    }
    
    @objc func clickShareButton(sender : UIButton)
    {
        sharePost = postObject
        self.shareONFacebook()

    }
    @objc func clickLiked(sender : UIGestureRecognizer){
        
    }
    
    @objc func clickCommentCount(sender : UIGestureRecognizer){
        
    }
    
    @objc func clickShareCount(sender : UIGestureRecognizer){
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        _tableView.updateHeaderViewHeight()
    }
    
    func getCommentData(){
        //        type=getreviewscommentlist&page_num=1&page_limit=10&review_id=1&user_id=35
        APIManager.shared.getRequest(values: "type=getreviewscommentlist&page_num=1&page_limit=100&user_id=\(GlobalMethod.userDetail!.user_id!)&review_id=\(postObject!.review_id!)", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                self.commentObjArray.removeAll()
                if error == nil{
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray{
                        let obj = commentObjectData.init(json: dict as! [String : Any])
                        self.commentObjArray.append(obj)
                    }
                    self.commentObjArray.reverse()
                }
                self._tableView.reloadData()

                self._tableView.scrollToLastRow(animated: true)

            }
        }
    }
    
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
    func sendCommentPost(){
        if check_api == false{
            return
        }
//        type=commentonreview&review_id=1&user_id=7&comment=Bad%20Infra
        let messgae = _messageTxtField.text!.encodeEmoji.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
        //stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLPathAllowedCharacterSet())!
            //_messageTxtField.text!.replacingOccurrences(of: " ", with: "%20")
        
        APIManager.shared.getRequest(values: "type=commentonreview&user_id=\(GlobalMethod.userDetail!.user_id!)&review_id=\(postObject!.review_id!)&comment=\(String(describing: messgae))", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    let mutableDict = ((jsonData?.jsonDic!["data"] as! NSArray)[0] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    mutableDict.setValue(GlobalMethod.userDetail!.firstname, forKey: "user_firstname")
                    mutableDict.setValue(GlobalMethod.userDetail!.lastname, forKey: "user_lastname")
                    mutableDict.setValue("0", forKey: "following")
                    mutableDict.setValue("0", forKey: "following_id")
                    mutableDict.setValue(profileObject.user_profilepicture!, forKey: "user_profilepicture")
                    let obj = commentObjectData.init(json: mutableDict as! [String : Any])
                    self._messageTxtField.text = ""
                    // Adding new item to your data source
                    self.commentObjArray.append(obj);
                    // Appending new item to table view
                    self._tableView.beginUpdates()
                    // Creating indexpath for the new item
                    let indexPath = IndexPath(row: self.commentObjArray.count - 1, section: 0)
                    // Inserting new row, automatic will let iOS to use appropriate animation
                    self._tableView.insertRows(at: [indexPath], with: .automatic)
                    self._tableView.endUpdates()
                    self._tableView.scrollToLastRow(animated: true)
                    
//                    self.commentCountVariable.value = "\(self.commentObjArray.count)"
                    self.postObject?.review_commentcount = "\(self.commentObjArray.count)"

                    self.delegate?.commentCountUpdate(value: self.postObject!)
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableViewData"), object: self.postObject) //posts the list items obect
                }
            }
        }
    }
    
    
    
    
    
    
    func updateCommentMethod(tag: Int , comment : String){
        
        var obj = commentObjArray[tag]
        let messgae = comment.replacingOccurrences(of: " ", with: "%20")
        APIManager.shared.getRequest(values: "type=updateComment&comment_id=\(obj.reviewscomment_id!)&comment=\(messgae)&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    let mutableDict = ((jsonData?.jsonDic!["data"] as! NSArray)[0] as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    obj.reviewscomment_comment = mutableDict["reviewscomment_comment"] != nil ? getString(string: mutableDict["reviewscomment_comment"] as Any) : ""
                    self.commentObjArray[tag] = obj
                    self._tableView.beginUpdates()
                    self._tableView.reloadRows(at: [IndexPath(item: tag, section: 0)], with: .automatic)
                    self._tableView.endUpdates()

                }
            }
        }
        
        
    }
    
    
    func deleteCommentMethod(tag: Int){
                let obj = commentObjArray[tag]
        APIManager.shared.getRequest(values: "type=deleteComment&comment_id=\(obj.reviewscomment_id!)&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    self.commentObjArray.remove(at: tag)
                    self._tableView.beginUpdates()
                    self._tableView.deleteRows(at: [IndexPath(row: tag, section: 0)], with: .automatic)
                    self._tableView.endUpdates()
 
                    self.postObject?.review_commentcount = "\(self.commentObjArray.count)"
                    self.delegate?.commentCountUpdate(value: self.postObject!)

                }
            }
        }
    }
    
    
    
    @objc func didDotActionClick(sender : UIButton){
        self.setupChooseArticleDropDown(listArray: ["Edit","Delete"], choosebutton: sender )
        self.chooseArticleDropDown.show()
    }
    
    func setupChooseArticleDropDown(listArray : NSArray , choosebutton : UIButton ) {
        let buttonPosition:CGPoint = choosebutton.convert(CGPoint.zero, to:_tableView)
        let indexPath = _tableView.indexPathForRow(at: buttonPosition)

        chooseArticleDropDown.anchorView = choosebutton
        chooseArticleDropDown.bottomOffset = CGPoint(x: -60, y: choosebutton.frame.size.height)
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseArticleDropDown.dataSource = listArray as! [String]
        // Action triggered on selection
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            self?.chooseArticleDropDown.hide()
            if item == "Edit"{
                
                let alertController = UIAlertController(title: "Grub n Chill", message: "", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
                    let textField = alertController.textFields![0] as UITextField
                    // do something with textField
                    self?.updateCommentMethod(tag: indexPath!.row, comment: textField.text!)
                }))
                alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
                    textField.placeholder = "Enter Comment"
                    let obj = self?.commentObjArray[(indexPath?.row)!]
                    textField.text = obj?.reviewscomment_comment
                })
                self!.present(alertController, animated: true, completion: nil)


                
            }
            else{
                let alertView = UIAlertController(title: "Grub n Chill", message: "Are you sure want to delete?", preferredStyle: .alert)
                let canclAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel , handler: nil)
                let ok = UIAlertAction(title: "Ok", style: .default, handler: {  _ in
                    self?.deleteCommentMethod(tag: indexPath!.row)
                })
                alertView.addAction(canclAction)
                alertView.addAction(ok)
                self?.present(alertView, animated: true, completion: nil)
            }
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func clickUserDetail(sender : UITapGestureRecognizer){
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let Obj = commentObjArray[indexPath.row]
            let viewObject = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
            viewObject.viewType = "backShow"
            viewObject.user_id = Obj.reviewscomment_userid!
            self.navigationController?.pushViewController(viewObject, animated: true)
        }
    }
    
}



extension UITableView {
    func setOffsetToBottom(animated: Bool) {
        self.setContentOffset(CGPoint(x: 0, y: self.contentSize.height - self.frame.size.height), animated: animated)
    }
    
    func scrollToLastRow(animated: Bool) {
        if self.numberOfRows(inSection: 0) > 0 {
            
            self.scrollToRow(at: IndexPath(row: self.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: animated)
        }
    }
    
    func updateHeaderViewHeight() {
        if let header = self.tableHeaderView {
            let newSize = header.systemLayoutSizeFitting(CGSize(width: self.bounds.width, height: 0))
            header.frame.size.height = newSize.height
        }
    }

}

extension CommentListVC : UITableViewDataSource,UITableViewDelegate{
    
    //MARK:- UITableView delegate Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentObjArray.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CommentCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? CommentCell
        if cell == nil {
            tableView.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? CommentCell
        }
        
        let obj = commentObjArray[indexPath.row]
        cell?._UserNameLbl.text = obj.user_firstname! + " " + obj.user_lastname!
        cell?._userImage.sd_setImage(with:  URL(string:(obj.user_profilepicture!)), placeholderImage:UIImage(named: "placeHolder"))
        cell?._descLbl.text = obj.reviewscomment_comment!.decodeEmoji.decodeEmoji
            //obj.reviewscomment_comment
        
        
        cell?._timeLbl.text = obj.reviewscomment_date
        
        if GlobalMethod.userDetail?.user_id == obj.reviewscomment_userid{
            cell?._dotBtn.isHidden = false
        }
        else{
           cell?._dotBtn.isHidden = true
        }
        cell?._dotBtn.tag = indexPath.row
        cell?._dotBtn.addTarget(self, action: #selector(didDotActionClick(sender:)), for: .touchUpInside)

        cell?._userImage.isUserInteractionEnabled = true
        let tapGesture5 = UITapGestureRecognizer(target: self, action: #selector(clickUserDetail(sender:)))
        cell?._userImage.isUserInteractionEnabled = true
        cell?._userImage.addGestureRecognizer(tapGesture5)
        
        cell?._UserNameLbl.isUserInteractionEnabled = true
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(clickUserDetail(sender:)))
        cell?._UserNameLbl.isUserInteractionEnabled = true
        cell?._UserNameLbl.addGestureRecognizer(tapGesture4)

        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}

extension CommentListVC : UITextFieldDelegate {
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        _viewButtomConstant.constant = 0
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if _messageTxtField.text! == "" {
            
        }
        else{
            self.sendCommentPost()
        }
        
    }
}
