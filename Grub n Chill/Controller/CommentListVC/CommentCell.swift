
//
//  CommentCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 07/10/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    @IBOutlet weak var _userImage: UIImageView!
    @IBOutlet weak var _UserNameLbl: UILabel!
    @IBOutlet weak var _timeLbl: UILabel!
    @IBOutlet weak var _descLbl: UILabel!
    @IBOutlet weak var _dotBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _userImage.layer.cornerRadius = _userImage.bounds.width / 2
        _userImage.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _userImage.layer.borderWidth = 1

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
