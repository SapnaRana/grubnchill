//
//  RegistrationVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 15/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire

let email           = "email"
let firstName       = "firstName"
let lastName        = "lastName"
let password        = "password"
let confirmPassword = "confirmPassword"


class RegistrationVC: ParentClass {
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet var _tableHeaderView: UIView!
    @IBOutlet var _tableFooterView: UIView!
    var valueDict = NSMutableDictionary()
    @IBOutlet weak var _fbButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        valueDict.setValue("", forKey: email)
        valueDict.setValue("", forKey: firstName)
        valueDict.setValue("", forKey: lastName)
        valueDict.setValue("", forKey: password)
        valueDict.setValue("", forKey: confirmPassword)
        _fbButton.layer.cornerRadius = 20
        
        _tableView.tableHeaderView = _tableHeaderView
        _tableView.tableFooterView = _tableFooterView
        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func _termClick(_ sender: UIButton) {
        let viewObject = TermsAndConditionVC.init(nibName: "TermsAndConditionVC", bundle: nil)
        if sender.tag == 11{
            viewObject.typeOfView = "terms"
        }
        else{
            viewObject.typeOfView = "privacy"
        }
        self.navigationController?.pushViewController(viewObject, animated: false)
    }
    
    @IBAction func _signInClick(_ sender: Any) {
        let viewObject = LoginVC.init(nibName: "LoginVC", bundle: nil)
        let controller =  UIManager.shared.checkNavigationController(getviewController: LoginVC.self, navigation: self.navigationController!)
        if !controller .isKind(of: LoginVC.self) {
            self.navigationController?.pushViewController(viewObject, animated: false)
        }
        else{
            self.navigationController?.popToViewController(controller, animated: false)
        }
    }
    
    @IBAction func _fbClick(_ sender: Any) {
        self.loginFBClicked()
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc @IBAction func _signUPClick(_ sender: Any) {
        self.view.endEditing(true)
        
        if valueDict[email] == nil || (valueDict[email] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter Email Address")
        }
        else if valueDict[firstName] == nil || (valueDict[firstName] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter First Name")
            
        }
        else if valueDict[lastName] == nil || (valueDict[lastName] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter Last Name")
            
        }
        else if valueDict[password] == nil || (valueDict[password] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter Password")
            
        }
        else if valueDict[confirmPassword] == nil || (valueDict[confirmPassword] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter Confirm Password")
            
        }
        else if (valueDict[email] as! String).isValidEmail == false{
            self.showMessage(msg: "Please Enter a Valid Email Address")
        }
        else if (valueDict[confirmPassword] as! String) != (valueDict[password] as! String){
            self.showMessage(msg: "Pasword and Confirm Password not match")
            
        }
        else{
            let value = String(format: "type=signup&user_email=%@&user_pass=%@&user_firstname=%@&user_lastname=%@&user_device=i&user_token=%@", valueDict[email] as! String,valueDict[password] as! String,valueDict[firstName] as! String,valueDict[lastName] as! String,fcm_reg_id)
            
            self.loginMethod(value: value,typeOffLogin: "Normal")
        }
        
        
    }
    
    
    
    
}
extension RegistrationVC : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "SignUpCustomCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SignUpCustomCell
        if cell == nil {
            tableView.register(UINib(nibName: "SignUpCustomCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? SignUpCustomCell
        }
        
        cell?._signUpButton.addTarget(self, action: #selector(_signUPClick(_:)), for: .touchUpInside)
        
        cell?._emailtxtField.tag         = 51
        cell?._firstNameTxtField.tag     = 52
        cell?._lastNameTxtField.tag      = 53
        cell?._passwordTxtField.tag      = 54
        cell?._confirmTxtField.tag       = 55
        
        cell?._emailtxtField.delegate       = self
        cell?._firstNameTxtField.delegate   = self
        cell?._lastNameTxtField.delegate    = self
        cell?._passwordTxtField.delegate    = self
        cell?._confirmTxtField.delegate     = self
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension RegistrationVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 51 {
            valueDict.setValue(textField.text!, forKey: "email")
        }
        else if textField.tag == 52 {
            valueDict.setValue(textField.text!, forKey: "firstName")
        }
        else if textField.tag == 53 {
            valueDict.setValue(textField.text!, forKey: "lastName")
        }
        else if textField.tag == 54 {
            valueDict.setValue(textField.text!, forKey: "password")
        }
        else{
            valueDict.setValue(textField.text!, forKey: "confirmPassword")
        }
        
    }
}
