//
//  ResDetailCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 11/02/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit

class ResDetailCell: UITableViewCell {
    @IBOutlet weak var _titleLbl: UILabel!
    @IBOutlet weak var _iconImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
