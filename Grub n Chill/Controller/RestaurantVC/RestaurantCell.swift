//
//  RestaurantCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 24/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class RestaurantCell: UITableViewCell {
    @IBOutlet weak var _imageView: UIImageView!
    @IBOutlet weak var _nameLbl: UILabel!
    @IBOutlet weak var _subTitleLbl: UILabel!
    @IBOutlet weak var _ratingView: UIView!
    @IBOutlet weak var _ratingLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _imageView.layer.cornerRadius = 5;
        _ratingView.layer.cornerRadius = 5;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
