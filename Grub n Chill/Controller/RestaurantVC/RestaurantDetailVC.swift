//
//  RestaurantDetailVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 11/02/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage

class RestaurantDetailVC: UIViewController {
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _centerView: UIView!
    @IBOutlet weak var _imageView: UIImageView!
    @IBOutlet weak var _nameLbl: UILabel!
    @IBOutlet weak var _subDetailLbl: UILabel!
    @IBOutlet var _tableFooterView: UIView!
    @IBOutlet weak var _mapView: MKMapView!
    @IBOutlet weak var _lineLbl: UILabel!
    @IBOutlet weak var _ratingView: UIView!
    @IBOutlet weak var _ratingLbl: UILabel!
    @IBOutlet weak var _topScrollView: UIView!
    @IBOutlet weak var _collectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!

    
    var bannerArray = [String]()
    
    let dataArray = NSMutableArray()
    
    var restaurantId = ""
    var mutableDetailDic = NSMutableDictionary()
    
    @IBOutlet var _tableHeaderView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        _ratingView.layer.cornerRadius = 5;

        _tableView.tableHeaderView = _tableHeaderView
        _tableView.tableFooterView = _tableFooterView

        _centerView.layer.cornerRadius = 20;
        _imageView.layer.cornerRadius = 5;
        _mapView.layer.cornerRadius = 10;
        _lineLbl.layer.cornerRadius = 4;
        _tableView.isHidden = true
        _topScrollView.isHidden = true

        _collectionView.register(UINib(nibName: "ImageCell", bundle:nil), forCellWithReuseIdentifier: "ImageCell")

        self.getRestaurantDetail()
        // Do any additional setup after loading the view.
    }

    
    func getRestaurantDetail(){
        APIManager.shared.getRequest(values: "type=getRestaurant&id=\(restaurantId)", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    print(jsonData?.jsonDic as Any)
                    self.mutableDetailDic = NSMutableDictionary()
                    let localDict = jsonData?.jsonDic!["data"] as! NSDictionary
                    self.mutableDetailDic.setDictionary(localDict as! [AnyHashable : Any])
                    self._nameLbl.text = self.mutableDetailDic["restaurant_name"] as? String
//                    self._subDetailLbl.text = self.mutableDetailDic["restaurant_foodtype"] as? String
                    self._ratingLbl.text = self.mutableDetailDic["restaurant_rating"] as? String
                    
                    
                 let rating =  self.mutableDetailDic["restaurant_rating"] != nil ? getString(string: self.mutableDetailDic["restaurant_rating"] as Any) : ""
                    
                    let ratingValue = Double(rating)
                    if rating != ""{
                        self._ratingLbl.text =   String(format: "%.1f", ratingValue!)
                    }
                    else{
                        self._ratingLbl.text =   "0.0"
                    }
                    
                    let newPin = MKPointAnnotation()
                    newPin.coordinate.latitude = Double(self.mutableDetailDic["restaurant_lat"] as! String)!
                    newPin.coordinate.longitude = Double(self.mutableDetailDic["restaurant_lon"] as! String)!
                    newPin.title = self.mutableDetailDic["restaurant_name"] as? String
                    self._mapView.addAnnotation(newPin)
                    self._mapView.showAnnotations(self._mapView.annotations, animated: true)
                    
                    let imageStr = self.mutableDetailDic["restaurant_images"] as? String
                    let imageArray = imageStr?.components(separatedBy: ",")
                    if (imageArray?.count)! > 0 {
                        self.bannerArray = [String]()
                        self.bannerArray = imageArray!
                        self._imageView.sd_setImage(with:  URL(string:((imageArray![0]))), placeholderImage:UIImage(named: "placeHolder"))
                        
                        self.pageController.numberOfPages = self.bannerArray.count

                    }
                    
                    self.dataArray.removeAllObjects()
                    if let name = self.mutableDetailDic["restaurant_website"] as? String {
                        let mutableDict  = NSMutableDictionary()
                        mutableDict.setValue(name, forKey: "name")
                        mutableDict.setValue("website", forKey: "imageName")
                        self.dataArray.add(mutableDict)
                    }
                    if let name = self.mutableDetailDic["restaurant_foodtype"] as? String {
                        let mutableDict  = NSMutableDictionary()
                        mutableDict.setValue(name, forKey: "name")
                        mutableDict.setValue("food", forKey: "imageName")
                        self.dataArray.add(mutableDict)
                    }
                    if let name = self.mutableDetailDic["restaurant_address"] as? String {
                        let mutableDict  = NSMutableDictionary()
                        mutableDict.setValue(name, forKey: "name")
                        mutableDict.setValue("address", forKey: "imageName")
                        self.dataArray.add(mutableDict)
                    }
                    if let name = self.mutableDetailDic["restaurant_phonenumber"] as? String {
                        let mutableDict  = NSMutableDictionary()
                        mutableDict.setValue(name, forKey: "name")
                        mutableDict.setValue("phone", forKey: "imageName")
                        self.dataArray.add(mutableDict)
                    }
                    if let name = self.mutableDetailDic["restaurant_internationalnumber"] as? String {
                        let mutableDict  = NSMutableDictionary()
                        mutableDict.setValue(name, forKey: "name")
                        mutableDict.setValue("international-phone", forKey: "imageName")
                        self.dataArray.add(mutableDict)
                    }
                    
                    print(self.dataArray)

                    self._collectionView.reloadData()
                    self._topScrollView.isHidden = false
                    self._tableView.isHidden = false
                    self._tableView.reloadData()

                }
            }
        }
    }
    @IBAction func _backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
   

}

extension RestaurantDetailVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let ip = self._collectionView.indexPathForItem(at: center) {
            self.pageController.currentPage = ip.row
        }
    }
}
extension RestaurantDetailVC : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    //MARK :- UICollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerArray.count
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1);  // top, left, bottom, right
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width , height: 228)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        
        cell._imageView.sd_setImage(with:  URL(string:((bannerArray[indexPath.row]))), placeholderImage:UIImage(named: "placeHolder"))
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
extension RestaurantDetailVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "ResDetailCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ResDetailCell
        if cell == nil {
            tableView.register(UINib(nibName: "ResDetailCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? ResDetailCell
        }
        let dict = dataArray[indexPath.row] as? NSDictionary
        cell?._iconImage.image = UIImage(named: (dict!["imageName"] as? String)!)
        cell?._titleLbl.text = dict!["name"] as? String
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = dataArray[indexPath.row] as? NSDictionary
        var value = (dict!["name"] as? String)!
        if dict!["imageName"] as! String  == "website"{
            let webView = BlogDetailVC(nibName: "BlogDetailVC", bundle: nil)
            webView.post_url = value
            webView.controllertype = "Restaurant"
            self.present(webView, animated: true, completion: nil)
        }
        else if dict!["imageName"] as! String  == "international-phone" ||  dict!["imageName"] as! String  == "phone" {
            value = value.replacingOccurrences(of: " ", with: "")
            guard let number = URL(string: "tel://" + value) else { return }
            UIApplication.shared.open(number)

        }
        else if  dict!["imageName"] as! String == "address" {
            value = value.replacingOccurrences(of: "#", with: "")

            value = value.replacingOccurrences(of: " ", with: "%20")
            let url = "https://maps.google.com/?q=\(value)"
            guard let addressPath = URL(string:url) else { return }
            UIApplication.shared.open(addressPath)
        }
        
        
        
//
    }
    
    
    
}
