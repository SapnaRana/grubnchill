//
//  ImageCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/02/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var _imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
