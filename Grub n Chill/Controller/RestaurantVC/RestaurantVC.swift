//
//  RestaurantVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 24/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift
import RxCocoa
class RestaurantVC: ParentClass {
    @IBOutlet weak var _backBtn: UIButton!
    @IBOutlet weak var _tableView: UITableView!
//    var restaurantArray = NSMutableArray()
//    var restaurantArray = [restaurantListObject]()
    var refreshControl = UIRefreshControl()
    
    
    @IBOutlet weak var _uiTextField : UITextField!

    //Input
    fileprivate let resArray = Variable<[restaurantListObject]>([])
    
    //Output
    fileprivate let searchArray = Variable<[restaurantListObject]>([])
    
    //MARK : -  Properties
    fileprivate let bag  = DisposeBag()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateNearByRestaurant()
        
        //Bind UI
        self.bindUI()

        
        _backBtn.setImage(UIImage(named: "back-arrow"), for: .normal);
        let lat = UserDefaults.standard.object(forKey: "lat")
        let long = UserDefaults.standard.object(forKey: "long")
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "Loading Data.....")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        _tableView.addSubview(refreshControl) // not required when using UITableViewController

        
        if lat != nil && long != nil {
            self.getRestaurant(lat: lat as! String, long: long as! String , isShowloadingIndicator: true)
        }
        // Do any additional setup after loading the view.
    }
    @objc func refresh(){
        self.updateNearByRestaurant()

        let lat = UserDefaults.standard.object(forKey: "lat")
        let long = UserDefaults.standard.object(forKey: "long")
        if lat != nil && long != nil {
            self.getRestaurant(lat: lat as! String, long: long as! String ,isShowloadingIndicator: false)
        }
    }
    
    @IBAction func _backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
//    https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=13.017156,77.6537774&radius=5000&type=restaurant&key=AIzaSyDDjQ-JbIKr4lpvSBUpHLH-MJAir9T9A2Q
    
    func getRestaurant(lat : String , long : String , isShowloadingIndicator : Bool){
        
        APIManager.shared.getRequest(values: "type=getNearestRestaurantsList&lat=\(lat)&lon=\(long)&s=", showIndicator: isShowloadingIndicator, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                var restaurantArray = [restaurantListObject]()
                if error == nil {
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray {
                        let obj = restaurantListObject.init(json: dict as! [String : Any])
                        restaurantArray.append(obj)
                    }
                    
                    
                    self.resArray.value = restaurantArray
                    
                }
                
                self.refreshControl.endRefreshing()
                self._tableView.reloadData()

                
            }
        }
        
//
//        APIManager.shared.getGoogleDataRequest(values: "location=13.017156,77.6537774&radius=5000&type=restaurant&key=AIzaSyDDjQ-JbIKr4lpvSBUpHLH-MJAir9T9A2Q", showIndicator: true, viewController: self) { (error, jsonData) in
//            DispatchQueue.main.async {
////                self.restaurantArray.removeAllObjects()
//                if error == nil{
//                    let localArray = jsonData?.jsonDic!["results"] as! NSArray
//                    self.restaurantArray = localArray.mutableCopy() as! NSMutableArray
//                }
//                self._tableView.reloadData()
//
//            }
//        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension RestaurantVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArray.value.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "RestaurantCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RestaurantCell
        if cell == nil {
            tableView.register(UINib(nibName: "RestaurantCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RestaurantCell
        }
        let dict = searchArray.value[indexPath.row]
        
        cell?._nameLbl.text = dict.restaurant_name
        cell?._subTitleLbl.text = dict.restaurant_foodtype
        cell?._imageView.sd_setImage(with:  URL(string:(dict.restaurant_icon!)), placeholderImage:UIImage(named: "placeHolder"))
        
        let ratingValue = Double(dict.restaurant_rating!)
        if dict.restaurant_rating != nil  &&  dict.restaurant_rating != ""{
            cell?._ratingLbl.text =   String(format: "%.1f", ratingValue!)
        }
        else{
            cell?._ratingLbl.text =   "0.0"
        }


        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = searchArray.value[indexPath.row]
        selectedRestaurantObj.onNext(dict)
//        selectedRestaurantObj = dict
        self.navigationController?.popViewController(animated: true);
    }
    
    
    
}


extension RestaurantVC {
    
    func bindUI(){
        
        Observable.combineLatest(
            resArray.asObservable(),
            _uiTextField.rx.text) { currentResturent, searchText in
                return currentResturent.filter{ obj in
                    return self.shouldDisplayValue(obj: obj, search: searchText)
                }
        }
        .bind(to:searchArray)
        .disposed(by: bag)
        
        
        
        searchArray.asObservable()
            .subscribe(onNext: { [weak self]value in
                self?._tableView.reloadData()
            })
        .disposed(by: bag)
        
    }
    
    
    func shouldDisplayValue(obj : restaurantListObject ,search :String?) -> Bool{
        
        if let searchValue = search , !searchValue.isEmpty {
            return obj.restaurant_name!.lowercased().contains(searchValue.lowercased())
        }
        return true


//        if let search = search,
//            !search.isEmpty,
//            !obj.restaurant_name!.contains(search)
//            {
//            return false
//        }
//        return true
    }
}
