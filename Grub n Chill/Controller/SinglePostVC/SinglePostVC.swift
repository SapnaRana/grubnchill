//
//  SinglePostVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 21/04/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit
import Windless
protocol LikeCommetDelegate {
    // MARK: - Delegate functions
    func likeCountUpdate(value : homeObjectData)
    func deleteReviewPost(value : homeObjectData)

}


class SinglePostVC: ParentClass {
    public var delegate : LikeCommetDelegate?

    @IBOutlet weak var _tableView: UITableView!
    var reviewId : String?
    var reviewObject : homeObjectData?
    var animationStop = false
    @IBOutlet weak var _deleteBtn: UIButton!
    @IBOutlet weak var _editBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
//    @objc func stopAnimation(){
//        _tableView.windless.end()
//        animationStop = true
//        _tableView.reloadData()
//    }
//    @objc func startAnimation(){
//        if animationStop == false {
//            _tableView.windless
//                .apply {
//                    $0.beginTime = 0.1
//                    $0.duration = 2
//                    $0.animationLayerOpacity = 0.5
//                }
//                .start()
//        }
//    }
    override func viewDidLayoutSubviews() {
        self.startAnimation(view: _tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.async {
            
            
            if self.reviewId != nil {
                self.getSinglePostMethod()
            }
            else{
                self.stopAnimation(view: self._tableView)
                self.animationStop = true

            }
        }

    }
    
    @IBAction func _backBtn(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableViewData"), object: reviewObject) //posts the list items obect

        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func editBtnClick(_ sender: Any) {
        let viewObject = ReviewPostVC.init(nibName: "ReviewPostVC", bundle: nil)
        viewObject.postType = "Edit"
        viewObject.postObject = reviewObject
        self.navigationController?.pushViewController(viewObject, animated: true)
        
    }
    
    @IBAction func deleteBtnClick(_ sender: Any) {
        let alertView = UIAlertController(title: "Grub n Chill", message: "Are you sure want to delete?", preferredStyle: .alert)
        let canclAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel , handler: nil)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: {  _ in
            self.deletePostMethod(tag: 0)
        })
        alertView.addAction(canclAction)
        alertView.addAction(ok)
        self.present(alertView, animated: true, completion: nil)

    }
    
    //MARK:-  GET Single ReviewPost Data Method
    func getSinglePostMethod(){
        APIManager.shared.getRequest(values: "&type=getreviewdetail&review_id=\(reviewId!)&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    self.stopAnimation(view: self._tableView)
                    self.animationStop = true
                    let dict = jsonData?.jsonDic!["data"] as! NSDictionary
                    print(dict)
                    self.reviewObject = homeObjectData.init(json: dict as! [String : Any])
                    if GlobalMethod.userDetail?.user_id == self.reviewObject?.user_id{
                        self._deleteBtn.isHidden = false
                        self._editBtn.isHidden = false
                    }
                    else{
                        self._deleteBtn.isHidden = true
                        self._editBtn.isHidden = true
                    }
                    self._tableView.reloadData()
                }
            }
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SinglePostVC : UITableViewDelegate , UITableViewDataSource {
    
    //MARK:- UITableView delegate Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "PostCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PostCell
        if cell == nil {
            tableView.register(UINib(nibName: "PostCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PostCell
        }
        if reviewObject != nil {
            cell?.setCellData(obj: reviewObject!, indexPath: indexPath, contoller: self)
        }
        
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        let lastElement = self.homeDataArray.count - 1
    //        if !isLoadingList && indexPath.row == lastElement {
    //        }
    //    }
    
    
}

extension SinglePostVC {
    
    @objc func clickResturantName(sender : UITapGestureRecognizer){
        
        let reviewPostVC = RestaurantDetailVC.init(nibName: "RestaurantDetailVC", bundle: nil)
        reviewPostVC.restaurantId = (reviewObject?.restaurant_id!)!
        self.navigationController?.pushViewController(reviewPostVC, animated: true)
    }

    
    @objc func clickImageView(sender : UITapGestureRecognizer){
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let cell = _tableView.cellForRow(at: indexPath) as? PostCell
            self.openImageViewController(sender: cell!._imageView)
        }
    }
    @objc func clickLiked(sender : UITapGestureRecognizer){
        let viewObjet = LikedUserListVC.init(nibName: "LikedUserListVC", bundle: nil)
        viewObjet.postObject = reviewObject
        let nvc = UINavigationController(rootViewController: viewObjet)
        nvc.isNavigationBarHidden = true
        self.present(nvc, animated: true, completion: nil)
    }
    
    @objc func clickCommentCount(sender : UITapGestureRecognizer){
        self.sendCommentListView(obj: reviewObject!)
    }
    
    @objc func clickUserDetail(sender : UITapGestureRecognizer){
        let viewObject = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
        viewObject.viewType = "backShow"
        viewObject.homeObjData = reviewObject
        self.navigationController?.pushViewController(viewObject, animated: true)
    }
}

extension SinglePostVC {
    
    @objc func clickCommentButton(sender : UIButton){
        self.sendCommentListView(obj: reviewObject!)
    }
    
    
    
    @objc func clickLikeButton(sender : UIButton){
        if sender.titleLabel?.text == "Like"{
            sender.setTitle("Unlike", for: .normal)
            sender.setImage(UIImage.init(named: "fullLike"), for: .normal)
            sender.setTitleColor(#colorLiteral(red: 0.9568627451, green: 0.2196078431, blue: 0.1803921569, alpha: 1), for: .normal)
        }
        else{
            sender.setTitle("Like", for: .normal)
            sender.setImage(UIImage.init(named: "like"), for: .normal)
            sender.setTitleColor(#colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1), for: .normal)
        }
        self.likeAndUnlikeMethod(tag: sender.tag)
    }
    
    @objc func clickShareButton(sender : UIButton)
    {
        self.sharePostMethod(tag:sender.tag)
        
    }
}

extension SinglePostVC {
    
    func likeAndUnlikeMethod(tag : Int){
        var obj = reviewObject!
        let status = obj.review_userliked == "1" ? "2" : "1"
        APIManager.shared.getRequest(values: "type=getreviewsstatus&review_id=\(obj.review_id!)&status=\(status)&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    var count = Int(obj.review_likedcount!)
                    count = status == "1" ? count! + 1 : count! - 1
                    obj.review_likedcount = "\(String(describing: count!))"
                    obj.review_userliked = status == "1" ? "1" : "0"
                    self.reviewObject = obj
                    self.delegate?.likeCountUpdate(value: self.reviewObject!)
                    self._tableView.reloadData()
                }
            }
        }
    }
    
    
    //MARK: - Delete Post Method
    func deletePostMethod(tag : Int){
        let obj = reviewObject!
        APIManager.shared.getRequest(values: "type=deleteReviews&user_id=\(GlobalMethod.userDetail!.user_id!)&review_id=\(obj.review_id!)", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.deleteReviewPost(value: self.reviewObject!)
                }
            }
        }
    }
    
    func sharePostMethod(tag : Int){
        sharePost = reviewObject!
        self.shareONFacebook()
        
    }
    
}

extension SinglePostVC : CommentDelegate{
    func commentCountUpdate(value: homeObjectData) {
        reviewObject = value
        _tableView.reloadData()
    }
}
