//
//  AvatarVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 24/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class AvatarVC: UIViewController {
    @IBOutlet weak var _collectionView: UICollectionView!
    var maleAvatarObj = [avatarObjectData]()
    var femaleAvatarObj = [avatarObjectData]()
    var mainArray = [avatarObjectData]()
    
    var indexP  = [IndexPath]()

    override func viewDidLoad() {
        super.viewDidLoad()
        _collectionView.register(UINib(nibName: "AvatarCell", bundle: nil), forCellWithReuseIdentifier: "AvatarCell")
        self.GetAvatarMethod()
        // Do any additional setup after loading the view.
    }

    func GetAvatarMethod(){
        APIManager.shared.getRequest(values: getAvatarUrl, showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
               self.maleAvatarObj.removeAll()
                if error == nil{
                    let dictObject = jsonData!.jsonDic!["data"] as! NSDictionary
                    let maleArray = dictObject["male"] as! NSArray
                    let femaleArray = dictObject["female"] as! NSArray

                    for dict in femaleArray{
                        let obj = avatarObjectData.init(json: dict as! [String : Any])
                        self.femaleAvatarObj.append(obj)
                    }

                    for dict in maleArray{
                        let obj = avatarObjectData.init(json: dict as! [String : Any])
                        self.maleAvatarObj.append(obj)
                    }
                    self.mainArray = self.maleAvatarObj
                    self._collectionView.reloadData()
                }
            }
        }
    }
    @IBAction func _setClick(_ sender: Any) {
        if indexP.count > 0 {
        avatarObj = mainArray[(indexP[0] as IndexPath).row]
        }
        self.navigationController?.popViewController(animated: true);

    }
    
    @IBAction func _backClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true);
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func _clickSegment(_ sender: UISegmentedControl) {
        indexP.removeAll()
        avatarObj = nil
        if sender.selectedSegmentIndex == 0 {
            mainArray = maleAvatarObj
        }
        else{
            mainArray = femaleAvatarObj
        }
        _collectionView.reloadData()
    }
}


extension AvatarVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainArray.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        return CGSize(width:99, height: 99)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCell", for: indexPath) as! AvatarCell
        let obj = mainArray[indexPath.row]
        cell._imageView.sd_setImage(with:  URL(string:(obj.path!)), placeholderImage:UIImage(named: "placeHolder"))
        if indexP.contains(indexPath){
            cell._checkView.isHidden = false
        }
        else{
            cell._checkView.isHidden = true

        }

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexP.removeAll()
        indexP.append(indexPath)
        _collectionView.reloadData()
    }
        
}
