//
//  ChangePasswordVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 09/05/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit
import MaterialTextField

class ChangePasswordVC: ParentClass {

    @IBOutlet weak var _confirmTxtField: MFTextField!
    @IBOutlet weak var _emailIdTextField: MFTextField!
    @IBOutlet weak var _otpTextField: MFTextField!
    @IBOutlet weak var _newPasswordtxtField: MFTextField!
    var emailID = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        _emailIdTextField.text = emailID

        // Do any additional setup after loading the view.
    }

    @IBAction func _backBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func _submitClick(_ sender: Any) {
        self.view.endEditing(true)
        
        
        if _otpTextField.text! == "" || _otpTextField.text!.isValidText() == false {
            self.showMessage(msg: "Please Enter OTP")
        }
        else if OTPValue != _otpTextField.text!{
            self.showMessage(msg: "Please Enter Correct OTP")
        }
        else if _newPasswordtxtField.text! == "" || _newPasswordtxtField.text!.isValidText() == false {
            self.showMessage(msg: "Please Enter New Password")
        }
        else if _confirmTxtField.text! == "" || _confirmTxtField.text!.isValidText() == false {
            self.showMessage(msg: "Please Enter Confirm Password")
        }
        else if _newPasswordtxtField.text! != _confirmTxtField.text!{
            self.showMessage(msg: "Pasword and Confirm Password not match")
        }
        else{
            let value = String(format: "type=updatepassword&user_email=%@&new_password=%@&confirm_password=%@&user_otp=%@",emailID,_newPasswordtxtField.text!,_confirmTxtField.text!,_otpTextField.text!)
            self.changePasswordMethod(value: value)
            
            
            
            
        }
        
        
    }
    
    func changePasswordMethod(value : String ){
        APIManager.shared.getRequest(values: value, showIndicator: true, viewController: self) { (error, JsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Grub n Chill", message: (JsonData?.jsonDic!["message"] as! String), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                            self.perform(#selector(self.sendToNextC), with: nil, afterDelay: 0.2)
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else{
//                    self.view.makeToast((JsonData?.jsonDic!["message"] as! String), duration: toastTimer, position: .center)
                }
            }
        }
        
    }
    
    
    @objc func sendToNextC(){
        let viewObject = LoginVC.init(nibName: "LoginVC", bundle: nil)
        let controller =  UIManager.shared.checkNavigationController(getviewController: LoginVC.self, navigation: self.navigationController!)
        if !controller .isKind(of: LoginVC.self) {
            self.navigationController?.pushViewController(viewObject, animated: false)
        }
        else{
            self.navigationController?.popToViewController(controller, animated: false)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
