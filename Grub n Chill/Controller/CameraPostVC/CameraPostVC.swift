//
//  CameraPostVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class CameraPostVC: ParentClass {
    @IBOutlet weak var _topView: UIView!
    @IBOutlet weak var _buttomView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeaderView(view: _topView)
        self.setFooterView(view: _buttomView)

        _headerView._icon.isHidden   = false
        _headerView._wheelBtn.isHidden = true
        _headerView._saveChangeBtn.isHidden = true
        _headerView._searchBtn.isHidden = true

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
