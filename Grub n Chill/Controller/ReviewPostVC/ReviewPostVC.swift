//
//  ReviewPostVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire
import Malert
import HCSStarRatingView
import RxSwift
import RxCocoa
//import FloatRatingView

class ReviewPostVC: ParentClass {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var _tableView: UITableView!
    var titleArray = ["Restaurent Name","Dish Name","Category","Write here with Chill..." ,"Rating"]
    var selectedDict = NSMutableDictionary()
    @IBOutlet weak var _postImageView: UIImageView!
    @IBOutlet var _tableHeaderView: UIView!
    var isImageSelectForPost : Bool!
    @IBOutlet weak var _backBtn: UIButton!
    
    var postObject : homeObjectData! = nil
    var postType  : String!
    var controllerType  : String?
    var presentType  : String!
    var image : UIImage!
    
    
    //MARK : -  Properties
    fileprivate let bag  = DisposeBag()

    @IBOutlet weak var _editBtn: UIButton!
    @IBOutlet weak var _addReviewBtn: UIButton!
    
    var ratingValue = "0"
    @IBOutlet weak var _headerLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        isImageSelectForPost = false
        _postImageView.layer.cornerRadius = 5
        _backBtn.setImage(UIImage(named: "back-arrow"), for: .normal);
        self.camerAPopUpSetUp()
        
        if postType == "Edit"{
//            _addReviewBtn.setTitle("Edit Review", for: .normal)
            isImageSelectForPost = true
            _headerLbl.text = "Edit Post"
            selectedDict.setValue(postObject!.restaurant_name, forKey: "Restaurent Name")
            selectedDict.setValue(postObject!.restaurant_foodtype, forKey: "Dish Name")
            selectedDict.setValue(postObject!.restaurant_foodcategory, forKey: "Category")
            selectedDict.setValue(postObject!.review_text, forKey: "Write here with Chill...")
            ratingValue = postObject.review_rating!
            selectedDict.setValue(postObject!.restaurant_id, forKey: "Restaurent Id")
            selectedDict.setValue(postObject!.restaurant_foodcategoryid, forKey: "CategoryId")

//            if postObject.review_contenttype! == "1" {
                _tableView.tableHeaderView = _tableHeaderView
                _postImageView.sd_setImage(with:  URL(string:(postObject.review_image!)), placeholderImage:UIImage(named: "placeHolder"))
//            }
            
        }
        else{
//            _addReviewBtn.setTitle("Add Review", for: .normal)
            _headerLbl.text = "Create Post"
        }

        if presentType == "CameraOpen"{
//            CameraManager.sharedInstance.PickImageFromGalary()
//            CameraManager.sharedInstance.delegate = self
            isImageSelectForPost = true
            _postImageView.image = image
            _tableView.tableHeaderView = _tableHeaderView
        }
        
        // Do any additional setup after loading the view.
    }
    @IBAction func _editImageClick(_ sender: Any) {
        present(examples[0].malert, animated: true)

    }
    
    private func camerAPopUpSetUp() {
        let example4View = CustomCameraPopUp.instantiateCustomFromNib()
        example4View.delegate = self
        let alert = Malert(customView: example4View)
        alert.animationType = .fadeIn
        alert.backgroundColor = UIColor(red:0.47, green:0.53, blue:0.80, alpha:1.0)
        alert.buttonsAxis = .horizontal
        alert.buttonsHeight = 60
        alert.separetorColor = .clear
        examples.append(Example(title: "Example 2", malert: alert))
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        let subscribe = selectedRestaurantObj.subscribe(onNext: {value in
            self.selectedDict.setValue(value.restaurant_name , forKey: "Restaurent Name")
            self.selectedDict.setValue(value.restaurant_id, forKey: "Restaurent Id")
            self._tableView.reloadData()
        })
        .disposed(by: bag)

        
        if selectedFoodCategoryObj.count > 0 {
            let categoryName = selectedFoodCategoryObj.filter({m in
                            m.selected == "1" }
                            ).map({$0.foodcategory_name!}).joined(separator: ",")
            let categoryId = selectedFoodCategoryObj.filter({m in
                m.selected == "1" }
                ).map({$0.foodcategory_id!}).joined(separator: ",")
            selectedDict.setValue(categoryName, forKey: "Category")
            selectedDict.setValue(categoryId, forKey: "CategoryId")
            _tableView.reloadData()
        }
//        if selectedFoodTypeObj.count > 0 {
//            let foodTypeName = selectedFoodTypeObj.filter({m in
//                m.selected == "1" }
//                ).map({$0.foodtype_name!}).joined(separator: ",")
////            let foodTypeId = selectedFoodTypeObj.filter({m in
////                m.selected == "1" }
////                ).map({$0.foodtype_id!}).joined(separator: ",")
//            selectedDict.setValue(foodTypeName, forKey: "Dish Name")
////            selectedDict.setValue(foodTypeId, forKey: "DishId")
//            _tableView.reloadData()
//        }
    }
    
    @IBAction func _addReviewClick(_ sender: Any) {
        
        if selectedDict["Restaurent Name"] == nil || (selectedDict["Restaurent Name"] as! String).isValidText() == false {
            self.showMessage(msg: "Please Select Restaurent Name")
        }
        else if selectedDict["Dish Name"] == nil || (selectedDict["Dish Name"] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter Dish Name")
        }
        else if selectedDict["Category"] == nil || (selectedDict["Category"] as! String).isValidText() == false {
            self.showMessage(msg: "Please Select Category")
        }
        else if selectedDict["Write here with Chill..."] == nil || (selectedDict["Write here with Chill..."] as! String).isValidText() == false {
            self.showMessage(msg: "Please Enter your review")
        }
        else if isImageSelectForPost == false{
            self.showMessage(msg: "Please add image")
        }
        else{
            self.postReviewMethod()
        }

    }
    
    @IBAction func _backBtnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func _imageClick(_ sender: Any) {
        present(examples[0].malert, animated: true)

    }
    @IBAction func _postClick(_ sender: Any) {
        
    }
    
    
    
    //MARK: - Post Review Method
    func postReviewMethod(){
        var imageArray = NSArray()

        if isImageSelectForPost == true{
            imageArray = [_postImageView.image as Any]
        }
       

        var apiType = ""
        if postType == "Edit"{
            apiType = "updateReview&review_id=\(postObject.review_id!)"
        }
        else{
            apiType = "addReview"
        }
        
        
//        reviews_restaurantid , reviews_foodtypeid (comma seperator) ,reviews_foodcategoryid (comma seperator)
        
//        let value = String(format: "restaurant_name=\(selectedDict["Restaurent Name"] as! String)&restaurant_foodtype=\(selectedDict["Dish Name"] as! String)&restaurant_foodcategory=\(selectedDict["Category"] as! String)&restaurant_address=India&restaurant_rating=\(ratingValue)&reviews_text=\(selectedDict["Write here with Chill..."] as! String)&type=\(apiType)&api=grubnchill&user_id=\(GlobalMethod.userDetail!.user_id! as Any)&reviews_restaurantid=\(selectedDict["Restaurent Id"] as! String)&reviews_foodcategoryid=\(selectedDict["CategoryId"] as! String)&reviews_foodtypeid=\(selectedDict["DishId"] as! String)")
        
        
        
        
/*http://www.letsgrubnchill.com/api/Api.php?
         reviews_restaurantid=10
         reviews_foodtypeid=2
         reviews_foodcategoryid=3
         restaurant_rating=3
         reviews_text=Nice%20Food
         type=updateReview
         api=grubnchill
         user_id=5
         review_id=5
 */
//
//
//
//
//        http://www.letsgrubnchill.com/api/Api.php?reviews_restaurantid=10&reviews_foodtypeid=2&reviews_foodcategoryid=3&&restaurant_rating=3&reviews_text=Nice Food&type=addReview&api=grubnchill&user_id=5
        
        print(selectedDict)
//        let value = String(format: "restaurant_name=\(selectedDict["Restaurent Name"] as! String)&restaurant_foodtype=\(selectedDict["Dish Name"] as! String)&restaurant_foodcategory=\(selectedDict["Category"] as! String)&restaurant_address=India&restaurant_rating=\(ratingValue)&reviews_text=\(selectedDict["Write here with Chill..."] as! String)&type=\(apiType)&api=grubnchill&user_id=\(GlobalMethod.userDetail!.user_id! as Any)&reviews_restaurantid=\(selectedDict["Restaurent Id"] as! String)&reviews_foodcategoryid=\(selectedDict["CategoryId"] as! String)")
        
        
        
         let value = String(format: "reviews_foodtypeid=\(selectedDict["Dish Name"] as! String)&reviews_text=\(selectedDict["Write here with Chill..."] as! String)&type=\(apiType)&api=grubnchill&user_id=\(GlobalMethod.userDetail!.user_id! as Any)&reviews_restaurantid=\(selectedDict["Restaurent Id"] as! String)&reviews_foodcategoryid=\(selectedDict["CategoryId"] as! String)&review_rating=\(ratingValue)")
        
        APIManager.shared.uploadImage(values: value, imagesArray: imageArray, postKey: ["reviews_image"], showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                APIManager.shared.hideProgress()

                if error == nil {
                    print(jsonData?.jsonDic)
                    if self.postType == "Edit"{
                        self.postObject.restaurant_name = self.selectedDict["Restaurent Name"] as? String
                        self.postObject.restaurant_foodtype = self.selectedDict["Dish Name"] as? String
                        self.postObject.restaurant_foodcategory = self.selectedDict["Category"] as? String
                        self.postObject.review_rating = self.ratingValue
                        self.postObject.review_text = self.selectedDict["Write here with Chill..."] as? String
                        self.postObject.restaurant_id = self.selectedDict["Restaurent Id"] as? String
                        self.postObject.restaurant_foodcategoryid = self.selectedDict["CategoryId"] as? String
                   
                        let notificationType = self.controllerType == "Profile" ? "updateReviewPost" : "loadTableViewData"
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationType), object: self.postObject) //posts the list items obect
                    }
                    
                    else {
                        let obj = homeObjectData.init(json: jsonData?.jsonDic?["data"] as! [String : Any])
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newObjectAddInArray"), object: obj)
                        
                    }
                    
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
    }
    
    @IBAction func _crossImageClick(_ sender: Any) {
        _postImageView.image = nil
        _tableView.tableHeaderView = nil

    }
    
    @objc func changeRating(sender : HCSStarRatingView){
        ratingValue = "\(sender.value)"
    }
    
    func headerSetOnTableView(image : UIImage){
        isImageSelectForPost = true
        _postImageView.image = image
        _tableView.tableHeaderView = _tableHeaderView
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ReviewPostVC : UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            textView.text = "Write here with Chill..."
        }
        else{
        selectedDict.setValue(textView.text!, forKey: titleArray[textView.tag])
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write here with Chill..." {
            textView.text = ""
        }
    }
}

extension ReviewPostVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedDict.setValue(textField.text!, forKey: titleArray[textField.tag])
    }
}

extension ReviewPostVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if titleArray[indexPath.row] == "Write here with Chill..."{
            return 180

        }
        else{
            return UITableView.automaticDimension

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if titleArray[indexPath.row] == "Write here with Chill..."{
            let identifier = "TextViewCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextViewCell
            if cell == nil {
                tableView.register(UINib(nibName: "TextViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextViewCell
            }
            cell?._textView.tag = indexPath.row;
            cell?._textView.delegate = self
            if selectedDict[titleArray[indexPath.row]] != nil{
                cell?._textView.text = selectedDict[titleArray[indexPath.row]] as? String
            }
            else{
                cell?._textView.text = titleArray[indexPath.row]
            }
            
            return cell!
        }
        else if titleArray[indexPath.row] == "Rating"{
            let identifier = "RatingCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RatingCell
            if cell == nil {
                tableView.register(UINib(nibName: "RatingCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RatingCell
            }
            cell?.ratingView.addTarget(self, action: #selector(changeRating(sender:)), for: .valueChanged)
//            if postType == "Edit"{
            
                if let n = NumberFormatter().number(from: ratingValue) {
                    let f = CGFloat(truncating: n)
                    cell?.ratingView.value = f
                }
//            }
//            cell?.ratingView.delegate = self;
//            cell?._sliderView.addTarget(self, action: #selector(changeRating(sender:)), for: .valueChanged)
            return cell!

        }
            
            
        else{
            let identifier = "TextFieldCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextFieldCell
            if cell == nil {
                tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TextFieldCell
            }
            cell?._txtField.tag = indexPath.row;
            cell?._txtField.delegate = self

            if selectedDict[titleArray[indexPath.row]] != nil{
                cell?._txtField.text = selectedDict[titleArray[indexPath.row]] as? String
            }
            

            if titleArray[indexPath.row] == "Dish Name" {
                cell?._txtField.isUserInteractionEnabled = true;
            }
            else{
                cell?._txtField.isUserInteractionEnabled = false;

            }
            cell?._txtField.autocapitalizationType = .sentences
            cell?._txtField.placeholder = titleArray[indexPath.row]
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if titleArray[indexPath.row] == "Category"{
            let viewObject = FoodCategoryVC.init(nibName: "FoodCategoryVC", bundle: nil)
            self.navigationController?.pushViewController(viewObject, animated: true)
        }
        else if titleArray[indexPath.row] == "Restaurent Name"{
            let viewObject = RestaurantVC.init(nibName: "RestaurantVC", bundle: nil)
            self.navigationController?.pushViewController(viewObject, animated: true)

        }
//        else if titleArray[indexPath.row] == "Dish Name"{
//            let viewObject = FoodTypeVC.init(nibName: "FoodTypeVC", bundle: nil)
//            self.navigationController?.pushViewController(viewObject, animated: true)
//
//        }
    }
    
    
}

//extension ReviewPostVC : CameraManagerDelegate{
//    func didEndChoose(image: UIImage) {
//        isImageSelectForPost = true
//        self.dismiss(animated: true, completion: nil)
//        _postImageView.image = image
//        _tableView.tableHeaderView = _tableHeaderView
//    }
//}

extension ReviewPostVC : CustomCameraPopUpDelegate{
   
    func didCameraClick() {
        CameraManager.sharedInstance.PickImageFromCamera()
        CameraManager.sharedInstance.delegate = self
        
    }
    func didGalleryClick() {
        CameraManager.sharedInstance.PickImageFromGalary()
        CameraManager.sharedInstance.delegate = self
        
    }
}
//
//extension ReviewPostVC: FloatRatingViewDelegate {
//
//    // MARK: FloatRatingViewDelegate
//
//    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
//    }
//
//    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
//    }
//
//}
