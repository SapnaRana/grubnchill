//
//  LocationVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/11/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import MapKit

class LocationVC: ParentClass {
    @IBOutlet weak var _mapView: MKMapView!
    @IBOutlet weak var _topView: UIView!
    @IBOutlet weak var _buttomView: UIView!
    var locationAnnotations = LocationAnnotations()
    
    var pinArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHeaderView(view: _topView)
        self.setFooterView(view: _buttomView)
        _headerView._icon.isHidden   = false
        _headerView._wheelBtn.isHidden = true
        _headerView._saveChangeBtn.isHidden = true
        _headerView._searchBtn.isHidden = true
        
        self.getNearestListOfRestaurantAndUser()
        
        let lat = UserDefaults.standard.object(forKey: "lat") as! String
        let long = UserDefaults.standard.object(forKey: "long") as! String
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!), span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
        DispatchQueue.main.async {
            self._mapView.setRegion(region, animated: true)
        }
        

        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self._mapView.removeAnnotations(self.locationAnnotations.restaurants)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if self.locationAnnotations.restaurants.count > 0{
            self._mapView.addAnnotations(self.locationAnnotations.restaurants)
            self._mapView.reloadInputViews()

        }
    }
    
    //MARK:- Get Nearest List Of Restaurant And User Method
    func getNearestListOfRestaurantAndUser(){
        let lat = UserDefaults.standard.object(forKey: "lat") as! String
        let long = UserDefaults.standard.object(forKey: "long") as! String

        print(lat)
        print(long)
        if lat != nil && long != nil {
            APIManager.shared.getRequest(values: "&type=getnearestlistformap&lat=\(lat)&lon=\(long)&user_id=\(GlobalMethod.userDetail!.user_id! as Any)", showIndicator: false, viewController: self) { (error, jsonData) in
                DispatchQueue.main.async {
                    print(jsonData?.jsonDic!)
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    self.pinArray = NSMutableArray(array: localArray)
                    for case let dict as NSDictionary  in localArray {
                        let distance = dict["distance"] != nil ? getString(string: dict["distance"] as Any) : ""
                        let icon = dict["icon"] != nil ? getString(string: dict["icon"] as Any) : ""
                        let id = dict["id"] != nil ? getString(string: dict["id"] as Any) : ""
                        let lat = dict["lat"] != nil ? getString(string: dict["lat"] as Any) : ""
                        let lon = dict["lon"] != nil ? getString(string: dict["lon"] as Any) : ""
                        let name = dict["name"] != nil ? getString(string: dict["name"] as Any) : ""
                        let status = dict["status"] != nil ? getString(string: dict["status"] as Any) : ""
                        if status == "restaurant"{
                            self.locationAnnotations.restaurants += [LocationModal(Double(lat)!, Double(lon)!, id: id, icon: icon, type: .restaurant, distance: distance, name: name)]
                        }
                        else{
                            self.locationAnnotations.restaurants += [LocationModal(Double(lat)!, Double(lon)!, id: id, icon: icon, type: .user, distance: distance, name: name)]
                        }
                    }
                    self._mapView.addAnnotations(self.locationAnnotations.restaurants)
                    self._mapView.reloadInputViews()
                }
            }
        }
        
    }
    
    
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension LocationVC : MKMapViewDelegate{
 
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation!.title != "My Location" {
            if let valueModel = view.annotation as? LocationModal{
                let reviewPostVC = RestaurantDetailVC.init(nibName: "RestaurantDetailVC", bundle: nil)
                reviewPostVC.restaurantId = (valueModel.id)!
                self.navigationController?.pushViewController(reviewPostVC, animated: true)
            }
        }
    }
    
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var annotationView = MKMarkerAnnotationView()
        guard let annotation = annotation as? LocationModal else {return nil}
        var identifier = ""
        var color = UIColor.red
        switch annotation.type{
        case .user:
            identifier = "User"
            color = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        case .restaurant:
            identifier = "Restaurant"
            color = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        }
        if let dequedView = mapView.dequeueReusableAnnotationView(
            withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            annotationView = dequedView
        } else{
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        }
        annotationView.markerTintColor = color
        
        if annotation.type == .restaurant {
            annotationView.glyphImage = UIImage(named: "restIcon")

        }
        

        
//        annotationView.glyphImage = UIImage(named: "pizza")
        annotationView.glyphTintColor = .yellow
        annotationView.clusteringIdentifier = identifier
        return annotationView
    }
    
    
}

