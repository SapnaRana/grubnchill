//
//  TermsAndConditionVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire
class TermsAndConditionVC: ParentClass {
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet var _tableFooterView: UIView!
    @IBOutlet var _tableHeaderView: UIView!
    @IBOutlet weak var _titleLbl: UILabel!
    @IBOutlet weak var _signUpButton: UIButton!
    var typeOfView        = ""
    var  page_content = ""

    override func viewDidLoad() {
        super.viewDidLoad()

//        webView2.loadHTMLString("<html><body><p>Hello!</p></body></html>", baseURL: nil)

        _tableView.tableHeaderView = _tableHeaderView
        _tableView.tableFooterView = _tableFooterView
        _signUpButton.layer.cornerRadius = 20
        if typeOfView == "privacy"{
            _titleLbl.text = "Privacy Policy"
            page_content = "http://www.letsgrubnchill.com/cms-page-for-app/?post_id=23"
        }
        else{
            _titleLbl.text = "Terms And Condition"
            page_content = "http://www.letsgrubnchill.com/cms-page-for-app/?post_id=20"
        }
        self._tableView.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.getDetail()
    }
    func getDetail(){
        var path = ""
        if typeOfView == "privacy"{
           path = "type=cms&slug=privacy-ploicy"
        }
        else{
            path = "type=cms&slug=terms-and-conditions"
        }
        
        APIManager.shared.getRequest(values: path, showIndicator: true, viewController: self) { (error, JsonData) in
            DispatchQueue.main.async {
                if JsonData?.jsonDic! != nil {
                self.showMessage(msg: JsonData?.jsonDic!["message"] as! String)
                }
                if error == nil {
                   let dict  = JsonData?.jsonDic!["data"] as! NSDictionary
                    self.page_content = (dict["page_content"] as? String)!
                }
                else{
                    
                }
                self._tableView.reloadData()
                
            }
        }
        
        
    }


    @IBAction func _backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func _signUpClick(_ sender: Any) {
        let viewObject = RegistrationVC.init(nibName: "RegistrationVC", bundle: nil)
        let controller =  UIManager.shared.checkNavigationController(getviewController: RegistrationVC.self, navigation: self.navigationController!)
        if !controller .isKind(of: RegistrationVC.self) {
            self.navigationController?.pushViewController(viewObject, animated: false)
        }
        else{
            self.navigationController?.popToViewController(controller, animated: false)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TermsAndConditionVC : UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "TermCustomCell"
        var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TermCustomCell
        if cell == nil {
            tableView.register(UINib(nibName: "TermCustomCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? TermCustomCell
        }
        
//
//        let url = URL(string: onBoardingObj.page_content!)!
//        _webKit.load(URLRequest(url: url))
//        _webKit.allowsBackForwardNavigationGestures = true

        cell?._webView.backgroundColor = UIColor.clear
        cell?._webView.allowsBackForwardNavigationGestures = true
        let url = URL(string: page_content)!
        cell?._webView.load(URLRequest(url: url))

        
//        cell?._textView.text  = page_content
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
