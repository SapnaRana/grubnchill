//
//  HomeVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 18/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Windless
import Alamofire
import SDWebImage
import DropDown
import CoreLocation
//import FirebaseAnalytics

class HomeVC: ParentClass {
    
    var page_num      = 1
    var page_limit    = 10
    @IBOutlet weak var _tableView: UITableView!
    @IBOutlet weak var _topView: UIView!
    var animationStop = false
    @IBOutlet weak var _buttomView: UIView!
    let chooseArticleDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseArticleDropDown,
            ]
    }()
    
    var timer = Timer()

    
    var refreshControl = UIRefreshControl()
    var isLoadingList : Bool = false
    
    @IBOutlet var _tableHeaderView: UIView!
    var homeDataArray = [homeObjectData]()
    var checkIsFirstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)

        
        LocationManager.sharedInstance.locationManager.startMonitoringSignificantLocationChanges()
        LocationManager.sharedInstance.locationManager.startUpdatingLocation()
        LocationManager.sharedInstance.delegate = self

        
        
        checkIsFirstTime = true
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshHomePage(notification:)) , name: NSNotification.Name(rawValue: "refreshHomePage"), object: nil)

        
        NotificationCenter.default.addObserver(self, selector: #selector(newObjectAddInArray(notification:)) , name: NSNotification.Name(rawValue: "newObjectAddInArray"), object: nil)

        
        NotificationCenter.default.addObserver(self, selector: #selector(loadTableViewData(notification:)) , name: NSNotification.Name(rawValue: "loadTableViewData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openCameraForSlideMenu) , name: NSNotification.Name(rawValue: "slideCameraOpen"), object: nil)

        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        _tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(connectNowMessageShow(notification:)) , name: NSNotification.Name(rawValue: "connectNowMessage"), object: nil)

        
        
        
        GlobalMethod.shared.setUserData()
        self.setHeaderView(view: _topView)
        self.setFooterView(view: _buttomView)
        
        _headerView.searchConstent.constant = 0
        _headerView._searchBtn.isHidden = true;
        _tableView.tableFooterView = UIView(frame: _buttomView.bounds)
        self.navigationController?.isNavigationBarHidden = true
        
        DispatchQueue.main.async {
            self.startAnimation(view: self._tableView)
            self.getUserProfile(showIndicator: false)
            self.getHomePageData(isRefersh: false)
        }
        
//        self.updateNearByRestaurant()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        if checkIsFirstTime == false{
            self.refresh()
        }
//        checkIsFirstTime = false
        
        
        self.getNotificationCount()
        self.getBlogCategory()
    }
    
    
    //MARK:- Timer Method For User Location Update
    @objc func timerAction() {
        self.userLocationUpdate()
    }
    
    func userLocationUpdate(){
        let lat = UserDefaults.standard.object(forKey: "lat")
        let long = UserDefaults.standard.object(forKey: "long")
        if lat != nil && long != nil {
            APIManager.shared.getRequest(values: "&type=getuserlocation&lat=-\(lat as! String)&lon=\(long as! String)&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
                DispatchQueue.main.async {
//                    print(jsonData?.jsonDic!)
                }
            }
        }

//        http://www.letsgrubnchill.com/api/Api.php?app=grubnchill&type=getnearestlistformap&lat=28.496810076784804&lon=77.09286245524612
    }
    
    
    @objc func openCameraForSlideMenu(){
        
        self.openCamera(controllerType: "slide")
        
    }
    @objc func refreshHomePage(notification: NSNotification) {
        self.refresh()
    }
    
    @objc func newObjectAddInArray(notification: NSNotification) {
        let obj = notification.object as! homeObjectData
        self.homeDataArray.insert(obj, at: 0)
        self._tableView.reloadData()
        
    }

    
    
    //MARK:- Refersh Method
    @objc func refresh(){
        self.getNotificationCount()
        page_num   = 1
        self.getHomePageData(isRefersh: true)
    }
    
    @IBAction func _clickForPost(_ sender: Any) {
        
        selectedFoodCategoryObj = [foodCategoryObject]()
//        selectedRestaurantObj   = nil
        selectedFoodTypeObj     = [foodTypeObject]()
        
        let reviewPostVC = ReviewPostVC.init(nibName: "ReviewPostVC", bundle: nil)
        reviewPostVC.postType = "Create"
        self.navigationController?.pushViewController(reviewPostVC, animated: true);
    }
    
    func getUserProfile(showIndicator : Bool){
        let path = "type=editprofile&user_id=\(GlobalMethod.userDetail!.user_id!)"
        APIManager.shared.getRequest(values: path, showIndicator: showIndicator, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    
                    let profileDict  = jsonData?.jsonDic!["data"] as! NSDictionary
                    GlobalMethod.shared.saveUserDefaultValue(key: profileObjectKey, value: profileDict as Any)
                    GlobalMethod.shared.getUserProfileObject()
                }
            }
        }
        
    }
    
    @objc func connectNowMessageShow(notification: NSNotification) {
        let obj = notification.object as! String
        self.showMessage(msg: obj)
    }
    
    @objc func loadTableViewData(notification: NSNotification) {
        let obj = notification.object as! homeObjectData
        self.updateArrayObj(obj: obj)
    }
    
//    @objc func stopAnimation(){
//        _tableView.windless.end()
//        animationStop = true
//        _tableView.reloadData()
//    }
//
//    @objc func startAnimation(){
//        _tableView.windless
//            .apply {
//                $0.beginTime = 0.1
//                $0.duration = 2
//                $0.animationLayerOpacity = 0.5
//            }
//            .start()
//    }
    
    @objc func didDotActionClick(sender: UIButton) {
        self.setupChooseArticleDropDown(listArray: ["Edit","Delete"], choosebutton: sender )
        self.chooseArticleDropDown.show()
    }
    
    func setupChooseArticleDropDown(listArray : NSArray , choosebutton : UIButton ) {
        chooseArticleDropDown.anchorView = choosebutton
        chooseArticleDropDown.bottomOffset = CGPoint(x: -60, y: choosebutton.frame.size.height)
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseArticleDropDown.dataSource = listArray as! [String]
        // Action triggered on selection
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            self?.chooseArticleDropDown.hide()
            if item == "Edit"{
                let viewObject = ReviewPostVC.init(nibName: "ReviewPostVC", bundle: nil)
                viewObject.postType = "Edit"
                viewObject.postObject = self?.homeDataArray[choosebutton.tag]
                self?.navigationController?.pushViewController(viewObject, animated: true)
            }
            else{
                let alertView = UIAlertController(title: "Grub n Chill", message: "Are you sure want to delete?", preferredStyle: .alert)
                let canclAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel , handler: nil)
                let ok = UIAlertAction(title: "Ok", style: .default, handler: {  _ in
                    self?.deletePostMethod(tag: choosebutton.tag)
                })
                alertView.addAction(canclAction)
                alertView.addAction(ok)
                self?.present(alertView, animated: true, completion: nil)
            }
        }
        
    }
    
    
    //MARK: - Comment Click Method
    
    @objc func clickCommentButton(sender : UIButton){
        self.sendCommentListView(obj: homeDataArray[sender.tag])
    }
    
    
    
    @objc func clickLikeButton(sender : UIButton){
        if sender.titleLabel?.text == "Like"{
            sender.setTitle("Unlike", for: .normal)
            sender.setImage(UIImage.init(named: "fullLike"), for: .normal)
            sender.setTitleColor(#colorLiteral(red: 0.9568627451, green: 0.2196078431, blue: 0.1803921569, alpha: 1), for: .normal)
        }
        else{
            sender.setTitle("Like", for: .normal)
            sender.setImage(UIImage.init(named: "like"), for: .normal)
            sender.setTitleColor(#colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1), for: .normal)
        }
        self.likeAndUnlikeMethod(tag: sender.tag)
    }
    
    @objc func clickShareButton(sender : UIButton)
    {
        self.sharePostMethod(tag:sender.tag)
        
    }
    func updateArrayObj(obj : homeObjectData){
        if let row = self.homeDataArray.index(where: {$0.review_id == obj.review_id}) {
            self.homeDataArray[row] = obj
            self._tableView.beginUpdates()
            self._tableView.reloadRows(at: [IndexPath(item: row, section: 0)], with: .automatic)
            self._tableView.endUpdates()
            
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension HomeVC{
    
    //MARK: - Home Page All Api Method
    //MARK: - Like and Unlike Post Method
    func likeAndUnlikeMethod(tag : Int){
        var obj = homeDataArray[tag]
        let status = obj.review_userliked == "1" ? "2" : "1"
        APIManager.shared.getRequest(values: "type=getreviewsstatus&review_id=\(obj.review_id!)&status=\(status)&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    var count = Int(obj.review_likedcount!)
                    count = status == "1" ? count! + 1 : count! - 1
                    obj.review_likedcount = "\(String(describing: count!))"
                    obj.review_userliked = status == "1" ? "1" : "0"
                    self.updateArrayObj(obj: obj)
                    
                }
            }
        }
    }
    
    
    
    func getBlogCategory(){
        
        //        http://www.letsgrubnchill.com/api/Api.php?app=grubnchill&type=getblogcategorylisting
        
        
        APIManager.shared.getRequest(values: "type=getblogcategorylisting", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray{
                        let obj = blogCategoryObject.init(json: dict as! [String : Any])
                        blogCategoryArray.append(obj)
                    }
                }
            }
        }
        
    }
    
    
    //MARK: - Delete Post Method
    func deletePostMethod(tag : Int){
        let obj = homeDataArray[tag]
        APIManager.shared.getRequest(values: "type=deleteReviews&user_id=\(GlobalMethod.userDetail!.user_id!)&review_id=\(obj.review_id!)", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    self.homeDataArray.remove(at: tag)
                    self._tableView.deleteRows(at: [IndexPath(row: tag, section: 0)], with: .automatic)
                }
            }
        }
    }
    
    
    
    //MARK: - Share Post Method
    func sharePostMethod(tag : Int){
        sharePost = homeDataArray[tag]
        self.shareONFacebook()
        
    }
    
    //MARK:-  Get Review List Method
    func getHomePageData(isRefersh : Bool){
        APIManager.shared.getRequest(values: "type=getreviews&page_num=\(page_num)&page_limit=\(page_limit)&user_id=\(GlobalMethod.userDetail!.user_id!)", showIndicator: false, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil{
                    print(jsonData?.jsonDic!)
                    self.isLoadingList = false
                    
                    if isRefersh == true{
                        self.homeDataArray.removeAll()
                    }
                    
                   
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray{
                        let obj = homeObjectData.init(json: dict as! [String : Any])
                        self.homeDataArray.append(obj)
                    }
                    if localArray.count > 0{
                        if self.animationStop == false || isRefersh == true{
                            self._tableView.tableHeaderView = self._tableHeaderView;
                            self.stopAnimation(view: self._tableView)
                            self.animationStop = true
                            self._tableView.reloadData()
                        }
                        else {
                            
                            var indexPathsArray = [IndexPath]()
                            var totalCount = (self.homeDataArray.count - localArray.count ) - 1
                            for _ in 0..<localArray.count{
                                totalCount = totalCount + 1
                                let indexPath = IndexPath(row: totalCount, section: 0)
                                indexPathsArray.append(indexPath)
                            }
                            print(indexPathsArray)
                            print(self.homeDataArray.count)
                            self._tableView.beginUpdates()
                            self._tableView.insertRows(at: indexPathsArray, with: .automatic)
                            self._tableView.endUpdates()
                        }
                        self.refreshControl.endRefreshing()
                        print(self.homeDataArray.count)
                    }
                }
                else{
                    self.page_num = self.page_num - 1
                    if self.isLoadingList == false{
                        self.homeDataArray.removeAll()
                        self._tableView.reloadData()
                    }
                    
                }
            }
        }
    }
    
 
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height ) && !isLoadingList){
            isLoadingList = true
            page_num = page_num + 1
            self.getHomePageData(isRefersh: false)
        }
        
    }
    
}

extension HomeVC {
    
    
    @objc func clickImageView(sender : UITapGestureRecognizer){
        

        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let viewObject = SinglePostVC.init(nibName: "SinglePostVC", bundle: nil)
            viewObject.reviewObject = homeDataArray[indexPath.row]
            viewObject.reviewId = homeDataArray[indexPath.row].review_id!
            viewObject.delegate = self
            self.navigationController?.pushViewController(viewObject, animated: true)
//            let cell = _tableView.cellForRow(at: indexPath) as? HomeImageCell
//            self.openImageViewController(sender: cell!._imageView)
        }

        
    }
    
    @objc func clickResturantName(sender : UITapGestureRecognizer){
        
        let reviewPostVC = RestaurantDetailVC.init(nibName: "RestaurantDetailVC", bundle: nil)
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let obj = homeDataArray[indexPath.row]
            reviewPostVC.restaurantId = obj.restaurant_id!
        }
        self.navigationController?.pushViewController(reviewPostVC, animated: true)
    }
    @objc func clickLiked(sender : UITapGestureRecognizer){
        let viewObjet = LikedUserListVC.init(nibName: "LikedUserListVC", bundle: nil)
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            viewObjet.postObject = homeDataArray[indexPath.row]
        }
        let nvc = UINavigationController(rootViewController: viewObjet)
        nvc.isNavigationBarHidden = true
        self.present(nvc, animated: true, completion: nil)
    }
    
    
    
    @objc func clickCommentCount(sender : UITapGestureRecognizer){
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            self.sendCommentListView(obj: homeDataArray[indexPath.row])
        }
    }
    
    @objc func clickUserDetail(sender : UITapGestureRecognizer){
        let touch = sender.location(in: _tableView)
        if let indexPath = _tableView.indexPathForRow(at: touch) {
            let Obj = homeDataArray[indexPath.row]
            let viewObject = ProfileVC.init(nibName: "ProfileVC", bundle: nil)
            viewObject.viewType = "backShow"
            viewObject.homeObjData = Obj
            self.navigationController?.pushViewController(viewObject, animated: true)
        }
    }
}

extension HomeVC : UITableViewDelegate , UITableViewDataSource {
    
    //MARK:- UITableView delegate Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if animationStop == true{
            return self.homeDataArray.count
        }
        else {
            return 4
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if animationStop == true {
            return UITableView.automaticDimension
        }
        else{
            if indexPath.row % 2 == 0 {
                
                return 100
            }
            else{
                return 300
                
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            self._tableView.tableFooterView = spinner
            self._tableView.tableFooterView?.isHidden = false
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if animationStop == false {
            
            if indexPath.row % 2 == 0 {
                let identifier = "FirstCell"
                var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FirstCell
                if cell == nil {
                    tableView.register(UINib(nibName: "FirstCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? FirstCell
                }
                return cell!
            }
            else{
                let identifier = "CenterCell"
                var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? CenterCell
                if cell == nil {
                    tableView.register(UINib(nibName: "CenterCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? CenterCell
                }
                return cell!
            }
            
        }
        else {
            let obj = homeDataArray[indexPath.row]
            let identifier = "HomeImageCell"
            var  cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeImageCell
            if cell == nil {
                tableView.register(UINib(nibName: "HomeImageCell", bundle: nil), forCellReuseIdentifier: identifier)
                cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? HomeImageCell
            }
            cell?.setCellData(obj: obj, indexPath: indexPath, contoller: self)
            return cell!
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.homeDataArray.count > 0 {
//            let viewObject = SinglePostVC.init(nibName: "SinglePostVC", bundle: nil)
//            viewObject.reviewObject = homeDataArray[indexPath.row]
//            viewObject.reviewId = homeDataArray[indexPath.row].review_id!
//            viewObject.delegate = self
//            self.navigationController?.pushViewController(viewObject, animated: true)

//            if GlobalMethod.userDetail?.user_id == self.homeDataArray[indexPath.row].user_id{
//                let viewObject = ReviewPostVC.init(nibName: "ReviewPostVC", bundle: nil)
//                viewObject.postType = "Edit"
//                viewObject.controllerType = "Home"
//                viewObject.postObject = self.homeDataArray[indexPath.row]
//                self.navigationController?.pushViewController(viewObject, animated: true)
//            }
        }
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastElement = self.homeDataArray.count - 1
//        if !isLoadingList && indexPath.row == lastElement {
//        }
//    }
    
    
}

extension HomeVC : LocationManagerDelegate {
    
    func didUpdateLocations(locations: [CLLocation]) {
        if (locations.first as CLLocation?) != nil {
            let location = locations.first
            UserDefaults.standard.set("\(location!.coordinate.latitude)", forKey: "lat")
            UserDefaults.standard.set("\(location!.coordinate.longitude)", forKey: "long")
            UserDefaults.standard.synchronize()
        }
    }
    
    
}


extension HomeVC : CommentDelegate{
    func commentCountUpdate(value: homeObjectData) {
        self.updateArrayObj(obj: value)
    }
}

extension HomeVC : LikeCommetDelegate {
    func likeCountUpdate(value: homeObjectData) {
        self.updateArrayObj(obj: value)
        
    }
    func deleteReviewPost(value: homeObjectData) {
        if let row = self.homeDataArray.index(where: {$0.review_id == value.review_id}) {
            self.homeDataArray.remove(at: row)
            self._tableView.deleteRows(at: [IndexPath(row: row, section: 0)], with: .automatic)

        }
    }
}

