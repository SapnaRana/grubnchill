//
//  FirstCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 22/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class FirstCell: UITableViewCell {
    @IBOutlet weak var _bgView: UIView!
    @IBOutlet weak var _lbl1: UILabel!
    @IBOutlet weak var _lbl2: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        _bgView.layer.cornerRadius = 5
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
