//
//  CenterCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 22/08/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class CenterCell: UITableViewCell {
    @IBOutlet weak var _centerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        _centerView.layer.cornerRadius = 5

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
