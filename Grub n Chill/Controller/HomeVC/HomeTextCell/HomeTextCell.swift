//
//  HomeTextCell.swift
//  Grub n Chill
//
//  Created by orangemac04 on 16/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit

class HomeTextCell: UITableViewCell {
    @IBOutlet weak var _nameLbl: UILabel!
    @IBOutlet weak var _timeLbl: UILabel!
    @IBOutlet weak var _descLbl: UILabel!
    @IBOutlet weak var _commentAndShareLbl: UILabel!
    @IBOutlet weak var _likeLbl: UILabel!
    @IBOutlet weak var _likeBtn: UIButton!
    @IBOutlet weak var _commentBtn: UIButton!
    @IBOutlet weak var _shareBtn: UIButton!
    @IBOutlet weak var _userImage: UIImageView!
    @IBOutlet weak var _dotBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        _userImage.layer.cornerRadius = _userImage.bounds.width / 2
        _userImage.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        _userImage.layer.borderWidth = 1

        // Initialization code
    }

//    func setCellData(obj : homeObjectData , indexPath : IndexPath , contoller : UIViewController){
//        
//        _nameLbl.text = obj.review_username
//        _timeLbl.text = obj.review_date
//        _descLbl.text = obj.review_text
//        _userImage.sd_setImage(with:  URL(string:(obj.review_userimage!)), placeholderImage:UIImage(named: "placeHolder"))
//        
//        if Int(obj.review_likedcount!)! > 1{
//            _likeLbl.text = obj.review_likedcount! == "0" ? "0 Like" :  obj.review_likedcount! + " Likes"
//        }
//        else{
//            _likeLbl.text = obj.review_likedcount! == "0" ? "0 Like" :  obj.review_likedcount! + " Like"
//        }
//        
//        let tapGesture = UITapGestureRecognizer(target: contoller, action: #selector(HomeVC.clickLiked(sender:)))
//        _likeLbl.isUserInteractionEnabled = true
//        _likeLbl.addGestureRecognizer(tapGesture)
//        
//        
//        
//        
////        _commentAndShareLbl.text = obj.review_commentcount! == "0" ? "0 Comment" :  obj.review_commentcount! + " Comment"
//        //        _ShareCountLbl.text = obj.review_sharedcount! == "0" ? "0 Share" :  obj.review_sharedcount! + " Share"
//        
//        
//        
//        if GlobalMethod.userDetail?.user_id == obj.user_id{
//            _dotBtn.isHidden = false
//        }
//        else{
//            _dotBtn.isHidden = true
//        }
//        
//        let tapGesture1 = UITapGestureRecognizer(target: contoller, action: #selector(HomeVC.clickCommentCount(sender:)))
//        _commentAndShareLbl.isUserInteractionEnabled = true
//        _commentAndShareLbl.addGestureRecognizer(tapGesture1)
//        
//        
//        if obj.review_userliked == "1"{
//            _likeBtn.setTitle("Unlike", for: .normal)
//            _likeBtn.setImage(UIImage.init(named: "fullLike"), for: .normal)
//            _likeBtn.setTitleColor(#colorLiteral(red: 0.9568627451, green: 0.2196078431, blue: 0.1803921569, alpha: 1), for: .normal)
//        }
//        else{
//            _likeBtn.setTitle("Like", for: .normal)
//            _likeBtn.setImage(UIImage.init(named: "like"), for: .normal)
//            _likeBtn.setTitleColor(#colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1), for: .normal)
//        }
//        _likeBtn.tag = indexPath.row
//        _likeBtn.addTarget(contoller, action: #selector(HomeVC.clickLikeButton(sender:)), for: .touchUpInside)
//        
//        _commentBtn.tag = indexPath.row
//        _commentBtn.addTarget(contoller, action: #selector(HomeVC.clickCommentButton(sender:)), for: .touchUpInside)
//        
//        
//        _shareBtn.tag = indexPath.row
//        _shareBtn.addTarget(contoller, action: #selector(HomeVC.clickShareButton(sender:)), for: .touchUpInside)
//        
//        _dotBtn.tag = indexPath.row
//        _dotBtn.addTarget(contoller, action: #selector(HomeVC.didDotActionClick(sender:)), for: .touchUpInside)
//        
//        
//        
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
