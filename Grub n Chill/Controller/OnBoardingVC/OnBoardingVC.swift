//
//  OnBoardingVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 16/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import Alamofire
class OnBoardingVC: ParentClass,CAPSPageMenuDelegate {
    var controllerArray : [UIViewController] = []
    var pageMenu : CAPSPageMenu?
    @IBOutlet weak var _buttomView: UIView!
    @IBOutlet weak var _nextButton: UIButton!
    @IBOutlet weak var _skipButton: UIButton!
    @IBOutlet weak var _pageController: UIPageControl!
    @IBOutlet weak var _introView: UIView!
    var onBoardingArray = [onBoardingDataObject]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.getOnBoardingData()
        
        
        UserDefaults.standard.setValue("1", forKey: "RunAppCheck")
        UserDefaults.standard.synchronize()
        // Do any additional setup after loading the view.
    }
    
    
    func getOnBoardingData(){
        APIManager.shared.getRequest(values: "type=onboarding&slug=onboarding", showIndicator: true, viewController: self) { (error, jsonData) in
            DispatchQueue.main.async {
                if error == nil {
                    print(jsonData?.jsonDic!)
                    self.onBoardingArray.removeAll()
                    let localArray = jsonData?.jsonDic!["data"] as! NSArray
                    for dict in localArray{
                        let obj = onBoardingDataObject.init(json: dict as! [String : Any])
                        self.onBoardingArray.append(obj)
                    }
                    if  self.controllerArray.count == 0{
                        self.pageSet()
                    }
                }
            }
        }
        
    }

    override func viewDidLayoutSubviews() {
        
    }
    
    func pageSet() {
        _pageController.numberOfPages = onBoardingArray.count
        for obj in onBoardingArray{
            let controller : BoardingScreenVC = BoardingScreenVC(nibName: "BoardingScreenVC", bundle: nil)
            controller.title = ""
            controller.onBoardingObj = obj
            self.controllerArray.append(controller)
        }
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorHeight(2.0),
            .selectionIndicatorColor(UIColor.clear),
            .bottomMenuHairlineColor(UIColor.clear),
            .selectedMenuItemLabelColor(UIColor.blue),
            .unselectedMenuItemLabelColor(UIColor.white),
            .useMenuLikeSegmentedControl(true),
            .enableHorizontalBounce(false),
            .showRoundFilledTitle(true),
            .menuMargin(0.0),
            .menuItemSeparatorPercentageHeight(0.0),
            .menuItemFont(UIFont.systemFont(ofSize: 17.0)),
            .selectedMenuItemFont(UIFont.systemFont(ofSize: 17.0)),
            .menuHeight(0.0),
            .centerMenuItems(true)
        ]
        
        // Initialize scroll menu
        self.pageMenu = CAPSPageMenu(viewControllers: self.controllerArray, frame: CGRect(x: 0, y:0, width: self.view.frame.size.width, height: self.view.frame.size.height - _buttomView.frame.size.height), pageMenuOptions: parameters)
        //        self.addChildViewController(pageMenu!)
        self._introView.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParent: self)
        pageMenu!.delegate = self
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int){
        
    }
    func didMoveToPage(_ controller: UIViewController, index: Int){
//        if index == 3 {
//            _skipButton.isHidden = true
//        }
//        else{
//            _skipButton.isHidden = false
//        }
        _pageController.currentPage = index
    }
    
    
    @IBAction func _nextClick(_ sender: Any) {
        if _pageController.currentPage == 3 {
            let viewObject:HomeVC = HomeVC(nibName: "HomeVC", bundle: nil)
            self.navigationController?.pushViewController(viewObject, animated: false)
        }
        else{
            pageMenu?.moveToPage(_pageController.currentPage + 1)
            _pageController.currentPage = _pageController.currentPage + 1
            if _pageController.currentPage == 3 {
                _skipButton.isHidden = true
            }
            else{
                _skipButton.isHidden = false
            }
            
        }
    }
    
    @IBAction func _skipClick(_ sender: Any) {
        self.sendnextHomePage()
//        let viewObject:HomeVC = HomeVC(nibName: "HomeVC", bundle: nil)
//        self.navigationController?.pushViewController(viewObject, animated: false)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
