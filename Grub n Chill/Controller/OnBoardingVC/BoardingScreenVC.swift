
//
//  BoardingScreenVC.swift
//  Grub n Chill
//
//  Created by orangemac04 on 16/09/18.
//  Copyright © 2018 orangemac04. All rights reserved.
//

import UIKit
import SDWebImage
import WebKit

class BoardingScreenVC: UIViewController {
    @IBOutlet weak var _webKit: WKWebView!
    
    var onBoardingObj : onBoardingDataObject!

    @IBOutlet weak var _imageView: UIImageView!
    @IBOutlet weak var _titleLbl: UILabel!
    @IBOutlet weak var _txtLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let url = URL(string: onBoardingObj.page_content!)!
        _webKit.load(URLRequest(url: url))
        _webKit.allowsBackForwardNavigationGestures = true
        
//        _imageView.sd_setImage(with:  URL(string:(onBoardingObj.page_image!)), placeholderImage:UIImage(named: "placeHolder"))
//        _titleLbl.text = onBoardingObj.page_title
//        _txtLbl.text = onBoardingObj.page_content

        // Do any additional setup after loading the view.
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
