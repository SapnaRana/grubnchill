//
//  Users.swift
//  Grub n Chill
//
//  Created by orangemac04 on 17/05/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit
import ObjectMapper

class Users: Mappable {
    var following                  : String?
    var following_id               : String?
    var reviewliked_id             : String?
    var reviewliked_reviewid       : String?
    var reviewliked_userid         : String?
    var user_firstname             : String?
    var user_lastname              : String?
    var user_profilepicture        : String?
    var following_status           : String?


    required convenience init(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        following              <- map["following"]
        following_id           <- map["following_id"]
        reviewliked_id         <- map["reviewliked_id"]
        reviewliked_reviewid   <- map["reviewliked_reviewid"]
        reviewliked_userid     <- map["reviewliked_userid"]
        user_firstname         <- map["user_firstname"]
        user_lastname          <- map["user_lastname"]
        user_profilepicture    <- map["user_profilepicture"]
        following_status       <- map["following_status"]

    }

}


class UserInfoResponse: Mappable {
    
    var status: String?
    var users: [Users]?
    
    required convenience init(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        status <- map["status"]
        users <- map["data"]
    }
}
