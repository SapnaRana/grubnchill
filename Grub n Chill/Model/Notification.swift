//
//  Notification.swift
//  Grub n Chill
//
//  Created by orangemac04 on 24/05/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import Foundation
import ObjectMapper

//class Notification: Mappable {
//    var current_userid           : String?
//    var follow_userid            : String?
//    var notification_id          : String?
//    var type                     : String?
//    var user_firstname           : String?
//    var user_lastname            : String?
//    var user_message             : String?
//    var user_profilepicture      : String?
//    var review_id                : String?
//
//
//    required convenience init(map: Map) {
//        self.init()
//    }
//
//    func mapping(map: Map) {
//
//        current_userid    =   getString(string: map["current_userid"].currentValue as Any)
//        follow_userid     =   getString(string: map["follow_userid"].currentValue as Any)
//        notification_id   =   getString(string: map["notification_id"].currentValue as Any)
//        type              =   getString(string: map["type"].currentValue as Any)
//        user_firstname    =   getString(string: map["user_firstname"].currentValue as Any)
//        user_lastname     =   getString(string: map["user_lastname"].currentValue as Any)
//        user_message          =   getString(string: map["user_message"].currentValue as Any)
//        user_profilepicture   =   getString(string: map["user_profilepicture"].currentValue as Any)
//        review_id             =   getString(string: map["review_id"].currentValue as Any)
//
//
////        self.current_userid   = current_userid   //     <- map["current_userid"]
////        follow_userid           <- map["follow_userid"]
////        notification_id         <- map["notification_id"]
////        type                    <- map["type"]
////        user_firstname          <- map["user_firstname"]
////        user_lastname           <- map["user_lastname"]
////        user_message            <- map["user_message"]
////        user_profilepicture     <- map["user_profilepicture"]
////        review_id               <- map["review_id"]
//
//    }
//
//}
//class NotificationInfoResponse: Mappable {
//
//    var status: String?
//    var notification: [Notification]?
//
//    required convenience init(map: Map) {
//        self.init()
//    }
//
//    func mapping(map: Map) {
//        status <- map["status"]
//        notification <- map["data"]
//    }
//}
//
//

struct Notification: Codable {
    let current_userid, follow_userid, notification_id, type, user_firstname, user_lastname, user_message, user_profilepicture,review_id: String?

    enum CodingKeys: String, CodingKey {
        case current_userid, follow_userid, notification_id, type, user_firstname, user_lastname, user_message, user_profilepicture,review_id
    }
}




extension Notification {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(Notification.self, from: data) else {
            return nil }
        self = me
    }
}


//
//class Bindable<Value> {
//    private var observations = [(Value) -> Bool]()
//    private var lastValue: Value?
//    
//    init(_ value: Value? = nil) {
//        lastValue = value
//    }
//}
//
//
//
//
//private extension Bindable {
//    
//    func addObservation<O: AnyObject>(
//        for object: O,
//        handler: @escaping (O,Value) -> Void
//        ){
//        
//        lastValue.map {handler (object, $0)}
//        
//        observations.append { [weak object] value in
//            guard let object = object else {
//                return false
//            }
//            handler(object ,value)
//            return true
//        }
//    }
//}
//extension Bindable {
//    func update(with value: Value){
//        lastValue = value
//        observations = observations.filter{$0(value)}
//    }
//}
