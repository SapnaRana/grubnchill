//
//  UserModel.swift
//  Grub n Chill
//
//  Created by orangemac04 on 17/05/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit

class UserModel: NSObject {
    var users: [Users]?
    
    typealias FetchUsersCompletion = ((_ isSuccess: Bool) -> Void)
    
    func fetchUsers(value : String , completion: @escaping FetchUsersCompletion) {
        APIManager.shared.requestGetInfo(value: value) { (isSuccess, jsonData) in
            if isSuccess {
                print(jsonData?.jsonDic as! [String : Any])
                if let response = UserInfoResponse(JSON: jsonData?.jsonDic as! [String : Any]){
                    if response.status == "1"{
                        self.users = response.users
                        completion(true)
                    }
                }
                else
                {
                    completion(false)
                }
            }
            else
            {
                completion(false)
            }
            
        }
    }


}
