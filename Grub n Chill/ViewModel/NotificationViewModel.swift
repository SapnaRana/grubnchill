//
//  NotificationViewModel.swift
//  Grub n Chill
//
//  Created by orangemac04 on 24/05/19.
//  Copyright © 2019 orangemac04. All rights reserved.
//

import UIKit
import Toast_Swift

class NotificationViewModel : NSObject  {
    var notification = [Notification]()

    typealias FetchUsersCompletion = ((_ isSuccess: Bool) -> Void)


    func requestData(value : String,completion: @escaping FetchUsersCompletion){
        APIManager.shared.rxApiMethod(typeOf: value ) { (result) in
            switch result {
            case .success(let returnJson) :
                let obj = returnJson["data"].arrayValue.compactMap {return Notification(data: try! $0.rawData())}
                self.notification = self.notification.count > 0 ? self.notification + obj : obj
                completion(true)
            case .failure(_):
                completion(false)
                break

            }
        }
    }
    
    func requestAcceptAndRejectMethod(value : String, from view: UIViewController ,  completion: @escaping FetchUsersCompletion) {
        APIManager.shared.rxApiMethod(typeOf: value) { (result) in
            switch result {
            case .success(let returnJson):
                let message = returnJson["message"].stringValue
                view.view.makeToast(message, duration: toastTimer, position: .center)
                completion(true)
            case .failure(_):
                view.view.makeToast("Something wrong", duration: toastTimer, position: .center)
                completion(false)
            }

        }
    }
    
    
    
    func gotoSinglePostView( notificationModel : Notification , from view: UIViewController){
        let viewObject = SinglePostVC.init(nibName: "SinglePostVC", bundle: nil)
        viewObject.reviewId = notificationModel.review_id!
        view.navigationController!.pushViewController(viewObject, animated: true)
    }
    
    
    

    
}

